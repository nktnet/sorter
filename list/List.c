// Khiet Tam Nguyen (z5313514)
// 24.08.20

// List ADT implementation.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "List.h"

//////////////////////////////////////////////////////////

typedef struct node *Node;

//////////////////////////////////////////////////////////

struct node {   
    Node prev;
    Node next;   
    Item item;   
};    

struct list {
    Node head;
    Node tail;
    int length;
};

//////////////////////////////////////////////////////////

// Helpers
static Node new_node(Item item);
static void insert_n(List l, Node prev, Item item);
static void remove_n(List l, Node prev);
static Node partition(Node n1, Node n2);
static void qs_recurse(Node n1, Node n2);
static Node find_node_pos(List l, int pos);
static bool not_in_set(List l, Item item);
static void insert_union(List uL, List iL);

//////////////////////////////////////////////////////////

// Creates a new malloced list.
List new_list(void) {
    List l = malloc(sizeof(struct list));
    assert(l);
    l->head = NULL;
    l->tail = NULL;
    l->length = 0;
    return l;
}

// Free a malloced list.
void free_list(List l) {
    if (!l) return;
    Node curr = l->head;
    while (curr) {
        Node rem = curr;
        curr = curr->next;
        free(rem);
    }
    free(l);
}

// Returns the length of the list.
int list_length(List l) {
    assert(l);
    return l->length;
}

// Insert a node at beginning of the list.
void insert_head(List l, Item item) {
    assert(l);
    insert_n(l, NULL, item);
}

// Insert a node at the end of the list.
void insert_tail(List l, Item item) {
    assert(l);
    insert_n(l, l->tail, item);
}

// Insert a node at a given position in the linked list.
void insert_pos(List l, Item item, int pos) {
    assert(l);
    Node prev = find_node_pos(l, pos);
    insert_n(l, prev, item);
}

// Attempts to insert a node into the list, if the list
// is sorted in ascending order. Returns false otherwise.
bool insert_order(List l, Item item) {
    if (!l || !list_is_ascending(l)) return false;
    Node curr = l->head;
    Node prev = NULL;
    while (curr && ItemCompare(curr->item, item) < 0) {
        prev = curr;
        curr = curr->next;
    }
    insert_n(l, prev, item);
    return true;
}

// Remove the first node in the list.
void remove_head(List l) {
    assert(l);
    remove_pos(l, 0);
}

// Remove the last node in the list.
void remove_tail(List l) {
    assert(l);
    remove_pos(l, l->length - 1);
}

// Remove the node at the given position.
void remove_pos(List l, int pos) {
    assert(l);
    if (!l->head || l->length < 1) return;
    if (pos > l->length - 1) pos = l->length - 1;
    Node prev = find_node_pos(l, pos);
    remove_n(l, prev);
}

//////////////////////////////////////////////////////////

// Reversing a double linked list using pointers,
// instead of altering n->item. Iterative method (3 pointers)
void reverse_list(List l) {
    if (!l || !l->head) return;
    l->tail = l->head;
    Node prev = NULL;
    Node curr = l->head;
    while (curr) {
        Node next = curr->next;
        curr->next = prev;
        curr->prev = next;
        prev = curr;
        curr = next;
    }
    l->head = prev;
}

// Wrapper around the recursive quick sort function.
void sort_list(List l) {
    if (!l) return;
    qs_recurse(l->head, l->tail);
}

// Returns true if list is sorted, false otherwise.
bool list_is_ascending(List l) {
    if (!l) return true;
    Node curr = l->head;
    while (curr && curr->next) {
        if (ItemCompare(curr->item, curr->next->item) > 0) 
            return false;
        curr = curr->next;
    }
	return true;
}

// Returns the union list of l1 and l2.
List list_union(List l1, List l2) {
    List uL = new_list();
    insert_union(uL, l1);
    insert_union(uL, l2);
    return uL;
}

// Returns the intersection list of l1 and l2.
List list_intersection(List l1, List l2) {
    if (!l1 || !l2) return NULL;
	List iL = new_list();
    Node curr1 = l1->head;
    while (curr1) {
        Node curr2 = l2->head;
        while (curr2) {
            if (curr1->item == curr2->item) {
                insert_head(iL, curr1->item);
            }
            curr2 = curr2->next;
        }
        curr1 = curr1->next;
    }
    return iL;
}

// Returns the difference list of l1 and l2 {l1 - l2}
List list_difference(List l1, List l2) {
    if (!l1 || !l2) return NULL;
	List dL = new_list();
    Node curr = l1->head;
    while (curr) {
        if (not_in_set(l2, curr->item)) {
            insert_head(dL, curr->item); 
        }
        curr = curr->next;
    }
    return dL;
}

//////////////////////////////////////////////////////////
//                   Print Functions                    //
//////////////////////////////////////////////////////////

// Print the linked list's information
void print_list(List l) {
    // print_info(l);
    if (!l) return;
    if (!l->head) { 
        printf("List is empty\n"); 
        return;
    }
    print_forward(l);
    print_reverse(l);
}

// Prints the linked list in the forward direction.
void print_forward(List l) {
    if (!l) return;
    printf("Forward:\n\t");
    Node curr = l->head; 
    while (curr) {
        ItemShow(curr->item);
        printf(" => "); 
        curr = curr->next;
    }
    printf("X\n");
}

// Prints the linked list in the reverse direction.
void print_reverse(List l) {
    if (!l) return;
    printf("Reverse:\n\t");
    Node curr = l->tail; 
    while (curr) {
        ItemShow(curr->item);
        printf(" => "); 
        curr = curr->prev;
    }
    printf("X\n");
}

// Print other useful information about the list.
void print_info(List l) {
    if (!l || !l->head) return;
    printf("Length: %d - ", l->length);
    if (l->length > 1) {
        printf("[%d-%d]\n", 0, l->length - 1);
    } else {
        printf("[%d]\n", 0);
    }
}

//////////////////////////////////////////////////////////
//                    Helper Functions                  //
//////////////////////////////////////////////////////////

// Creates a new malloced node.
static Node new_node(Item item) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->item = item;
    n->prev= NULL;
    n->next = NULL;
    return n;
}

// Insert a node into the list, given a prev pointer.
static void insert_n(List l, Node prev, Item item) {
    Node n = new_node(item);
    if (!prev) {
        // Inserting at the start.
        n->next = l->head;
        l->head = n;
    } else {
        // Inserting in the middle.
        n->next = prev->next;
        n->prev = prev;
        prev->next = n;
    }
    if (n->next) {
        // There is a next node, set n to be 
        // the prev in the list.
        n->next->prev = n;
    } else {
        // Inserted at the end.
        l->tail = n;
    }
    l->length++;
}

static void remove_n(List l, Node prev) {
    Node rem = NULL;
    if (!prev) {
        // Removing at the start
        rem = l->head;
        l->head = rem->next;
        // Set prev for the new head.
        if (rem->next) l->head->prev = NULL;
    } else {
        // Removing in the middle/end
        rem = prev->next;
        prev->next = rem->next;
        // Set rev for the node after the removed node.
        if (rem->next) rem->next->prev = prev;
    }
    // Removing the tail node.
    if (!rem->next) l->tail = prev;
    free(rem);
    l->length--;
}

// Creating partitions for quick sort.
static Node partition(Node n1, Node n2) {
    assert(n1 && n2);
    Item pivot = n2->item;
    Node i = n1->prev;
    for (Node j = n1; j != n2; j = j->next) {
        if (ItemCompare(j->item, pivot) <= 0) {
            i = (!i) ? n1 : i->next;
            ItemSwap(&i->item, &j->item);
        }
    }
    i = (!i) ? n1 : i->next;
    ItemSwap(&i->item, &n2->item);
    return i;
}

// Sort the linked list recurssively.
static void qs_recurse(Node n1, Node n2) {
    if (!n1 || !n2) return;
    // Already crossing over the partition.
    if (n1 == n2 || n1 == n2->next) return;
    Node p = partition(n1, n2);
    if (!p) return;
    qs_recurse(n1, p->prev);
    qs_recurse(p->next, n2);
}

// Find the position of insertion and returns the prev node.
// If this exceeds the length of the list, returns the
// last node.
static Node find_node_pos(List l, int pos) {
    assert(l);
    Node curr = l->head;
    Node prev = NULL;
    int p = 0;
    while (curr && p < pos) {
        p++;
        prev = curr;
        curr = curr->next;
    }
    return prev;
}

// Checks if a node is in the given set.
static bool not_in_set(List l, Item item) {
    Node curr = l->head;
    while (curr) {
        if (ItemCompare(item, curr->item) == 0) return false;
        curr = curr->next;
    }
    return true;
}

// Insert nodes from an insertList to the unionSet if
// those nodes are not already in the set.
static void insert_union(List uL, List iL) {
    Node curr = iL->head;
    while (curr != NULL) {
        if (not_in_set(uL, curr->item)) {
            insert_head(uL, curr->item);
        }
        curr = curr->next;
    }
}
