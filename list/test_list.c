// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Main function for random list ADT.

#include <stdio.h>

#include "List.h"

typedef enum command {
    PRINT,
    INSERT_HEAD,
    INSERT_TAIL,
    INSERT_AT_POS,
    INSERT_ORDER,
    REMOVE_HEAD,
    REMOVE_TAIL,
    REMOVE_AT_POS,
    REVERSE_LIST,
    SORT_LIST,
    CHECK_ASCENDING,
    HELP,
} Command;

static void print_help(void);

int main(void) {
    List l = new_list();
    printf("|----------------------|\n");
    printf("|   Enter %d for help   |\n", HELP);
    printf("|----------------------|\n");
    while (1) {
        printf("Enter command: ");
        int command, pos;
        Item it;
        if (scanf("%d", &command) != 1) break;
        switch (command) {
        case PRINT:
            /* print_list(l); */                                        
            break;
        case INSERT_HEAD:
            if ((it = ItemRead()) == NULL) return 1;
            printf("Inserting head\n");
            insert_head(l, it);   
            break;
        case INSERT_TAIL:
            if ((it = ItemRead()) == NULL) return 1;
            printf("Inserting tail\n");
            insert_tail(l, it);   
            break;
        case INSERT_ORDER:
            if ((it = ItemRead()) == NULL) return 1;
            if (insert_order(l, it)) {
                printf("Inserting in the right order\n");
            } else {
                printf("Insertion failed, make sure the list is ");
                printf("sorted. This can be done with command %d\n", SORT_LIST);
            }
            break;
        case INSERT_AT_POS:
            if (scanf("%d", &pos) != 1) return 1;
            if ((it = ItemRead()) == NULL) return 1;
            if (pos >= list_length(l)) pos = list_length(l);
            printf("Inserting at pos [%d]\n", pos),
            insert_pos(l, it, pos);   
            break;
        case REMOVE_HEAD:
            printf("Removing head\n");
            remove_head(l);                                 
            break;
        case REMOVE_TAIL:
            printf("Removing tail\n");
            remove_tail(l);                                 
            break;
        case REMOVE_AT_POS:
            scanf("%d", &pos); 
            if (pos > list_length(l) - 1) pos = list_length(l) - 1;
            if (pos < 0) {
                printf("Nothing to remove\n");
            } else {
                printf("Removing the node at pos %d\n", pos);
                remove_pos(l, pos);
            }
            break;
        case REVERSE_LIST:
            printf("Reversing the linked list\n");
            reverse_list(l);                                
            break;
        case SORT_LIST:
            printf("Sorting the linked list\n");
            sort_list(l);                                  
            break;
        case CHECK_ASCENDING:
            if (list_is_ascending(l)) printf("List is ascending\n");
            else printf("List is not ascending\n");
            break;
        case HELP:
        default:
            print_help();
            continue;
        }
        print_list(l);
        printf("-----------------------\n");
    }
    free_list(l);
}

static void print_help(void) {
    printf("--------------------------------------------------------\n");
    printf(
        "\tCommands:\n"
        "%5d                  print list\n"
        "%5d <item>           insert item at head\n"
        "%5d <item>           insert item at tail\n"    
        "%5d <pos> <item>     insert item at pos\n"    
        "%5d <item>           insert item in order IF list is ordered\n"    
        "%5d                  remove item at head\n"
        "%5d                  remove item at tail\n"
        "%5d <pos>            remove item at pos\n"
        "%5d                  reverse the order of the array\n"
        "%5d                  sort array in ascending order\n"
        "%5d                  check if list is ascending\n"
        "%5d                  print help message\n",
        PRINT,
        INSERT_HEAD,
        INSERT_TAIL,
        INSERT_AT_POS,
        INSERT_ORDER,
        REMOVE_HEAD,
        REMOVE_TAIL,
        REMOVE_AT_POS,
        REVERSE_LIST,
        SORT_LIST,
        CHECK_ASCENDING,
        HELP
    );
    printf("--------------------------------------------------------\n");
}
