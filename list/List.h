// Khiet Tam Nguyen (z5313514)
// Tuesday 25th August 2020

// Interface for List ADT.

//////////////////////////////////////////////////////////////
//            _     _     _        _    ____ _____          //
//           | |   (_)___| |_     / \  |  _ \_   _|         //
//           | |   | / __| __|   / _ \ | | | || |           //
//           | |___| \__ \ |_   / ___ \| |_| || |           //
//           |_____|_|___/\__| /_/   \_\____/ |_|           //
//                                                          //
//         ___       _             __                       //
//        |_ _|_ __ | |_ ___ _ __ / _| __ _  ___ ___        //
//         | || '_ \| __/ _ \ '__| |_ / _` |/ __/ _ \       //
//         | || | | | ||  __/ |  |  _| (_| | (_|  __/       //
//        |___|_| |_|\__\___|_|  |_|  \__,_|\___\___|       //
//                                                          //
//////////////////////////////////////////////////////////////

#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

#include "Item.h"

typedef struct list *List;

// Creates a new malloced list.
List new_list(void);
// Free a malloced list.
void free_list(List l);

// Returns the length of the list.
int list_length(List l);
// Insert a node at beginning of the list.
void insert_head(List l, Item item);
// Insert a node at the end of the list.
void insert_tail(List l, Item item);
// Insert a node at a given position in the linked list.
void insert_pos(List l, Item item, int pos);
// Attempts to insert a node into the list, if the list
// is sorted in ascending order. Returns false otherwise.
bool insert_order(List l, Item item);

// Remove the first node in the list.
void remove_head(List l);
// Remove the last node in the list.
void remove_tail(List l);
// Remove the node at the given position.
void remove_pos(List l, int pos);

// Reverses the order of the given list.
void reverse_list(List l);
// Sort the list
void sort_list(List l);
// Returns true if list is sorted, false otherwise.
bool list_is_ascending(List l);

// Returns the union list of l1 and l2.
List list_union(List l1, List l2);
// Returns the intersection list of l1 and l2.
List list_intersection(List l1, List l2);
// Returns the difference list of l1 and l2 {l1 - l2}
List list_difference(List l1, List l2);

// Print all relevant linked list's information (including below)
void print_list(List l);
// Prints the linked list in the forward direction.
void print_forward(List l);
// Prints the linked list in the reverse direction.
void print_reverse(List l);
// Print other useful information about the list.
void print_info(List l);

#endif
