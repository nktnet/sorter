// Khiet Tam Nguyen (z5313514)
// 28.08.20

// Interface for the AvlBSTree ADT

#ifndef BSTREE_H
#define BSTREE_H

#include "Item.h"

typedef struct bstree *BSTree;

// Creates a new empty tree
BSTree BSTreeNew(void);

// Frees all memory associated with the given tree
void BSTreeFree(BSTree t);

// Free tree structure, but not memories of items.
void BSTreeFreeStructreOnly(BSTree t);

// Inserts copy of item into tree. 
void BSTreeInsert(BSTree t, Item item);

// Return latest item below given time, or NULL.
Item BSTreeFloor(BSTree t, Item item);

// Return earliest item after given time, or NULL.
Item BSTreeCeiling(BSTree t, Item item);

// Fill given array with first n items, or max available
// and return the amount filled.
int BSTreeFillFirstN(BSTree t, Item *arr, int n);

// Get the kth item 'value' in the tree (inorder)
Item BSTreeGetKth(BSTree t, int k);

// Copy a given tree up to the given depth
BSTree BSTreeCopy(BSTree t);

// Lists all the items in the tree, one per line
void BSTreeList(BSTree t);

// Shows the tree structure
void BSTreeShow(BSTree t);

////////////////////////////////////////////////////////////////////////

#endif
