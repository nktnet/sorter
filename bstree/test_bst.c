// Khiet Tam Nguyen (z5313514)
// 27.08.20

// Main function to test avl tree.

#include <stdio.h>

#include "BSTree.h"
#define MAX_LINE 30

// Stop after this many items.
#define TEST_MAX_TREE 5

int main(void) {
    BSTree t = BSTreeNew();
    int n = 0;
    Item it; 
	while ((it = ItemRead()) && n < TEST_MAX_TREE) {
        BSTreeInsert(t, it);
        printf("-----------------------------------\n");
        BSTreeShow(t);
        n++;
    }
/*
    printf("-----------------------------------\n");
    printf("BSTree Copy\n");
    BSTree copyBSTree = BSTreeCopy(t);
    BSTreeShow(copyBSTree);
    BSTreeFree(copyBSTree);
    printf("-----------------------------------\n");
*/

/*
    printf("BSTree Fill First N\n");
    Item arr[n];
    int nReturned = BSTreeFillFirstN(t, arr, n);
    for (int i = 0; i < nReturned; i++) {
        ItemShow(arr[i]);
        printf(" ");
    }
    printf("\n");
    BSTreeFree(t);
*/

/*
    printf("BSTree floor and ceiling\n");
    while ((it = ItemRead())) {
        Item ceil = BSTreeCeiling(t, it);
        Item floor = BSTreeFloor(t, it);

        printf("Ceil: ");
        ItemShow(ceil);

        printf("Item: ");
        ItemShow(it);

        printf("Floor: ");
        ItemShow(floor);

        printf("\n");
    }
*/
    printf("Item at kth position\n");
    while (true) {
        int k;
        scanf("%d", &k);
        Item kIt = BSTreeGetKth(t, k);
        printf("Item at position %d is: ", k);
        ItemShow(kIt);
        printf("\n");

    }
    return 0;
}
