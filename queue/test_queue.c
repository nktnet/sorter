// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Main function to test Queue ADT.

#include <stdio.h>

#include "Queue.h"

typedef enum command {
    PRINT,
    PUSH,
    POP,
    QUIT,
    HELP,
} Command;

static void printHelp(void);

int main(void) {
    Item item, pop;
    int cmd; 
    Queue q = QueueNew();

    printf("Push first item: ");
    item = ItemRead();
    if (!item) return 1;
    QueuePush(q, item);
    QueueShow(q);
    printf("--------------------------\n");
    printf("Enter '%d' for a list of commands\n", HELP);
    while (!QueueIsEmpty(q)) {
        printf("Enter command: ");
        if (scanf("%d", &cmd) != 1) return 1;
        switch (cmd) {
        case PRINT: 
            break;
        case PUSH:
            item = ItemRead();
            QueuePush(q, item);
            break;
        case POP:
            pop = QueuePop(q);
            ItemShow(pop);
            printf(" was popped from the queue\n");
            break;
        case QUIT:
            printf("Sayonara\n");
            return 0;
        case HELP:
        default:
            printHelp();
            continue;
        }
        QueueShow(q);
        printf("--------------------------\n");
    }
    QueueDrop(q);

    return 0;
}

static void printHelp(void) {
    printf("-----------------------------------------------\n");
    printf(
        "\tCommands:\n"
        "\t%d                print the queue\n"
        "\t%d <item>         push item to queue\n"
        "\t%d                pop item from queue\n"
        "\t%d                quits the program\n"
        "\t%d                print this help message\n",
        PRINT,
        PUSH,
        POP,
        QUIT,
        HELP
    );
    printf("-----------------------------------------------\n");
}
