// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Queue ADT interface.

#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>

#include "Item.h"

typedef struct queue *Queue;

// Create a new queue
Queue QueueNew(void);

// Free all resources allocated for the queue
void QueueDrop(Queue q);

// Add an item to the end of the queue
void QueuePush(Queue q, Item item);

// Remove an element from the front of the queue and return it
Item QueuePop(Queue q);

// Get the element at the front of the queue (without removing it)
Item QueuePeek(Queue q);

// Get the number of elements in the queue
int QueueSize(Queue q);

// Check if the queue is empty
bool QueueIsEmpty(Queue q);

// Prints the queue to stdin
void QueueShow(Queue q);

#endif
