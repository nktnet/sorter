// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Queue ADT implementation.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Queue.h"

//////////////////////////////////////////////////////////

typedef struct node *Node;

//////////////////////////////////////////////////////////

struct node {
    Item item;
    Node next;
};

struct queue {
    Node front;
    Node end;
    int size;
};

//////////////////////////////////////////////////////////

// Creates a malloced node and returns it.
static Node newNode(Item item) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->item = item;
    n->next = NULL;
    return n;
}

// Returns a malloced queue.
Queue QueueNew(void) {
    Queue q = malloc(sizeof(struct queue));
    assert(q);
    q->front = NULL;
    q->end = NULL;
    q->size = 0;
    return q;
}

// Free all memories allocated to queue.
void QueueDrop(Queue q) {
    if (!q) return;
    Node curr = q->front;
    while (curr) {
        Node rem = curr;
        curr = curr->next;
        free(rem);
    }
    free(q);
}

// Add an item to the end of the queue.
void QueuePush(Queue q, Item item) {
    assert(q);
    Node n = newNode(item);
    if (!q->end) {
        q->front = n;
    } else {
        q->end->next = n; 
    }
    q->end = n;
    q->size++;
}

// Remove the first item in the queue and 
// return its value.
Item QueuePop(Queue q) {
    assert(q && !QueueIsEmpty(q));
    Node rem = q->front;
    Item pop = rem->item;
    q->front = rem->next;
    if (rem == q->end) q->end = NULL;
    free(rem);
    q->size--;
    return pop;
}

// Get the element at the front of 
// the queue (without removing it)
Item QueuePeek(Queue q) {
    assert(q && !QueueIsEmpty(q));
    return q->front->item;
}

// Gets the size of the queue.
int QueueSize(Queue q) {
    assert(q);
    return q->size;
}

// Checks if the queue is empty.
bool QueueIsEmpty(Queue q) {
    assert(q);
    return (q->size == 0);
}

// Prints the queue to standard output.
void QueueShow(Queue q) {
    assert(q);
    printf("Queue:\t");
    Node curr = q->front;
    while (curr) {
        ItemShow(curr->item);
        printf(" --> ");
        curr = curr->next;
    }
    printf("X\n");
    printf("Size:\t%d\n", QueueSize(q));
}
