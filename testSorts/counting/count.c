// Khiet Tam Nguyen (z5313514)
// 01.09.20

// https://www.techiedelight.com/counting-sort-algorithm-implementation/

#include <stdio.h>
#include <string.h>

#include "Item.h"
#include "Array.h"

#define MAX_SORT 10000000

static Item find_max(Array A, int lo, int hi);
void count_sort(Array A, int hi, int lo);
static void cs_helper(Array A, int size, int range);

// C program for stable version of counting sort
int main(void) {
    Array A = ArrayNew(MAX_SORT);
    int size = ArrayRead(A);

    int lo = 0;
    int hi = size - 1;

    count_sort(A, hi, lo);
    ArrayPrintVert(A, 0, size - 1);

    return 0;
}

void count_sort(Array A, int hi, int lo) {
    if (!A) return;
    Item max = find_max(A, lo, hi);
    int range = Key(max) + 1;
    cs_helper(&A[lo], hi - lo + 1, range);
}


static void cs_helper(Array A, int size, int range) {
    Array output = ArrayNew(range);
    int *freq = calloc(range, sizeof(int));
    // using value of integer in the input Array as index,
    // store count of each integer in freq[] Array
    for (int i = 0; i < size; i++) {
        freq[Key(A[i])]++;
    }
    // Calculate the starting index for each integer
    int total = 0;
    for (int i = 0; i < range; i++) {
        int oldCount = freq[i];
        freq[i] = total;
        total += oldCount;
    }
    // copy to output Array, preserving order of inputs with equal keys
    for (int i = 0; i < size; i++) {
        output[freq[Key(A[i])]] = A[i];
        freq[Key(A[i])]++;
    }
    free(freq);

    // copy the output Array back to the input Array
    for (int i = 0; i < size; i++) {
        A[i] = output[i];
    }
    // Don't want to use ArrayFree since that frees the item (still need
    // to print them in the main function);
    free(output);
}

static Item find_max(Array A, int lo, int hi) {
    Item max = A[lo];
    for (int i = lo; i <= hi; i++) {
        if (ItemCompare(max, A[i]) < 0) {
            max = A[i];
        }
    }
    return max;
}
