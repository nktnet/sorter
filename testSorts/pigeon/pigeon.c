// Khiet Tam Nguyen (z5313514)
// 04.09.20

// Program base code:
// https://programology.wordpress.com/2015/08/28/c-program-for-pigeonhole-sort/

#include <stdio.h>

#include "Item.h"
#include "Array.h"

#define MAX_SORT 2000

static void pigeon(Array A, int num, int min, int max);
static void changeMinMax(Array A, Item *minItem, Item *maxItem, int lo, int hi);
void pigeonhole_sort(Array A, int lo, int hi);

int main(void) {
    Array A = ArrayNew(MAX_SORT);
    printf("Enter Values:\n");
    int size = ArrayRead(A);
    int lo = 0;
    int hi = size - 1;

#if 1
    lo = 2;
    hi = 8;
#endif

    printf("Sorted Array:\n");
    pigeonhole_sort(A, lo, hi);

    ArrayPrintVert(A, 0, size - 1);
    return 0;
}

// Sorts the array using the pigeonhle principle.
void pigeonhole_sort(Array A, int lo, int hi) {
    if (!A || hi <= lo) return;
    int size = A_SIZE(lo, hi);
    Item minItem; 
    Item maxItem; 
    changeMinMax(A, &minItem, &maxItem, lo, hi);
    // Give only A[lo] .. A[hi] section of array.
    pigeon(&A[lo], size, Key(minItem), Key(maxItem));
}

// Helper function to update the minimum and maximum valued item
// from the given array and min/max pointers.
static void changeMinMax(Array A, Item *minItem, Item *maxItem, int lo, int hi) {
    if (!A) return;
    *minItem = A[lo];
    *maxItem = A[lo];
    for (int i = lo + 1; i <= hi; i++) {
        if (ItemCompare(*minItem, A[i]) > 0) {
            *minItem = A[i];
        } else if (ItemCompare(*maxItem, A[i]) < 0) {
            *maxItem = A[i];
        }
    }
}

// Helper function that sorts the entire given array 
// using the pigeon hole principle.
static void pigeon(Array A, int num, int min, int max) {
    int holeSize = A_SIZE(min, max);
    // Array of pigeonholes
    int holes[holeSize];
    for(int i = 0; i < holeSize; i++) {                 
        holes[i]=0;
    }

    // Populate the pigeonholes.
    for (int i = 0; i < num; i++) {
        holes[Key(A[i]) - min]++;
    }

    // Put the elements back into the Array in order.
    int k = 0;
    for (int count = 0; count < holeSize; count++) {
        while (holes[count] > 0) {
            ItemChangeKey(A[k], count + min);
            holes[count]--;
            k++; 
        }
    }
}


