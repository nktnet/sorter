// Khiet Tam Nguyen (z5313514)
// 01.09.20

// C program to sort array using pancake sort 
// Adapted from:
// https://www.geeksforgeeks.org/pancake-sorting/
#include <stdlib.h> 
#include <stdio.h> 

#include "Array.h"

static int getMaxIndex(Array A, int lo, int hi);

static int getMaxIndex(Array A, int lo, int hi) {
    int maxI = lo;
    Item max = A[lo];
    for (int i = lo + 1; i <= hi; i++) {
        if (ItemCompare(max, A[i]) < 0) {
            max = A[i];
            maxI = i;
        }
    }
    return maxI;
}
  
void pancakeSort(Array A, int lo, int hi) { 
    for (int currHi = hi; currHi > lo; currHi--) { 
        int maxI = getMaxIndex(A, lo, currHi);
        printf("Crash %d\n", maxI);
        if (maxI < hi) { 
            ArrayReverse(A, lo, maxI);
            ArrayReverse(A, lo, currHi); 
        } 
    } 
} 

#define MAX_SORT 10000
  
// Driver program to test above function 
int main(void) { 
    Array A = ArrayNew(MAX_SORT);
    int size = ArrayRead(A);
    int lo = 0;
    int hi = size - 1;
  
    pancakeSort(A, lo, hi); 
  
    puts("Sorted Array "); 
    ArrayPrintVert(A, lo, hi);
  
    return 0; 
} 
