#include <stdio.h>

#define MAX_SORT 2000

#define UP 1
#define DOWN 0

#include "Array.h"
#include "Item.h"

void compareSwap(Array A, int i, int j, int dir) {
    if (dir == (ItemCompare(A[i], A[j]) > 0)) {
        ItemSwap(&A[i], &A[j]);
    }
}

static void bitonicmerge(Array A, int lo, int size, int dir) {
    if (size <= 1) return;
    int k = size / 2;
    for (int i = lo; i < lo + k ;i++) {
        compareSwap(A, i, i+k, dir);    
    }
    bitonicmerge(A, lo, k, dir);
    bitonicmerge(A, lo + k, k, dir);    
}

static void recurseBitonic(Array A, int lo, int size, int dir) {
    if (size <= 1) return;
    int k = size / 2;
    recurseBitonic(A, lo, k, UP);
    recurseBitonic(A, lo + k, k, DOWN);
    bitonicmerge(A, lo, size, dir);
}

void bitonic_sort(Array A, int lo, int hi) {
    recurseBitonic(A, lo, hi + 1, UP);
}

int main(void) {
    Array A = ArrayNew(MAX_SORT);
    int size = ArrayRead(A);
    int lo = 0;
    int hi = size - 1;
  
    printf("Original:\n");
    ArrayPrintHori(A, lo, hi);

    bitonic_sort(A, lo, hi); 
  
    puts("Sorted Array\n"); 
    ArrayPrintVert(A, lo, hi);
  
    return 0; 
}
