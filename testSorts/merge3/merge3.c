// From Stackoverflow:
// https://stackoverflow.com/questions/46370099/merge-sort-3-way-split-c

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void mersort(int v[], size_t len);
void mersort3(int v[], size_t len);
void merge(int v[], size_t mid, size_t len);

/* a simple little VLA-based merge. don't invoke with huge arrays */
void merge(int v[], size_t mid, size_t len) {
    if (len < 2) return;
    size_t i=0, j=mid, k=0;
    int tmp[len];
    while (i < mid && j < len) {
        tmp[k++] = (v[i] < v[j]) ? v[i++] : v[j++];
    }
    memcpy(tmp+k, v+i, (mid-i) * sizeof *v);
    memcpy(v, tmp, (k + (mid-i)) * sizeof *v);
}

void mersort(int v[], size_t len) {
    if (len < 2) return;
    size_t mid = len/2;
    mersort(v, mid);
    mersort(v+mid, len-mid); // see here.
    merge(v, mid, len);
}


void mersort3(int v[], size_t len) {
    if (len < 3) {
        mersort(v, len);
        return;
    }

    size_t m1 = len/3;
    size_t m2 = (2 * len)/3;
    mersort3(v, m1);
    mersort3(v+m1, m2-m1);   // see here
    mersort3(v+m2, len-m2);  // and here
    merge(v, m1, m2);
    merge(v, m2, len);
}

int main(void) {
    srand((unsigned)time(NULL));

    const size_t N = 29;
    size_t i,j;
    int ar[N], n=0;

    // build a sequence from 1..29
    for (i=0; i<N; ++i)
        ar[i] = ++n;

    // shuffle the sequence
    for (i=0; i<3; ++i) {
        for (j=0; j<N; ++j) {
            n = rand() % N;
            int tmp = ar[n];
            ar[n] = ar[j];
            ar[j] = tmp;
        }
    }

    // show the shuffled sequence
    for (i=0; i<N; ++i)
        printf("%d ", ar[i]);
    fputc('\n', stdout);

    // sort it
    mersort3(ar, N);

    // show it again
    for (i=0; i<N; ++i)
        printf("%d ", ar[i]);
    fputc('\n', stdout);

    return EXIT_SUCCESS;
}
