// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Priority PQueue ADT interface.

#ifndef PQUEUE_H
#define PQUEUE_H

#include <stdbool.h>

#include "Item.h"

typedef struct pQueue *PQueue;

// Create a new PQueue
PQueue PQueueNew(void);

// Free all resources allocated for the PQueue
void PQueueDrop(PQueue q);

// Add an item to the end of the PQueue
void PQueueJoin(PQueue q, Item item);

// Remove an element from the front of the PQueue and return it
Item PQueueLeave(PQueue q);

// Get the element at the front of the PQueue (without removing it)
Item PQueuePeek(PQueue q);

// Get the number of elements in the PQueue
int PQueueSize(PQueue q);

// Check if the PQueue is empty
bool PQueueIsEmpty(PQueue q);

// Prints the PQueue to stdin
void PQueueShow(PQueue q);

#endif
