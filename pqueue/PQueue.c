// Khiet Tam Nguyen (z5313514)
// 26.08.20

// Priority PQueue ADT implementation.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "PQueue.h"

//////////////////////////////////////////////////////////

typedef struct node *Node;

//////////////////////////////////////////////////////////

struct node {
    Item key;
    Node next;
};

struct pQueue {
    Node front;
    Node end;
    int size;
};

//////////////////////////////////////////////////////////

// Helpers
static Node findPos(PQueue q, Node n);

//////////////////////////////////////////////////////////

// Creates a malloced node and returns it.
static Node newNode(Item key) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->key = key;
    n->next = NULL;
    return n;
}

// Returns a malloced PQueue.
PQueue PQueueNew(void) {
    PQueue q = malloc(sizeof(struct pQueue));
    assert(q);
    q->front = NULL;
    q->end = NULL;
    q->size = 0;
    return q;
}

// Free all memories allocated to PQueue.
void PQueueDrop(PQueue q) {
    if (!q) return;
    Node curr = q->front;
    while (curr) {
        Node rem = curr;
        curr = curr->next;
        free(rem);
    }
    free(q);
}

// Add an key to the correct position.
void PQueueJoin(PQueue q, Item key) {
    assert(q);
    Node n = newNode(key);
    Node prev = findPos(q, n);
    if (!prev) {
        // Inserting at the head.
        n->next = q->front;
        q->front = n;
    } else {
        n->next = prev->next;
        prev->next = n;
    }
    if (prev == q->end) {
        q->end = n;
    }
    q->size++;
}

// Remove the first key in the PQueue and 
// return its value.
Item PQueueLeave(PQueue q) {
    assert(q && !PQueueIsEmpty(q));
    Node rem = q->front;
    Item pop = rem->key;
    q->front = rem->next;
    if (rem == q->end) q->end = NULL;
    free(rem);
    q->size--;
    return pop;
}

// Get the element at the front of 
// the PQueue (without removing it)
Item PQueuePeek(PQueue q) {
    assert(q && !PQueueIsEmpty(q));
    return q->front->key;
}

// Gets the size of the PQueue.
int PQueueSize(PQueue q) {
    assert(q);
    return q->size;
}

// Checks if the PQueue is empty.
bool PQueueIsEmpty(PQueue q) {
    assert(q);
    return (q->size == 0);
}

// Prints the PQueue to standard output.
void PQueueShow(PQueue q) {
    assert(q);
    printf("PQueue:\t");
    Node curr = q->front;
    while (curr) {
        ItemShow(curr->key);
        printf(" --> ");
        curr = curr->next;
    }
    printf("X\n");
    printf("Size:\t%d\n", PQueueSize(q));
}

/////////////////////////////////////////////////////////

// Find the position to insert the new node and 
// returns the prev node.
static Node findPos(PQueue q, Node n) {
    Node prev = NULL;
    Node curr = q->front;
    while (curr) {
        if (ItemCompare(curr->key, n->key) < 0) break;
        prev = curr;
        curr = curr->next;
    }
    return prev;
}
