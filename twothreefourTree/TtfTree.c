// Khiet Tam Nguyen (z5313514)
// 31.08.20 - 01.09.20

// 2-3-4 Tree Implementation
// Somewhat Inspired by Squiidz's Program on Github:
// https://gist.github.com/squiidz/996129e16d870a8244fcaa7010ee8b5c

// However, most of the code has been altered since the program
// above is written in a style that I'm not familiar with.

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "TtfTree.h"

#define PRINT_SPACE 10

// Can have any number of children greater than 2.
// Should use a binary search tree instead for 2 children.
#define MAX_CHILDREN 4
#define MAX_ITEMS (MAX_CHILDREN - 1)

typedef struct node *Node;
struct node {
    int order;
    Item items[MAX_ITEMS]; 
    Node child[MAX_CHILDREN]; 
}; 

struct ttf {
    Node root;
};

///////////////////////////////////////////////////////////////////

// Helpers
static int searchPos(Item item, Item *Arr, int size);
static void doFillFirstN(Node n, Item *arr, int size, int *count);

// Show Helpers
static void doShow(Node n, int blankSpace);
static void doInorder(Node n);

// Free Helpers
static void dofreeNodeItems(Node n);
static void doFree(Node n);
static void doFreeStructureOnly(Node n);

// Insert helpers
bool doInsert(Node n, Item item, Item *upItem, Node *new);
static void doInsertNotFull(Node n, int pos, Item item, Node newPtr);
static void doPromoteRoot(Ttf t, Item upItem, Node n);

///////////////////////////////////////////////////////////////////

// Return a new Two-Three-Four Tree.
Ttf TtfNew(void) {
    Ttf t = malloc(sizeof(struct ttf));
    assert(t);
    t->root = NULL;
    return t;
}

// Inserts the given item into the tree.
void TtfInsert(Ttf t, Item item) {
    if (!t) return;
    Node new;
    Item upItem;
    bool promoteRoot = doInsert(t->root, item, &upItem, &new);
    if (promoteRoot) {
        doPromoteRoot(t, upItem, new);
    }
}

// Display the tree to standard output.
void TtfShow(Ttf t) {
    if (!t) return;
    doShow(t->root, 0);
}

// Print items from the tree inorder.
void TtfInorder(Ttf t) {
    if (!t) return;
    doInorder(t->root);
}

// Free all memories associated with the tree
void TtfFree(Ttf t) {
    if (!t) return;
    doFree(t->root);
    free(t);
}

// Free Ghe tree structure, leaving the items
// (if they are pointers) untouched.
void TtfFreeStructureOnly(Ttf t) {
    if (!t) return;
    doFreeStructureOnly(t->root);
    free(t);
}

// Fill given array with first n items, or max available
// and return the amount filled.
int TtfFillFirstN(Ttf t, Item *arr, int n) {
    if (!t) return 0;
    int count = 0;
    doFillFirstN(t->root, arr, n, &count);
    return count;
}

///////////////////////////////////////////////////////////////////
//                        Helper Functions                       //
///////////////////////////////////////////////////////////////////

// Return a new, uninitialised malloced node
static Node newNode(void) {
    Node n = malloc(sizeof(struct node));
    return n;
}

// Promote a new node tob be the root of the tree.
static void doPromoteRoot(Ttf t, Item upItem, Node new) {
    Node uproot = t->root;
    t->root = newNode();
    t->root->order = 1;
    t->root->items[0] = upItem;
    t->root->child[0] = uproot;
    t->root->child[1] = new;
}

// Recursively insert the new node, returning true/false for
// whether a node should be promoted.
bool doInsert(Node n, Item item, Item *upItem, Node *new) {
    if (!n) {
        *new = NULL;
        *upItem = item;
        return true;
    }

    Item newItem;
    Node newPtr;
    int pos = searchPos(item, n->items, n->order);
    bool promote = doInsert(n->child[pos], item, &newItem, &newPtr);

    if (!promote) {
        // Insertion was successful, no need to promote.
        return false;
    }

    if (n->order < MAX_ITEMS) {
        // Not full, so we can insert in the node. No promotion needed.
        doInsertNotFull(n, pos, newItem, newPtr);
        return false;
    }

    // Items in node are maximum.

    // Assume that we're inserting at the last pos, pos == MAX_ITEMS.
    Item lastItem = newItem;
    Node lastPtr = newPtr;

    if (pos < MAX_ITEMS) {
        // Position of node to be inserted is not last, change last 
        // item and pointers.
        lastItem = n->items[MAX_ITEMS - 1];
        lastPtr = n->child[MAX_CHILDREN - 1];
        for (int i = MAX_ITEMS - 1; i > pos; i--) {
            // Shift array down for insertion.
            n->items[i] = n->items[i - 1];
            n->child[i + 1] = n->child[i];
        }
        // Insert the new item.
        n->items[pos] = newItem;
        n->child[pos + 1] = newPtr;
    }

    // Always promote the middle item.
    int splitPos = MAX_ITEMS / 2;
    *upItem = n->items[splitPos];
    // Right node after split
    *new = newNode();
    // No. of items for left splitted node
    n->order = splitPos;                              
    // No. of items for right splitted node
    (*new)->order = MAX_ITEMS - splitPos;               

    for (int i = 0; i < (*new)->order; i++) {
        (*new)->child[i] = n->child[i + splitPos + 1];
        if (i < (*new)->order - 1) {
            (*new)->items[i] = n->items[i + splitPos + 1];
        } else {
            (*new)->items[i] = lastItem;
        }
    }
    (*new)->child[(*new)->order] = lastPtr;

    // Needs a promotion.
    return true;
}

// Search for the position of the item in the node.
static int searchPos(Item item, Item *Arr, int size) {
    int pos = 0;
    while (pos < size && ItemCompare(item, Arr[pos]) > 0) {
        pos++;
    }
    return pos;
}

// Insert into a node that isn't full, i.e. #nitems < MAX_ITEMS
static void doInsertNotFull(Node n, int pos, Item item, Node newPtr) {
    for (int i = n->order; i>pos; i--) {
        // Shifting array down before inserting the item.
        n->items[i] = n->items[i - 1];
        n->child[i + 1] = n->child[i];
    }
    n->items[pos] = item;
    n->child[pos + 1] = newPtr;
    n->order++; 
}

// Recursive helper to display all nodes in the tree, separated
// by the set blank space.
static void doShow(Node n, int blankSpace) {
    if (n) {
        for (int i = 1; i <= blankSpace; i++) {
            printf(" ");
        }
        printf("[");
        for (int i = 0; i < n->order; i++) {
            ItemShow(n->items[i]);
            if (i == n->order - 1) continue;
            // Print comma space for all but last item.
            printf(", ");
        }
        printf("]");
        printf("\n");
        for (int i = 0; i <= n->order; i++) {
            doShow(n->child[i], blankSpace + PRINT_SPACE);
        }
    }
}

// Free all items within the node.
static void dofreeNodeItems(Node n) {
    for (int i = 0; i < n->order; i++) {
        ItemFree(n->items[i]);
    }
}

// Recursive helper to free all nodes and 
// associated memories in the tree.
static void doFree(Node n) {
    if (!n) return;
    doFree(n->child[0]);
    for (int i = 0; i < n->order; i++) {
        doFree(n->child[i + 1]);
    }
    dofreeNodeItems(n);
    free(n);
}

// Only free the tree structure, leaving the items
// (if they are pointers) untouched.
static void doFreeStructureOnly(Node n) {
    if (!n) return;
    doFreeStructureOnly(n->child[0]);
    for (int i = 0; i < n->order; i++) {
        doFreeStructureOnly(n->child[i + 1]);
    }
    free(n);
}

// Traverse the tree and print the items in
// sorted (inorder) to stdout.
static void doInorder(Node n) {
    if (!n) return;
    doInorder(n->child[0]);
    for (int i = 0; i < n->order; i++) {
        ItemShow(n->items[i]);
        printf("\n");
        doInorder(n->child[i + 1]);
    }
}

// Fill the array with the first size items from the tree.
static void doFillFirstN(Node n, Item *arr, int size, int *count) {
    if (!n) return;
    if (*count >= size) return;
    doFillFirstN(n->child[0], arr, size, count);
    for (int i = 0; i < n->order; i++) {
        arr[*count] = n->items[i];
        *count += 1;
        doFillFirstN(n->child[i + 1], arr, size, count);
    }
}

