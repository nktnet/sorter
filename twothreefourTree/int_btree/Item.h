// Khiet Tam Nguyen (z5313514)
// 31.08.20

// Simple item interface for ints.

#ifndef ITEM_H
#define ITEM_H

#include "stdio.h"

typedef int Item;

// Read item from standard input.
Item ItemRead();

// Display item to standard output.
void ItemShow(Item item);

// Free the item.
void ItemFree(Item item);

// Compare the value of 2 items, returning
// the difference {a - b}
Item ItemCompare(Item a, Item b);

#endif
