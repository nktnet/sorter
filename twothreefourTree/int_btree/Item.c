// Khiet Tam Nguyen (z5313514)
// 31.08.20

// Simple Item Implementation for ints.

//////////////////////////////////////////////////////////
//                      Test Items                      //
//////////////////////////////////////////////////////////

#include "Item.h"

// Read item from standard input.
Item ItemRead() {
    Item item;
    if (scanf("%d", &item) != 1) return 0;
    return item;
}

// Compare the value of 2 items, returning
// the difference {a - b}
Item ItemCompare(Item a, Item b) {
    return a - b;
}

// Display item to standard output.
void ItemShow(Item item) {
    printf("%d", item);
}

// Free the item.
void ItemFree(Item item) {
    return;
}
