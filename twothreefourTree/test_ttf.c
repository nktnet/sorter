// Khiet Tam Nguyen (z5313514)
// 31.08.20

// Simple main program to test 2-3-4 tree

#include <stdio.h>
#include <stdbool.h>

#include "TtfTree.h"

#define MAX_ITEMS 10000

int main(void) {
    Ttf t = TtfNew();
    int nItems = 0;
    while (nItems < MAX_ITEMS) {
        printf("Enter Item: ");
        Item item;
        if (!(item = ItemRead())) break;
        nItems++;
        TtfInsert(t, item);
        printf("-----------------------\n");
        TtfShow(t);
    }
    printf("\nInorder traversal:\n");
    TtfInorder(t);

    printf("-----------------------\n");
    printf("Printing array from FillFirstN:\n");
    Item arr[MAX_ITEMS];
    TtfFillFirstN(t, arr, nItems);
    for (int i = 0; i < nItems; i++) {
        ItemShow(arr[i]);
        printf("\n");
    }

    TtfFree(t);
    return 0;
}
