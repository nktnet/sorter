// Khiet Tam Nguyen (z5313514)
// 31.08.20

// Interface for the Two-Three-Four Tree ADT

#ifndef TTF_TREE_H
#define TTF_TREE_H

#include "Item.h"

typedef struct ttf *Ttf;

// Return a new Two-Three-Four Tree.
Ttf TtfNew(void);

// Inserts the given item into the tree.
void TtfInsert(Ttf t, Item item);

// Display the tree to standard output.
void TtfShow(Ttf t);

// Free all memories associated with the tree
void TtfFree(Ttf t);

// Print items from the tree inorder.
void TtfInorder(Ttf t);

// Free the tree structure, leaving the items
// (if they are pointers) untouched.
void TtfFreeStructureOnly(Ttf t);

// Fill given array with first n items, or max available
// and return the amount filled.
int TtfFillFirstN(Ttf t, Item *arr, int n);

#endif
