// Khiet Tam Nguyen (z5313514)
// 31.08.20

// Red-Black Tree Implementation
// Some Functions ported from AVL Tree. 

// Insert function modified from COMP1927: 
// cs1927/16s2/lecs/week11wed/exercises/TreeLab
// Also COMP2521 20T2.

// For visualisation (Should match TreeShow's output)
// https://www.cs.usfca.edu/~galles/visualization/RedBlack.html

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "RBTree.h"

// For insert function.
#define L(t)   ((t) == NULL ? NULL : (t)->left)
#define R(t)   ((t) == NULL ? NULL : (t)->right)
#define red(t) ((t) != NULL && (t)->colour == RED)

// For printing colours to stdout.
#define COLO_RED "\033[1;31m"
#define COLO_ORIGINAL "\033[0m"
#define PRINT_COLO(C) printf("%s",(C))

typedef enum {
    RED, 
    BLACK
} Colour;

typedef struct node *Node;
struct node {
    Item item;
    Node left;
    Node right;
    Colour colour;
};

struct rbtree {
    Node root;
};

////////////////////////////////////////////////////////////////////////

// Helper functions
static void doFree(Node n);
static void doFreeStructureOnly(Node n);
static Node rotateLeft(Node n);
static Node rotateRight(Node n);
static Node doInsert(Node n, Item item, bool inRight);
static Node newNode(Item item);
static Item findFloor(Node n, Item item);
static Item findCeil(Node n, Item item);
static void doFillFirstN(Node n, Item *arr, int size, int *i);
static Node doCopyRBTreeNode(Node n);
static Node findKthNode(Node n, int k, int *counter);

typedef unsigned long long uint64;
static void doList(Node n);
static void doShow(Node n, int level, uint64 arms);

////////////////////////////////////////////////////////////////////////

// Creates a new, empty tree.
RBTree RBTreeNew(void) {
    RBTree t = malloc(sizeof(struct rbtree));
    assert(t);
    t->root = NULL;
    return t;
}

// Frees all memory associated with the given tree
void RBTreeFree(RBTree t) {
    if (!t) return;
    doFree(t->root);
    free(t);
}

// Free the tree structure, leaving the items untouched.
void RBTreeFreeStructreOnly(RBTree t) {
    if (!t) return;
    doFreeStructureOnly(t->root);
    free(t);
}

////////////////////////////////////////////////////////////////////////

// Insert an item into the tree.
void RBTreeInsert(RBTree t, Item item) {
    assert(t);
    t->root = doInsert(t->root, item, false);
    assert(t->root);
    t->root->colour = BLACK;
}

////////////////////////////////////////////////////////////////////////

// Return the highest valued item below the given Item.
Item RBTreeFloor(RBTree t, Item item) {
    if (!t || !item) return NULL;
    return findFloor(t->root, item);
}

// Return the lowest valued item below the given Item.
Item RBTreeCeiling(RBTree t, Item item) {
    if (!t || !item) return NULL;
    return findCeil(t->root, item);
}

////////////////////////////////////////////////////////////////////////

// Fill the first N slots of the array with the items (in order) 
// from the tree. Return the number of items inserted.
int RBTreeFillFirstN(RBTree t, Item *arr, int n) {
    int i = 0;
    doFillFirstN(t->root, arr, n, &i);
    return i;
}

// Creates a copy of a tree and return its pointer.
RBTree RBTreeCopy(RBTree t) {
    if (!t) return NULL;
    RBTree copyRBTree = RBTreeNew();
    assert(copyRBTree);
    doCopyRBTreeNode(t->root);
    return t;
}

// Get the kth item 'value' in the tree (inorder)
Item RBTreeGetKth(RBTree t, int k) {
    if (!t) return NULL;
    int counter = -1;
    Node kIt = findKthNode(t->root, k, &counter);
    if (kIt) return kIt->item;
    return NULL;
}


////////////////////////////////////////////////////////////////////////

// Display the tree as a list.
void RBTreeList(RBTree t) {
    if (!t) return;
    doList(t->root);
}

// Display the tree structure.
void RBTreeShow(RBTree t) {
    if (!t || !t->root) return;
    doShow(t->root, 0, 0);
}

////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
//                      Helpers                      //
///////////////////////////////////////////////////////

// Returns a new malloced node.
static Node newNode(Item item) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->item = item;
    n->colour = RED;
    n->left = NULL;
    n->right = NULL;
    return n;
}

// Free all nodes in the tree and associated memories.
static void doFree(Node n) {
    if (!n) return;
    doFree(n->left);
    doFree(n->right);
    ItemFree(n->item);
    free(n);
}

// Free all nodes in the tree, but leave item untouched.
static void doFreeStructureOnly(Node n) {
    if (!n) return;
    doFreeStructureOnly(n->left);
    doFreeStructureOnly(n->right);
    free(n);
}

// Recursively inserts the new item into the redblack tree.
// dirRight indicates the direction of the last branch.
static Node doInsert(Node n, Item item, bool dirRight) {
    if (!n) return newNode(item);

    if (red(L(n)) && red(R(n))) {
        // split 4-node.
        n->colour = RED;
        n->left->colour = BLACK;
        n->right->colour = BLACK;
    }
    // Recursive Insertion.
    int cmp = ItemCompare(item, n->item);
    if (cmp < 0) {
        n->left = doInsert(n->left, item, false);
        if (red(n) && red(L(n)) && dirRight) {
            n = rotateRight(n);
        }
        if (red(L(n)) && red(L(L(n)))) {
            // Left Left case.
            n = rotateRight(n);
            n->colour = BLACK;
            n->right->colour = RED;
        }
    } else {
        n->right = doInsert(n->right, item, true);
        if (red(n) && red(R(n)) && !dirRight) {
            n = rotateLeft(n);
        }
        if (red(R(n)) && red(R(R(n)))) { 
            // Right Right Case
            n = rotateLeft(n);
            n->colour = BLACK;
            n->left->colour = RED;
        }
    }
    return n;
}

// Rotates the given subtree left and returns 
// the root of the updated subtree.
static Node rotateLeft(Node n) {
    if (!n || !n->right) return n;
    Node newRoot = n->right;
    n->right = newRoot->left;
    newRoot->left = n;
    return newRoot;
}

// Rotates the given subtree right and returns 
// the root of the updated subtree.
static Node rotateRight(Node n) {
    if (!n || !n->left) return n;
    Node newRoot = n->left;
    n->left = newRoot->right;
    newRoot->right = n;
    return newRoot;
}

///////////////////////////////////////////////////////////////////

// 29.08.20 - Changed to better looking code for floor and ceiling.
// Below Iterative and Recursive approaches are courtesy of 
// Josie Anugerah, Tutor at UNSW COMP2521 20T2

// Find the floor item ITERATIVELY and return that item.
static Item findFloor(Node n, Item item) {
    Node curr = n;
    Node floor = NULL;
    while (curr) {
        if (ItemCompare(item, curr->item) >= 0) {
            floor = curr;
            curr = curr->right;
        } else {
            curr = curr->left;
        }
    }
    return (floor) ? floor->item : NULL;
}

// Find the ceiling item RECURSIVELY and return that item.
static Item findCeil(Node n, Item item) {
    if (!n) return NULL;
    if (ItemCompare(item, n->item) <= 0) {
        Item left = findCeil(n->left, item);
        if (!left) return n->item;
        return left;
    }
    return findCeil(n->right, item);
}

///////////////////////////////////////////////////////////////////

// Recursive function to copy nodes from a tree.
static Node doCopyRBTreeNode(Node n) {
    if (!n) return NULL;
    Node copyNode = newNode(n->item);
    copyNode->left = doCopyRBTreeNode(n->left);
    copyNode->right = doCopyRBTreeNode(n->right);
    return copyNode;
}

// Fill the given array with "size" items, or the max possible.
static void doFillFirstN(Node n, Item *arr, int size, int *i) {
    if (!n) return;
    if (*i > size) return;
    doFillFirstN(n->left, arr, size, i);
    arr[*i] = n->item;
    *i += 1;
    doFillFirstN(n->right, arr, size, i);
}

// Find the kth valued node in the tree.
static Node findKthNode(Node n, int k, int *counter) {
    if (!n) return NULL;
    Node left = findKthNode(n->left, k, counter);
    *counter += 1;
    if (left) return left;
    if (*counter == k) return n;
    return findKthNode(n->right, k, counter);
}

////////////////////////////////////////////////////////////////////////

// Recursively show the tree as a list.
static void doList(Node n) {
    if (!n) return;
    doList(n->left);
    ItemShow(n->item);
    printf("\n");
    doList(n->right);
}

// This  function  uses a hack to determine when to draw the arms of the
// tree and relies on the tree being reasonably balanced. Don't try to
// use this function if the tree is not an AVL tree!
// >> Tam: Modified to work with RedBlack Tree (just added colours).
static void doShow(Node n, int level, uint64 arms) {
    if (n == NULL) return;

    if (n->colour == RED) PRINT_COLO(COLO_RED);
    ItemShow(n->item);
    PRINT_COLO(COLO_ORIGINAL);

    printf("\n");

    if (n->left != NULL) {
        for (int i = 0; i < level; i++) {
            if ((1LLU << i) & arms) {
                printf("│     ");
            } else {
                printf("      ");
            }
        }
        printf("%s", n->right != NULL ? "┝━╸L: " : "┕━╸L: ");
        if (n->right != NULL) {
            arms |= (1LLU << level);
        } else {
            arms &= ~(1LLU << level);
        }
        doShow(n->left, level + 1, arms);
    }
    if (n->right != NULL) {
        for (int i = 0; i < level; i++) {
            if ((1LLU << i) & arms) {
                printf("│     ");
            } else {
                printf("      ");
            }
        }
        printf("┕━╸R: ");
        arms &= ~(1LLU << level);
        doShow(n->right, level + 1, arms);
    }
}
