// Khiet Tam Nguyen (z5313514)
// 31.08.20

// Main function to test red-black tree.

#include <stdio.h>

#include "RBTree.h"
#define MAX_LINE 30

// Stop after this many items.
#define TEST_MAX_TREE 1000

int main(void) {
    RBTree t = RBTreeNew();
    int n = 0;
    Item it; 
	while ((it = ItemRead()) && n < TEST_MAX_TREE) {
        RBTreeInsert(t, it);
        printf("-----------------------------------\n");
        RBTreeShow(t);
        n++;
    }
/*
    printf("-----------------------------------\n");
    printf("RBTree Copy\n");
    RBTree copyRBTree = RBTreeCopy(t);
    RBTreeShow(copyRBTree);
    RBTreeFree(copyRBTree);
    printf("-----------------------------------\n");
*/

/*
    printf("RBTree Fill First N\n");
    Item arr[n];
    int nReturned = RBTreeFillFirstN(t, arr, n);
    for (int i = 0; i < nReturned; i++) {
        ItemShow(arr[i]);
        printf(" ");
    }
    printf("\n");
    RBTreeFree(t);
*/

/*
    printf("RBTree floor and ceiling\n");
    while ((it = ItemRead())) {
        Item ceil = RBTreeCeiling(t, it);
        Item floor = RBTreeFloor(t, it);

        printf("Ceil: ");
        ItemShow(ceil);

        printf("Item: ");
        ItemShow(it);

        printf("Floor: ");
        ItemShow(floor);

        printf("\n");
    }
*/
/*
    printf("Item at kth position\n");
    while (true) {
        int k;
        scanf("%d", &k);
        Item kIt = RBTreeGetKth(t, k);
        printf("Item at position %d is: ", k);
        ItemShow(kIt);
        printf("\n");

    }
*/
    return 0;
}
