// Khiet Tam Nguyen (z5313514)
// 28.08.20

// Interface for the AvlRBTree ADT

#ifndef RBTREE_H
#define RBTREE_H

#include "Item.h"

typedef struct rbtree *RBTree;

// Creates a new empty tree
RBTree RBTreeNew(void);

// Frees all memory associated with the given tree
void RBTreeFree(RBTree t);

// Free tree structure, but not memories of items.
void RBTreeFreeStructreOnly(RBTree t);

// Inserts copy of item into tree. 
void RBTreeInsert(RBTree t, Item item);

// Return latest item below given time, or NULL.
Item RBTreeFloor(RBTree t, Item item);

// Return earliest item after given time, or NULL.
Item RBTreeCeiling(RBTree t, Item item);

// Fill given array with first n items, or max available
// and return the amount filled.
int RBTreeFillFirstN(RBTree t, Item *arr, int n);

// Get the kth item 'value' in the tree (inorder)
Item RBTreeGetKth(RBTree t, int k);

// Copy a given tree up to the given depth
RBTree RBTreeCopy(RBTree t);

// Lists all the items in the tree, one per line
void RBTreeList(RBTree t);

// Shows the tree structure
void RBTreeShow(RBTree t);

////////////////////////////////////////////////////////////////////////

#endif
