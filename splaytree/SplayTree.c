// Khiet Tam Nguyen (z5313514)

// 30.08.20 - First written.

// Splay Splay ADT implementations

// Adapted code from GeeksToGeeks and
// COMP2521 Lecture Slides. Some functions
// were ported from from AVL Tree ADT.

#include <stdio.h>
#include <stdlib.h>

#include "SplayTree.h"

typedef struct node *Node;
struct node {
    Item item;
    Node left;
    Node right;
};

struct splay {
    Node root;
};

////////////////////////////////////////////////

// Helper functions
static Node newNode(Item item);
static Node splay(Node n, Item item);
static Node rotateLeft(Node n);
static Node rotateRight(Node n);
static void doFree(Node n);
static void doFreeStructureOnly(Node n);
static Node doInsert(Node n, Item item);
static Item findFloor(Node n, Item item);
static Item findCeil(Node n, Item item);
static void doFillFirstN(Node n, Item *arr, int size, int *i);
static Node doCopySplayNode(Node n);
static Node findKthNode(Node n, int k, int *counter);

typedef unsigned long long uint64;
static void doList(Node n);
static void doShow(Node n, int level, uint64 arms);

////////////////////////////////////////////////

Splay SplayNew() {
    Splay t = malloc(sizeof(struct splay));
    assert(t);
    t->root = NULL;
    return t;
}

// Free the tree and the items within it.
void SplayFree(Splay t) {
    if (!t) return;
    doFree(t->root);
    free(t);
}

// Only free the tree, leaving the 
// items untouched if they are pointers.
void SplayFreeStructreOnly(Splay t) {
    if (!t) return;
    doFreeStructureOnly(t->root);
    free(t);
}

// Search for the given item (also rearranges tree).
Node search(Node n, Item item) {
    return splay(n, item);
}

// Insert a new item into the tree.
void SplayInsert(Splay t, Item item)  {  
    assert(t);
    t->root = doInsert(t->root, item);
}  

// Return the highest valued item below the given Item.
Item SplayFloor(Splay t, Item item) {
    if (!t || !item) return NULL;
    return findFloor(t->root, item);
}

// Return the lowest valued item below the given Item.
Item SplayCeiling(Splay t, Item item) {
    if (!t || !item) return NULL;
    return findCeil(t->root, item);
}

// Fill the first N slots of the array with the items (in order) 
// from the tree. Return the number of items inserted.
int SplayFillFirstN(Splay t, Item *arr, int n) {
    int i = 0;
    doFillFirstN(t->root, arr, n, &i);
    return i;
}

// Creates a copy of a tree and return its pointer.
Splay SplayCopy(Splay t) {
    if (!t) return NULL;
    Splay copySplay = SplayNew();
    assert(copySplay);
    doCopySplayNode(t->root);
    return t;
}

// Get the kth item 'value' in the tree (inorder)
Item SplayGetKth(Splay t, int k) {
    if (!t) return NULL;
    int counter = -1;
    Node kIt = findKthNode(t->root, k, &counter);
    if (kIt) return kIt->item;
    return NULL;
}

// Display the splay tree to stdout.
void SplayList(Splay t) {
    if (!t) return;
    doList(t->root);
}

// Display the tree structure of the splay tree to stdout.
void SplayShow(Splay t) {
    if (!t || !t->root) return;
    doShow(t->root, 0, 0);
}

///////////////////////////////////////////////////////
//                      Helpers                      //
///////////////////////////////////////////////////////

// Creates a new malloced node.
static Node newNode(Item item) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->item = item;
    n->left = NULL;
    n->right = NULL;
    return n;
}

// Recursively look for an item and move it (if found)
// or the last visited item to the n.
static Node splay(Node n, Item item) { 
    if (!n || ItemCompare(n->item, item) == 0) {
        return n; 
    }
    if (item < n->item) { 
        // Searching left, exit on base case.
        if (n->left == NULL) return n; 
        if (ItemCompare(item, n->left->item) < 0) { 
            // left left.
            n->left->left = splay(n->left->left, item); 
            n = rotateRight(n); 
        } else if (ItemCompare(item, n->left->item) > 0) { 
            // left right.
            n->left->right = splay(n->left->right, item); 
            if (n->left->right != NULL) {
                n->left = rotateLeft(n->left); 
            }
        } 
        // Second rotation
        return (!n->left) ? n : rotateRight(n); 
    } else { 
        // Searching right, exit on base case.
        if (!n->right) return n; 

        if (ItemCompare(n->right->item, item) > 0) { 
            // Right left.
            n->right->left = splay(n->right->left, item); 
            if (n->right->left != NULL) {
                n->right = rotateRight(n->right); 
            }
        } else if (ItemCompare(n->right->item, item) < 0) { 
            // Left right.
            n->right->right = splay(n->right->right, item); 
            n = rotateLeft(n); 
        } 
        // Second rotation
        return (!n->right) ? n : rotateLeft(n); 
    } 
} 

// Rotate the subtree left and return
// the new subn.
static Node rotateLeft(Node n) {
    Node newRoot = n->right;
    n->right = newRoot->left;
    newRoot->left = n;
    return newRoot;
}

// Rotate the subtree right and return
// the new subn.
static Node rotateRight(Node n) {
    Node newRoot = n->left;
    n->left = newRoot->right;
    newRoot->right = n;
    return newRoot;
}

// Free all nodes in the tree and associated memories.
static void doFree(Node n) {
    if (!n) return;
    doFree(n->left);
    doFree(n->right);
    ItemFree(n->item);
    free(n);
}

// Free all nodes in the tree, but leave item untouched.
static void doFreeStructureOnly(Node n) {
    if (!n) return;
    doFreeStructureOnly(n->left);
    doFreeStructureOnly(n->right);
    free(n);
}

// Recursively insert the new item into the tree, and
// returning the new n.
static Node doInsert(Node n, Item item) {  
    if (!n) return newNode(item);  
    if (ItemCompare(item, n->item) < 0) {
        if (!n->left) {
            n->left = newNode(item);
        } else if (ItemCompare(item, n->left->item) < 0) {
            // Left of left
            n->left->left = doInsert(n->left->left, item);
            n = rotateRight(n);
        } else {
            // Right of Left
            n->left->right = doInsert(n->left->right, item);
            n->left = rotateLeft(n->left);
        }
        return rotateRight(n);
    } else {
        // ItemCompare(item, n->item) >= 0.
        if (!n->right) {
            n->right = newNode(item);
        } else if (ItemCompare(item, n->right->item) < 0) {
            // Left of Right.
            n->right->left = doInsert(n->right->left, item);
            n->right = rotateRight(n->right);
        } else {
            // Right of right.
            n->right->right = doInsert(n->right->right, item);
            n = rotateLeft(n);
        }
        return rotateLeft(n);
    }
}

///////////////////////////////////////////////////////////////////

// 29.08.20 - Changed to better looking code for floor and ceiling.
// Below Iterative and Recursive approaches are courtesy of 
// Josie Anugerah, Tutor at UNSW COMP2521 20T2

// Find the floor item ITERATIVELY and return that item.
static Item findFloor(Node n, Item item) {
    Node curr = n;
    Node floor = NULL;
    while (curr) {
        if (ItemCompare(item, curr->item) >= 0) {
            floor = curr;
            curr = curr->right;
        } else {
            curr = curr->left;
        }
    }
    return (floor) ? floor->item : NULL;
}

// Find the ceiling item RECURSIVELY and return that item.
static Item findCeil(Node n, Item item) {
    if (!n) return NULL;
    if (ItemCompare(item, n->item) <= 0) {
        Item left = findCeil(n->left, item);
        if (!left) return n->item;
        return left;
    }
    return findCeil(n->right, item);
}

///////////////////////////////////////////////////////////////////

// Recursive function to copy nodes from a tree.
static Node doCopySplayNode(Node n) {
    if (!n) return NULL;
    Node copyNode = newNode(n->item);
    copyNode->left = doCopySplayNode(n->left);
    copyNode->right = doCopySplayNode(n->right);
    return copyNode;
}

// Fill the given array with "size" items, or the max possible.
static void doFillFirstN(Node n, Item *arr, int size, int *i) {
    if (!n) return;
    if (*i > size) return;
    doFillFirstN(n->left, arr, size, i);
    arr[*i] = n->item;
    *i += 1;
    doFillFirstN(n->right, arr, size, i);
}

// Find the kth valued node in the tree.
static Node findKthNode(Node n, int k, int *counter) {
    if (!n) return NULL;
    Node left = findKthNode(n->left, k, counter);
    *counter += 1;
    if (left) return left;
    if (*counter == k) return n;
    return findKthNode(n->right, k, counter);
}

////////////////////////////////////////////////////////////////////////

// Recursively show the tree as a list.
static void doList(Node n) {
    if (!n) return;
    doList(n->left);
    ItemShow(n->item);
    printf("\n");
    doList(n->right);
}

// This  function  uses a hack to determine when to draw the arms of the
// tree and relies on the tree being reasonably balanced. Don't try to
// use this function if the tree is not an AVL tree!
static void doShow(Node n, int level, uint64 arms) {
    if (n == NULL) return;
    ItemShow(n->item);
    printf("\n");

    if (n->left != NULL) {
        for (int i = 0; i < level; i++) {
            if ((1LLU << i) & arms) {
                printf("│     ");
            } else {
                printf("      ");
            }
        }
        printf("%s", n->right != NULL ? "┝━╸L: " : "┕━╸L: ");
        if (n->right != NULL) {
            arms |= (1LLU << level);
        } else {
            arms &= ~(1LLU << level);
        }
        doShow(n->left, level + 1, arms);
    }
    if (n->right != NULL) {
        for (int i = 0; i < level; i++) {
            if ((1LLU << i) & arms) {
                printf("│     ");
            } else {
                printf("      ");
            }
        }
        printf("┕━╸R: ");
        arms &= ~(1LLU << level);
        doShow(n->right, level + 1, arms);
    }
}
