// Khiet Tam Nguyen (z5313514)
// 28.08.20

// Interface for the SplaySplay ADT

#ifndef SPLAY_TREE_H
#define SPLAY_TREE_H

#include "Item.h"

typedef struct splay *Splay;

// Creates a new empty tree
Splay SplayNew(void);

// Frees all memory associated with the given tree
void SplayFree(Splay t);

// Free tree structure, but not memories of items.
void SplayFreeStructreOnly(Splay t);

// Inserts copy of item into tree. 
void SplayInsert(Splay t, Item item);

// Return latest item below given time, or NULL.
Item SplayFloor(Splay t, Item item);

// Return earliest item after given time, or NULL.
Item SplayCeiling(Splay t, Item item);

// Fill given array with first n items, or max available
// and return the numbers filled.
int SplayFillFirstN(Splay t, Item *arr, int n);

// Get the kth item 'value' in the tree (inorder)
Item SplayGetKth(Splay t, int k);

// Copy a given tree
Splay SplayCopy(Splay t);

// Lists all the items in the tree, one per line
void SplayList(Splay t);

// Shows the tree structure
void SplayShow(Splay t);

////////////////////////////////////////////////////////////////////////

#endif
