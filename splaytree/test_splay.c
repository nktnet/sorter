// Khiet Tam Nguyen (z5313514)
// 30.08.20

// Program to test splay tree

#include <stdio.h>

#include "SplayTree.h"

#define MAX_LINE 30

// Stop after this many items.
#define TEST_MAX_TREE 100

int main(void) {
    Splay t = SplayNew();
    int n = 0;
    Item it; 
	while ((it = ItemRead()) && n < TEST_MAX_TREE) {
        SplayInsert(t, it);
        printf("-----------------------------------\n");
        SplayShow(t);
        n++;
    }
/*
    printf("-----------------------------------\n");
    printf("Splay Copy\n");
    Splay copySplay = SplayCopy(t);
    SplayShow(copySplay);
    SplayFree(copySplay);
    printf("-----------------------------------\n");
*/

/*
    printf("Splay Fill First N\n");
    Item arr[n];
    int nReturned = SplayFillFirstN(t, arr, n);
    for (int i = 0; i < nReturned; i++) {
        ItemShow(arr[i]);
        printf(" ");
    }
    printf("\n");
    SplayFree(t);
*/

/*
    printf("Splay floor and ceiling\n");
    while ((it = ItemRead())) {
        Item ceil = SplayCeiling(t, it);
        Item floor = SplayFloor(t, it);

        printf("Ceil: ");
        ItemShow(ceil);

        printf("Item: ");
        ItemShow(it);

        printf("Floor: ");
        ItemShow(floor);

        printf("\n");
    }
*/
/*
    printf("Item at kth position\n");
    while (true) {
        int k;
        scanf("%d", &k);
        Item kIt = SplayGetKth(t, k);
        printf("Item at position %d is: ", k);
        ItemShow(kIt);
        printf("\n");
    }
*/


    return 0;
}
