// Khiet Tam Nguyen (z5313514)
// 20.08.20

// Interface for shape sequences

#include "shape.h"

// 1, 3, 6, 10, 15, 21, 28, etc
bool is_triangle(int n) {
    if (n < 0) return false;
    int sum = 0;
    int i = 1;
    while (sum < n) sum += i++;
    return (sum == n);
}

// 0, 1, 4, 9, 16, 25, 36, etc
bool is_square(int n) {
    if (n < 0) return false;
    int s = 0;
    for (int i = 0; (s = i * i) < n; i++);
    return (s == n);
}

// 1, 8, 27, 64, 125, 216, etc
// -1, -8, -27, -64, -125, etc
bool is_cube(int n) {
    if (n < 0) n *= -1;
    int c;
    for (int i = 0; (c = i * i * i) < n; i++);
    return (c == n);
}

// 1, 5, 12, 22, 35, 51, 70, etc
bool is_pentagonal(int n) {
    int p = 1;
    for (int i = 1; p < n; i++)
        p = (3*i*i - i) / 2;
    return (p == n);
}

// 1, 6, 15, 28, 45, 66, 91, etc
bool is_hexagonal(int n) {
    int h = 1;
    for (int i = 1; h < n; i++)
        h = 2*i*i - i;
    return (h == n);
}

