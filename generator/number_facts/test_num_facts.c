// Khiet Tam Nguyen (z5313514)
// 20.08.20

// Program to print random sequence facts about 
// a number. Written just for fun.

#include <stdio.h>
#include "shape.h"
#include "special.h"

int main(void) {
    int n;
    printf("Enter your numbers\n");
    while (scanf("%d", &n) == 1) {
        if (n < 0) printf("> negative\n"); else if (n > 0) printf("> positive\n"); else printf("> neutral\n");
        if (n % 2) printf("> odd\n"); else printf("> even\n");
        if (is_prime(n)) printf("> prime\n");
        if (is_triangle(n)) printf("> triangle\n");
        if (is_square(n)) printf("> square\n");
        if (is_cube(n)) printf("> cube\n");
        if (is_fibonacci(n)) printf("> fibonacci\n");
        if (is_factorial(n)) printf("> factorial\n");
        if (is_pentagonal(n)) printf("> pentagonal\n");
        if (is_hexagonal(n)) printf("> hexagonal\n");
        if (is_lazy_caterer(n)) printf("> lazy caterer\n");
        if (is_magic_square(n)) printf("> magic square\n");
        if (is_interior_angle_sum(n)) printf("> interior angle sum\n");
        printf("------------------\n");
    }
    return 0;
}
