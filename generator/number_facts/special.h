// Khiet Tam Nguyen (z5313514)
// 20.08.20

#ifndef SPECIAL_H
#define SPECIAL_H

#include <stdbool.h>

// 2, 3, 5, 7, 11, 13, 17, etc
bool is_prime(int n);

// 1, 1, 2, 3, 5, 8, 13, etc
bool is_fibonacci(int n);

// 1, 2, 6, 24, 120, 720, etc
bool is_factorial(int n);

// The Lazy Caterer’s Sequence:
// Maximum number of pieces formed when slicing a 
// pancake (circle) with n cuts
// 1, 2, 4, 7, 11, 16
bool is_lazy_caterer(int n);

// 15, 34, 65, 111, 175, 260, etc
bool is_magic_square(int n);

// 180, 360, 540, 720, 900, etc
bool is_interior_angle_sum(int n);

#endif
