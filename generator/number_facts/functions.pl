#!/usr/bin/perl -w

# Khiet Tam Nguyen (z5313514)
# 20.08.20

sub processFile {
    my ($file) = @_;
    open my $F, $file or die;

    our (%prototype, %defined, %calls);

    while ($line = <$F>) {
        chomp $line;
        if ($line =~ /^\s*(\w+\s+(\w+)\s*\(.*\))\s*/) {
            my $proto = "$1";
            my $func = "$2";
            $prototype{$func} = $proto;
            $defined{$func} = "$file:$.";
        } elsif ($line =~ /(\w+)\(.*\)/) {
            $func = "$1";
            $called{$func} .= "$file:$. ";
        }
    }
    close $F;
}


for $file (glob "*.c") {
    processFile($file);
}

for $func (sort keys %prototype) {
    print 
        "function: $func\n",
        "\tprototype:\t $prototype{$func}\n",
        "\tdefined:\t $defined{$func}\n";

    print "\tcalled:\t\t $called{$func}\n" if defined $called{$func};
}
