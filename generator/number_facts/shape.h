// Khiet Tam Nguyen (z5313514)
// 20.08.20

#ifndef SHAPE_H
#define SHAPE_H

#include <stdbool.h>

// 1, 3, 6, 10, 15, 21, 28, etc
bool is_triangle(int n);

// 0, 1, 4, 9, 16, 25, 36, etc
bool is_square(int n);

// 1, 8, 27, 64, 125, 216, etc
// -1, -8, -27, -64, -125, etc
bool is_cube(int n);

// 1, 5, 12, 22, 35, 51, 70, etc
bool is_pentagonal(int n);

// 1, 6, 15, 28, 45, 66, 91, etc
bool is_hexagonal(int n);

#endif
