// Khiet Tam Nguyen (z5313514)
// 20.08.20

// Interface for special sequences

#include "special.h"

// 2, 3, 5, 7, 11, 13, 17, etc
bool is_prime(int n) {
    for (int i = 2; i < n; i++)
        if (n % i == 0) return false;
    return (n > 1);
}

// 1, 1, 2, 3, 5, 8, 13, etc
bool is_fibonacci(int n) {
    int fibo1, fibo2 = fibo1 = 1;
    int fibo3 = 2;
    while (fibo2 < n) {
        fibo3 = fibo1 + fibo2;
        fibo1 = fibo2;
        fibo2 = fibo3;
    }
    return (fibo2 == n); 
}

// 1, 2, 6, 24, 120, 720, etc
bool is_factorial(int n) {
    if (n < 1) return false;
    for (int i = 1; n % i == 0; i++) n /= i;
    return (n == 1);
}

// The Lazy Caterer’s Sequence:
// Maximum number of pieces formed when slicing a 
// pancake (circle) with n cuts
// 1, 2, 4, 7, 11, 16
bool is_lazy_caterer(int n) {
    int l = 1;
    for (int i = 1; l < n; i++)
        l = (i*i + i + 2) / 2;
    return (l == n);
}

// 15, 34, 65, 111, 175, 260, etc
bool is_magic_square(int n) {
    int m = 15;
    for (int i = 1; m < n; i++)
        m = (i*i*i + i) / 2;
    return (m == n);
}

// 180, 360, 540, 720, 900, etc
bool is_interior_angle_sum(int n) {
    return (n % 180 == 0);
}
