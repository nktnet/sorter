// Khiet Tam Nguyen (z5313514)
// 26.08.20

// Number and Sequence Generator for testing 
// sort programs.
// EDIT: Oops I got carried away and added a 
// bunch of useless commands, haha.

////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

////////////////////////////////////////////////////////

#define MAX_WORD                            3
#define MAX_SIZE                     20000000
#define MAX_FACTORIAL                      16
#define MAX_FIBONACCI                      46
#define MAX_MAGIC_SQUARE                 1288
#define MAX_LAZY_CATERER                46340
#define MAX_INTERIOR_ANGLE           MAX_SIZE
#define MAX_TRIANGLE_SIZE               65535
#define MAX_SQUARE_SIZE                 46341
#define MAX_CUBE_SIZE                    3182
#define MAX_PENTA_SIZE                  26755
#define MAX_HEXA_SIZE                   32768

////////////////////////////////////////////////////////

// Main Printing Functions
void print_usage(char *program);
void print_ascending(int size);
void print_descending(int size);
void print_random_unique(int size);
void print_random_with_duplicates(int size);
void print_same_key(int size);
void print_bubble_up_test(int size);
void print_bubble_down_test(int size);
void print_palindrome(int size);
void print_fibonacci(int size);
void print_factorial(int size);
void print_lazy_caterer(int size);
void print_magic_square(int size);
void print_interior_angle_sum(int size);
void print_triangular(int size);
void print_square(int size);
void print_cubic(int size);
void print_pentagonal(int size);
void print_hexagonal(int size);

////////////////////////////////////////////////////////

// Helpers
static void print_random_char(void);
static void fix_overflow_ascending(int *x, int *y, int *z);
static void fix_overflow_descending(int *x, int *y, int *z);
static void print_bubble_body(int size);

////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
    switch(argc) {
    case 4:     srand(atoi(argv[3]));                   break;
    case 3:     srand(time(NULL));                      break;
    default:    print_usage(argv[0]);                   exit(1);
    }

    int size = atoi(argv[2]);
    if (size > MAX_SIZE) size = MAX_SIZE;

    switch(argv[1][0]) {
    case 'a':   print_ascending(size);                  break;
    case 'd':   print_descending(size);                 break;
    case 'r':   print_random_unique(size);              break; 
    case 'R':   print_random_with_duplicates(size);     break;
    case 'S':   print_same_key(size);                   break;
    case 'b':   print_bubble_up_test(size);             break;
    case 'B':   print_bubble_down_test(size);           break;
    case 'P':   print_palindrome(size);                 break;
    case 'f':   print_fibonacci(size);                  break;
    case 'F':   print_factorial(size);                  break;
    case 'l':   print_lazy_caterer(size);               break;
    case 'm':   print_magic_square(size);               break;
    case 'i':   print_interior_angle_sum(size);         break;
    case 't':   print_triangular(size);                 break;
    case 's':   print_square(size);                     break;
    case 'c':   print_cubic(size);                      break;
    case 'p':   print_pentagonal(size);                 break;
    case 'h':   print_hexagonal(size);                  break;
    default:    print_usage(argv[0]);                   exit(1);
    }
    return 0;
}

// Print a unique ascending list.
void print_ascending(int size) {
    int x, y, z = y = x = 'a';
    for (int i = 0; i < size; i++) {
        printf("%d %c%c%c\n", i, x, y, z++);
        fix_overflow_ascending(&x, &y, &z);
    }
}

// Print a unique descending list.
void print_descending(int size) {
    int x, y, z = y = x = 'z';
    for (int i = size - 1; i >= 0; i--) {
        printf("%d %c%c%c\n", i, x, y, z--);
        fix_overflow_descending(&x, &y, &z);
    }
}

// Print random unique numbers
void print_random_unique(int size) {
#if 0
    // Inefficient - 26.08.20
    int *exist = calloc(size, sizeof(int));
    int i = 0;
    while (i < size) {
        int rN = rand() % size;
        if (exist[rN]) continue;
        exist[rN] = true;
        printf("%d ", rN);
        print_random_char();
        i++;
    }
    free(exist);
#endif
#if 0
    // First attempt, made it worse - 04.09.20
    int *arr = malloc(size * sizeof(int)); 
    for (int i = 0 ; i < size; i++) {
        arr[i] = -1;
    }

    for (int i = 0; i < size; i++) {
        int rN = rand() % size;
        while (arr[rN] != -1) {
            // Already have a slot.
            int increment = rand() % size;
            rN = (rN + increment) % size;
        }
        arr[rN] = i;
    }
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
        print_random_char();
    }
    free(arr);
#endif
#if 1
    // Best approach, uniformly shuffle.
    int *arr = malloc(size * sizeof(int)); 
    // Initialise sorted sequence.
    for (int i = 0; i < size; i++) {
        arr[i] = i;
    }
    // Uniform shuffle
    for (int i = size - 1; i >= 0; i--) { 
        // Pick a random index from 0 to i 
        int j = rand() % (i + 1); 
        // Swap elements in the array.
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    } 
    // Print array.
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
        print_random_char();
    }
#endif
}

// Print random numbers with possible duplicates 
void print_random_with_duplicates(int size) {
    for (int i = 0; i < size; i++) {
        int rN = rand() % size;
        printf("%d ", rN);
        print_random_char();
    }
}

// Print the same number repeatedly.
void print_same_key(int size) {
    int x, y, z = y = x = 'a';
    for (int i = 0; i < size; i++) {
        printf("1 %c%c%c\n", x, y, z++);
        fix_overflow_ascending(&x, &y, &z);
    }
}

// Prints the pattern {3, 3, 3, ..., 3, 2} which
// helps to differentiate between bubble up (O(n^2))
// and insertion sort (O(n));
void print_bubble_up_test(int size) {
    print_bubble_body(size);
    printf("2 aaa\n");
}

// Prints pattern {4, 3, 3, 3, ..., 3} for
// the same reason as bubble up.
void print_bubble_down_test(int size) {
    printf("4 ccc\n");
    print_bubble_body(size);
}

// Prints the palindrome pattern going forward, 
// e.g. the set {1, 2, 3, 4, 5, 4, 3, 2, 1}
void print_palindrome(int size) {
    int a = (size % 2);
    print_ascending(size / 2 + a);
    print_descending(size / 2);
}

// Prints the first "size" fibonacci numbers if below
// maximum, otherwise print maximum.
void print_fibonacci(int size) {
    if (size > MAX_FIBONACCI) size = MAX_FIBONACCI;
    int fibo1, fibo2 = fibo1 = 1;
    int fibo3 = 2;
    printf("%d ", fibo1);
    print_random_char();
    for (int i = 0; i < size - 1; i++) {
        printf("%d ", fibo2);
        print_random_char();
        fibo3 = fibo1 + fibo2;
        fibo1 = fibo2;
        fibo2 = fibo3;
    }
}

// Prints the first "size" factorial numbers if below
// maximum, otherwise print maximum.
void print_factorial(int size) {
    if (size > MAX_FACTORIAL) size = MAX_FACTORIAL;
    int f = 1;
    for (int i = 2; i < size + 2; i++) {
        printf("%d ", f);
        f *= i;
        print_random_char();
    }
}

// The Lazy Caterer’s Sequence:
// Maximum number of pieces formed when slicing a 
// pancake (circle) with n cuts
// 1, 2, 4, 7, 11, 16
void print_lazy_caterer(int size) {
    if (size > MAX_LAZY_CATERER) size = MAX_LAZY_CATERER;
    int l = 1;
    for (int i = 1; i < size + 1; i++) {
        l = (i*i + i + 2) / 2;
        printf("%d ", l);
        print_random_char();
    }
}

// Magic square sequence: Row, Column and Diagonal 
// adds up to a number in {15, 34, 65, 111, 175, 260, etc}
void print_magic_square(int size) {
    if (size > MAX_MAGIC_SQUARE) size = MAX_MAGIC_SQUARE;
    for (int i = 3; i < size + 3; i++) {
        printf("%d ", (i*i*i + i) / 2);
        print_random_char();
    }
}

// Interior angle sum of polygons, {180, 360, 540, 720, etc}
void print_interior_angle_sum(int size) {
    if (size > MAX_INTERIOR_ANGLE) size = MAX_INTERIOR_ANGLE;
    for (int i = 1; i < size + 1; i++) {
        printf("%d ", 180 * i);
        print_random_char();
    }
}

// Print triangle numbers:
// 1, 3, 6, 10, 15, 21, 28, etc
void print_triangular(int size) {
    if (size > MAX_TRIANGLE_SIZE) size = MAX_TRIANGLE_SIZE;
    int t = 1;
    for (int i = 2; i < size + 2; i++) {
        printf("%d ", t);
        print_random_char();
        t += i;
    }
}

// Print square numbers:
// 0, 1, 4, 9, 16, 25, 36, etc
void print_square(int size) {
    if (size > MAX_SQUARE_SIZE) size = MAX_SQUARE_SIZE;
    for (int i = 0; i < size; i++) {
        printf("%d ", i * i);
        print_random_char();
    }
}

// Print cubic numbers:
// 1, 8, 27, 64, 125, 216, etc
void print_cubic(int size) {
    if (size > MAX_CUBE_SIZE) size = MAX_CUBE_SIZE;
    for (int i = 0; i < size; i++) {
        printf("%d ", i * i * i);
        print_random_char();
    }
}

// Print pentagonal numbers:
// 1, 5, 12, 22, 35, 51, 70, etc
void print_pentagonal(int size) {
    if (size > MAX_PENTA_SIZE) size = MAX_PENTA_SIZE;
    for (int i = 1; i < size + 1; i++) {
        printf("%d ", (3*i*i - i)/2);
        print_random_char();
    }
}

// Print hexagonal numbers:
// 1, 6, 15, 28, 45, 66, 91, etc
void print_hexagonal(int size) {
    if (size > MAX_HEXA_SIZE) size = MAX_HEXA_SIZE;
    for (int i = 1; i < size + 1; i++) {
        printf("%d ", 2*i*i - i);
        print_random_char();
    }
}

///////////////////////////////////////////////////////

// Fix overflow of letters when printing ascending.
static void fix_overflow_ascending(int *x, int *y, int *z) {
    if (*z > 'z') {
        *z = 'a';
        *y += 1;
    }
    if (*y > 'z') {
        *y = 'a';
        *x += 1;
    }
    if (*x > 'z') {
        *x = *y = *z = 'a';
    }
}

// Fix overflow of letters when printing descending.
static void fix_overflow_descending(int *x, int *y, int *z) {
    if (*z < 'a') {
        *z = 'z';
        *y -= 1;
    }
    if (*y < 'a') {
        *y = 'z';
        *x -= 1;
    }
    if (*x < 'a') {
        *x = *y = *z = 'z';
    }
}

// Print random characters in sets of 3 {abc}.
static void print_random_char(void) {
    for (int j = 0; j < MAX_WORD; j++) {
        int rL = rand() % 26 + 'a';
        printf("%c", rL);
    }
    printf("\n");
}

// Print the tail of bubble sort tests.
static void print_bubble_body(int size) {
    for (int i = 0; i < size - 2; i++) {
        printf("3 bbb\n");
    }
}

///////////////////////////////////////////////////////

// Printing usage/error message to standard error.
void print_usage(char *program) {
    fprintf(
        stderr, 
        "-----------------------------------------------\n"
        "\tUsage: %s  a|d|r|...|h  N  [Seed]\n" 
        "-----------------------------------------------\n"
        "\ta|d|r|R|S|b|B|P|f|F|l|m|i|t|s|c|p|h\n"
        "\t\ta          ascending unique\n"
        "\t\td          descending unique\n"
        "\t\tr          random unique\n"
        "\t\tR          random with duplicates\n"
        "\t\tS          same keys, different values\n"
        "\t\tb          Bubble up O(n^2), Insertion O(n)\n"
        "\t\tB          Bubble down O(n^2), Insertion O(n)\n"
        "\t\tP          Palindrome\n"
        "\t\tf          Fibonacci\n"
        "\t\tF          Factorial\n"
        "\t\tl          Lazy Caterer\n"
        "\t\tm          Magic Square\n"
        "\t\ti          Interior Angle Sum of Polygons\n"
        "\t\tt          Triangular numbers\n"
        "\t\ts          Square numbers\n"
        "\t\tc          Cubic numbers\n"
        "\t\tp          Pentagonal numbers\n"
        "\t\th          Hexagonal numbers\n"
        "-----------------------------------------------\n",
        program
    );
}

//////////////////////////////////////////////////////////
