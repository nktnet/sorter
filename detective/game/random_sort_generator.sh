#!/bin/sh

sA="sortA.sh"
sB="sortB.sh"

echo "Generating random sorting algorithms, $sA and $sB"

printf "#!/bin/sh\n\n" > "$sA"
printf "#!/bin/sh\n\n" > "$sB"

n=47

idA=$((($RANDOM + $$) % $n - 6))
idB=$((($RANDOM + $$ + $idA) % $n - 6))

while [ $idA -eq $idB ]; do
    idB=$((($RANDOM + $$ + idA + idB) % $n - 6))
done

printf "./sorter $idA | egrep -v \"^>\"\n" >> "$sA"
printf "./sorter $idB | egrep -v \"^>\"\n" >> "$sB"

chmod 755 "$sA" "$sB"
