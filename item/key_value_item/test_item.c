// Khiet Tam Nguyen (z5313514)
// 30.08.20

// Test program for item structure

#include <stdio.h>
#include "Item.h"

//////////////////////////////////////////

// Helper
static void display_item_details(Item it);

//////////////////////////////////////////

int main(void) {
    printf("Enter Item 1: ");
    Item it1 = ItemRead();
    printf("Enter Item 2: ");
    Item it2 = ItemRead();

    display_item_details(it1);
    display_item_details(it2);

    printf("Swapping it1 and it2\n");
    ItemSwap(&it1, &it2);
    display_item_details(it1);
    display_item_details(it2);

    int cmp12 = ItemCompare(it1, it2);
    ItemShow(it1);
    if (cmp12 < 0) {
        printf(" is less than ");
    } else if (cmp12 > 0) {
        printf(" is greater than ");
    } else {
        printf(" is equal to ");
    }
    ItemShow(it2);
    printf("\n\n");

    printf("Creating Third item, 3 Tri\n");
    Item it3 = ItemCreate(3, "Tri");
    display_item_details(it3);

    ItemFree(it1);
    ItemFree(it2);
    ItemFree(it3);

    return 0;
}

// Display the item to standard output in more details.
static void display_item_details(Item it) {
    printf("Item: ");
    ItemShow(it);
    printf(
        "  |   Key: %d   |   Value: %s\n", 
        ItemGetKey(it), 
        ItemGetName(it)
    );
}
