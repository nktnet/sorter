// Khiet Tam Nguyen (z5313514)
// 26.08.20

// Item ADT Interface.

#ifndef ITEM_H
#define ITEM_H

#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

// Decide whether to compare just the keys,
// or both the key and (then) values.
#define BOOL_COMPARE_KEY_ONLY 1

// Size of the value (name).
#define MAX_WORD 4
// Item is currently a pointer.
#define UNKNOWN_ITEM NULL
// #define UNKNOWN_ITEM -1

typedef struct item *Item;

// Read value of items from Standard Input and return
// the new item, or NULL if the read fails.
Item ItemRead(void);

// Return a new item with initialised fields
Item ItemCreate(int key, char *name);

// Free all memories associated with the item.
void ItemFree(Item it);

// Return a number equivalent to a->key - b->key.
int ItemCompare(Item a, Item b);

// Get the key of the given item.
int ItemGetKey(Item it);

// Get the name field of the given item.
char *ItemGetName(Item it);

// Trivial function to change the key of an item.
void ItemChangeKey(Item it, int key);

// Trivial function to change the name of an item.
void ItemChangeName(Item it, char *name);

// Macros to shorten writting
#define Key(x) ItemGetKey(x)
#define Name(x) ItemGetName(x)

// Same as ItemCompare, but for Qsort.
int ItemCompareQsort(const void *a, const void *b);

// Swaps the two given items. 
void ItemSwap(Item *a, Item *b);

// Display the item to standard output.
void ItemShow(Item a);

#endif
