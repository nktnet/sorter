// Khiet Tam Nguyen (z5313514)
// 26.08.20 - 29.08.20

// Item ADT Implementation

#include <stdio.h>
#include <string.h>

#include "Item.h"

#define MAX_LINE 30

struct item {
    int key;
    char name[MAX_WORD];
};

// Returns a new, uninitialised item.
static Item ItemNew(void) {
    Item it = malloc(sizeof(struct item));
    assert(it);
    return it;
}

// Read value of items from Standard Input and return the item.
// Will return NULL if the read fails.
Item ItemRead() {
    char line[MAX_LINE];
    if (fgets(line, MAX_LINE, stdin) == NULL) return NULL;

    Item it = ItemNew();
    int n = sscanf(line, "%d %3s", &(it->key), it->name);
    if (n == 1) {
        // Only scanned the number.
        it->name[0] = '\0';
    } else if (n < 1) {
        // Number not scanned.
        fprintf(stderr, "Bad input: %s", line);
        it = NULL;
        // exit(1);
    }
    return it;
}

// Return a new item with initialised fields
Item ItemCreate(int key, char *name) {
    Item it = ItemNew();
    it->key = key;
    strcpy(it->name, name);
    return it;
}

// Free all memories associated with the item.
void ItemFree(Item it) {
    free(it);
}

// Returns a positive number of item a is greater than b,
// 0 if equal and negative otherwise.
int ItemCompare(Item a, Item b) {
#if BOOL_COMPARE_KEY_ONLY
    // Only compare key.
    return (a->key - b->key);
#else
    // Compare keys first, if equal then compare values
    if (a->key - b->key != 0) 
        return a->key - b->key;
    return strcmp(a->name, b->name);
#endif
}

// Same as Item compare, but for Qsort.
int ItemCompareQsort(const void *a, const void *b) {
#if BOOL_COMPARE_KEY_ONLY
    return (*(Item*)a)->key - (*(Item*)b)->key;
#else
    if ((*(Item*)a)->key != (*(Item*)b)->key) 
        return ((*(Item*)a)->key - (*(Item*)b)->key);
    return strcmp((*(Item*)a)->name, (*(Item*)b)->name);
#endif
}

// Get the key of the item.
int ItemGetKey(Item it) {
    assert(it);
    return it->key;
}

// Get the name field of the item.
char *ItemGetName(Item it) {
    assert(it);
    return it->name;
}

// Trivial function to change the key of an item.
void ItemChangeKey(Item it, int key) {
    assert(it);
    it->key = key;
}

// Trivial function to change the name of an item.
void ItemChangeName(Item it, char *name) {
    assert(it);
    strcpy(it->name, name);
}

// Swap two items
void ItemSwap(Item *a, Item *b) {
    Item temp = *a;
    *a = *b;
    *b = temp;
}

// Display the item to standard output.
void ItemShow(Item a) {
    if (!a) printf("UNKNOWN_ITEM");
    else printf("%d %s", a->key, a->name);
}
