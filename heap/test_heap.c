// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Main function to test heap ADT.

#include <stdio.h>

#include "Heap.h"

#define MAX_LINE 30

typedef enum _commands {
    HELP,
    INSERT,
    DELETE, 
    PRINT,
} Command;

int main(void) {
    printf("How many slots? ");
    int N;
    if (scanf("%d", &N) != 1) {
        printf("Required a positive integer\n");
        return 1;
    }
    Heap h = HeapNew(N);
    HeapPrint(h);

    int command;
    char line[MAX_LINE];
    printf("Enter command: ");
    while (scanf("%d", &command) == 1 && fgets(line, MAX_LINE, stdin) != NULL) {
        Item item;

        switch (command) {
        case INSERT:
            item = ItemRead();
            HeapInsert(h, item);
            break;
        case DELETE: 
            item = HeapDelete(h);
            printf("Deleted: ");
            ItemShow(item);
            printf("\n");
            break;
        case PRINT:
            break;
        case HELP:
        default: 
            printf(
                "Usage: <command> [num?]\n"
                "0           HELP\n"
                "1 [num]     INSERT\n"
                "2           DELETE\n"
                "3           PRINT\n"
            );
            break;
        }
        HeapPrint(h);
        printf("Enter command: ");
    }
    HeapFree(h);
    return 0;
}
