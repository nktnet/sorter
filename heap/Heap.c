// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Modified program from John Shepherd's code:
// Slides : http://www.cse.unsw.edu.au/~cs2521/20T2/lecs/heaps/slides.html#s1
// YouTube: https://www.youtube.com/watch?v=TkZ2Few6KnU&feature=youtu.be

// Heap Implementations

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#include "Heap.h"

struct heap {
    Item *items;  
    int  nitems;
    int  nslots;
};

// Helpers
static void fixUp(Item a[], int i);
static void fixDown(Item a[], int i, int N);

// Returns a pointer to a newly generated heap.
Heap HeapNew(int N) {
    Heap new = malloc(sizeof(struct heap));
    Item *arr = malloc((N + 1) * sizeof(Item));
    assert(new != NULL && arr != NULL);
    new->items = arr; // no initialisation needed
    new->nitems = 0;  // counter and index
    new->nslots = N;  // index range 1..N
    return new;
}

// Free the heap and all memories attached to it.
void HeapFree(Heap heap) {
    assert(heap != NULL);
    free(heap->items);
    free(heap);
}

// Insert an element into the heap
void HeapInsert(Heap h, Item key) {
    // is there space in the array?
    assert(h->nitems < h->nslots);
    h->nitems++;
    // add new key at end of array
    h->items[h->nitems] = key;
    // move new key to its correct place
    fixUp(h->items, h->nitems);
}
// Delete a heap.
Item HeapDelete(Heap h) {
    Item top = h->items[1];
    // overwrite first by last
    h->items[1] = h->items[h->nitems];
    h->nitems--;
    // move new root to correct position
    fixDown(h->items, 1, h->nitems);
    return top;
}

// Prints two rows, one for the index starting from
// 1 to N (ignoring 0th), and one for values.
void HeapPrint(Heap h) {
    printf("\n");
    for (int i = 1; i <= h->nslots; i++) {
        if (i > h->nitems) {
            printf("   --   ");
        } else {
            printf(" ");
            ItemShow(h->items[i]);
            printf(" ");
        }
    }
    printf("\n");
}

//////////////////////////////////////////////////////////////

// force value at a[i] into correct position
static void fixUp(Item a[], int i) {
    while (i > 1 && ItemCompare(a[i/2], a[i]) < 0) {
        ItemSwap(&a[i], &a[i/2]);
        // integer division
        i = i/2;  
    }
}

// force value at a[i] into correct position
// note that N gives max index *and* # items
static void fixDown(Item a[], int i, int N) {
    while (2*i <= N) {
        // compute address of left child
        int j = 2*i;
        // choose larger of two children
        if (j < N && ItemCompare(a[j], a[j+1]) < 0) j++;
        if (ItemCompare(a[j], a[i]) < 0) break;
        ItemSwap(&a[i], &a[j]);
        // move one level down the heap
        i = j;
    }
}
