// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Heap Interface

#ifndef HEAP_H
#define HEAP_H

#include "Item.h"

typedef struct heap *Heap;

// Returns a pointer to a newly generated heap.
Heap HeapNew(int N);

// Free the heap and all memories attached to it.
void HeapFree(Heap heap);

// Insert an element into the heap.
void HeapInsert(Heap h, Item item);

// Remove the first item in the heap and return its value.
Item HeapDelete(Heap h);

// Display the heap to standard output.
void HeapPrint(Heap h);

#endif
