# Sorter - C Sorting Program

42 different sorting algorithms implemented in C. This is
indeed the answer to life, the universe and everything!

## Contributors

* Khiet Tam Nguyen (z5313514)
* Wikipedia, GeeksToGeeks, COMP2521-20T2, StackOverflow, etc :).

## List of Algorithms:

To see a list of all the implemented sorting algorithms, run
the command 

    $ ./sorter

with no extra arguments. The output will be:

     ID         ALGORITHM                  VARIANT

     -6         Bogo Sort                  (Original)
     -5         Bogo Sort                  (Pancake)
     -4         Bogo Sort                  (Permutation)
     -3         Bogo Sort                  (Pseudo/Bozo)
     -2         Bogo Sort                  (Slow Sort)
     -1         Bogo Sort                  (Stooge Sort)
      0         Bubble Sort                (Adaptive)
      1         Bubble Sort                (Oblivious)
      2         Cocktail Shaker Sort
      3         Comb Sort
      4         Counting Sort              (Non-comparison, Stable)
      5         Cycle Sort
      6         Gnome Sort                 (Optimised)
      7         Gnome Sort                 (Vanilla)
      8         Heap Sort                  (ADT)
      9         Heap Sort                  (Inplace)
     10         Insertion Sort             (Binary Search)
     11         Insertion Sort             (Bitonic)
     12         Insertion Sort             (Vanilla)
     13         Introspective Sort         (Insertion, Heap, Quick)
     14         Merge Sort                 (Bottom Up, Iterative)
     15         Merge Sort                 (Naive Inplace)
     16         Merge Sort                 (Optimised Inplace)
     17         Merge Sort                 (Timsort, with Insertion)
     18         Merge Sort                 (Vanilla)
     19         Odd-Even Sort
     20         Pigeonhole Sort            (Non-Comparison, Stable)
     21         Priority Queue Sort
     22         Quick Sort                 (C-Library qsort)
     23         Quick Sort                 (Dual Pivot)
     24         Quick Sort                 (Hybrid with Insertion)
     25         Quick Sort                 (Iterative, Stack)
     26         Quick Sort                 (Median of Three)
     27         Quick Sort                 (Random Pivot)
     28         Quick Sort                 (Shuffle First)
     29         Quick Sort                 (Three Way)
     30         Quick Sort                 (Vanilla)
     31         Radix Sort                 (Non-Comparison, Stable)
     32         Selection Sort             (Min/Max Dual)
     33         Selection Sort             (Vanilla)
     34         Shell Sort                 (Power of Four)
     35         Shell Sort                 (Sedgewick-like)
     36         Shell Sort                 (Vanilla, Halving)
     37         Tree Sort                  (AVL Tree)
     38         Tree Sort                  (BSTree Vanilla)
     39         Tree Sort                  (Red Black Tree)
     40         Tree Sort                  (Splay Tree)
     41         Tree Sort                  (Two-Three-Four BTree)

    Usage: ./sorter ID

As of 05/09/20, sorter only reads from standard input.

## Sorter Usage: 

Sorting from a file called sortFile.txt can be done by:

    $ ./sorter ID < sortFile.txt

Using number generator program:

    $ ./gen r 1000 | ./sorter ID

Items should be in the form 

    KEY [VALUE]

with the value field being optional. For example, 

    $ ./gen r 10 
    0 fab
    3 jzc
    1 ytf
    7 riq
    8 ozs
    6 cgd
    4 hpn
    5 jby
    9 qhl
    2 xwe

Currently, the maximum length of the value/name field is 3 
characters, as displayed above.

## Number Generator: 

For help, simply run the command

    $ ./gen 

with no extra arguments. The output will be:

    Usage: ./gen  a|d|r|...|h  N  [Seed]

    a|d|r|R|S|b|B|P|f|F|l|m|i|t|s|c|p|h
        a          ascending unique
        d          descending unique
        r          random unique
        R          random with duplicates
        S          same keys, different values
        b          Bubble up O(n^2), Insertion O(n)
        B          Bubble down O(n^2), Insertion O(n)
        P          Palindrome
        f          Fibonacci
        F          Factorial
        l          Lazy Caterer
        m          Magic Square
        i          Interior Angle Sum of Polygons
        t          Triangular numbers
        s          Square numbers
        c          Cubic numbers
        p          Pentagonal numbers
        h          Hexagonal numbers

By default, if the number of inputs (N) produces an item with 
a key that overflows type ints, then the generator will adjust 
N so as to print the maximum number of items possible. 

For example, the N-cap for the Fibonacci sequence
is 46, since F(47) = 2971215073 > 2^31 - 1.

## Further Notes

By default, only the key of each item is being compared, as the
initial purpose of *gen* and *sorter* was to help distinguish between 
different sorting algorithms. Sorting by keys only will help
to discern whether the sort is stable or not.

This behaviour can be altered by flipping the switch
BOOL\_COMPARE\_KEY\_ONLY in Item.h. However, algorithms like Counting 
Sort, Pigeonhole Sort and Radix Sort will not change their behaviour
as desired as they are non-comparison based algorithm.

---

For Tree Sorts, the Regular Binary Search tree and Splay Tree will
crash for inputs of ascending or descending keys when the number of
keys exceed (approximately) 175000. 
This does not occur with AVL, Red-Black or 2-3-4 Trees since they 
are self-balancing.

---

Pigeon Sort may appear to be the fastest, E.g when running 

    $ time ./gen r 20000000 | sorter ID

but this is only because the number generator written only generate keys
from 0 .. N - 1 (where N is the number of items). Follow the links provided
to understand how this favours pigeon sort (and also counting sort).

--- 

Comb sort is surprising fast despite being a derivative of bubble 
sort :O. Has very little code yet still outperforms binary insertion 
sort and some of the other (seemingly) powerful sorts. 

---

Makefiles were generated with the perl script makemake.pl from COMP2041.
Not relevant, but another cool perl program to make/clean/clobber on 
multiple directories is make\_tree.pl.

---

Algorithms that may be included in the future:

* Bead Sort
* Block Merge Sort (WikiSort)
* Bucket Sort
* Cartesian Tree Sort
* Concentric Circle Sort
* Library Sort
* Merge Sort 3 Way
* Patience Sort
* Sleep Sort
* Spread Sort
* Strand Sort
* Tournament Sort

---

This project was inspired by COMP2521 20T2 Lab09's Sort Detective.

## Resources:

Most of the algorithms were rewritten from pseudo/base code provided by 
Wikipedia, GeeksToGeeks, COMP2521 20T2 Lecture Slides, Stack Overflow
Forums and Github Pages. 

The URLs below were extracted from all \*[ch] files by 
running the command pipeline

    $ egrep "http" *.[ch] | sed "s/^\(.*\.[ch]\):.*http/\1\n\thttp/g" | awk '!x[$0]++'

from the "sorter" directory as of 05.09.2020:

    bogo_sort.c
        https://en.wikipedia.org/wiki/Pancake_sorting
        https://en.wikipedia.org/wiki/Slowsort
        https://en.wikipedia.org/wiki/Stooge_sort
        https://www.geeksforgeeks.org/stooge-sort/
    bubble_sort.c
        https://www.geeksforgeeks.org/bubble-sort/
    cocktail_shaker_sort.c
        https://www.geeksforgeeks.org/cocktail-sort/
        https://en.wikipedia.org/wiki/Cocktail_shaker_sort
    comb_sort.c
        https://www.geeksforgeeks.org/comb-sort/
        https://en.wikipedia.org/wiki/Comb_sort
    counting_sorts.c
        https://www.techiedelight.com/counting-sort-algorithm-implementation/
        https://www.youtube.com/watch?v=OKd534EWcdk
    cycle_sort.c
        https://www.youtube.com/watch?v=1E1Vnq5EsYg
        https://www.geeksforgeeks.org/cycle-sort/
    gnome_sort.c
        https://en.wikipedia.org/wiki/Gnome_sort
        https://www.geeksforgeeks.org/gnome-sort-a-stupid-one/
    Heap.c
        http://www.cse.unsw.edu.au/~cs2521/20T2/lecs/heaps/slides.html#s1
        https://www.youtube.com/watch?v=TkZ2Few6KnU&feature=youtu.be
    heap_sort.c
        http://www.cse.unsw.edu.au/~cs2521/20T2/lecs/heapsort/slides.html#s1
    insertion_sort.c
        https://www.geeksforgeeks.org/bitonic-sort/
        https://www.geeksforgeeks.org/binary-insertion-sort/
    introspective_sort.c
        https://en.wikipedia.org/wiki/Introsort
        https://www.geeksforgeeks.org/introsort-or-introspective-sort/
        https://www.techiedelight.com/introsort-algorithm/
    merge_sort.c
        https://www.geeksforgeeks.org/merge-sort/
        https://www.geeksforgeeks.org/in-place-merge-sort/
        https://stackoverflow.com/questions/2571049/how-to-sort-in-place-using-the-merge-sort-algorithm
        https://www.geeksforgeeks.org/timsort/
    odd_even_sort.c
        https://www.geeksforgeeks.org/odd-even-sort-brick-sort/
    pigeonhole_sort.c
        https://programology.wordpress.com/2015/08/28/c-program-for-pigeonhole-sort/
        https://www.geeksforgeeks.org/pigeonhole-sort/
    quick_sort.c
        https://www.geeksforgeeks.org/quick-sort/
        https://www.geeksforgeeks.org/iterative-quick-sort/
        https://ide.geeksforgeeks.org/ZDEEjbelWh
        https://www.geeksforgeeks.org/dual-pivot-quicksort/
    radix_sort.c
        https://www.geeksforgeeks.org/radix-sort/
    RBTree.c
        https://www.cs.usfca.edu/~galles/visualization/RedBlack.html
    selection_sort.c
        https://www.geeksforgeeks.org/selection-sort/
        https://www.geeksforgeeks.org/sorting-algorithm-slightly-improves-selection-sort/
    shell_sort.c
        https://www.geeksforgeeks.org/shellsort/
    TtfTree.c
        https://gist.github.com/squiidz/996129e16d870a8244fcaa7010ee8b5c

For more details, visit sorter.h and the respective \*.c file. 
Note that some algorithms were adapted from UNSW COMP2521 20T2 
Lecture Slides for which the links are not provided here.
