// Khiet Tam Nguyen (z5313514)
// 27.08.20

// Tools for sorting algorithm

#ifndef ARRAY_H
#define ARRAY_H

#include "Item.h"

#define MAX_SIZE 20000000

#define A_SIZE(lo, hi) (hi - lo + 1)

typedef Item *Array;

// Creates a new Array with "size" item slots.
Array ArrayNew(int size);

// Free the array and all memories associated.
void ArrayFree(Array A, int size);

// Read items from standard input and return #nread.
int ArrayRead(Array A);

// Return a new array copied from the original.
// New size will be hi - lo + 1.
Array ArrayCopy(Array A, int lo, int hi);

// Shuffle an Array of Items uniformly (equal perms).
void ArrayShuffle(Array A, int lo, int hi);

// Reverse the items in the given range within the array.
void ArrayReverse(Array A, int lo, int hi);

// Returns true if the array is sorted, false otherwise.
bool ArrayIsSorted(Array A, int lo, int hi);

// Find the minimum item in the given range
Item ArrayGetMin(Array A, int lo, int hi);

// Find the maximum item in the given range
Item ArrayGetMax(Array A, int lo, int hi);

// Prints the array one per line to stdout
void ArrayPrintVert(Array A, int lo, int hi);

// Prints the array all in one line to stdout.
void ArrayPrintHori(Array A, int lo, int hi);

#endif
