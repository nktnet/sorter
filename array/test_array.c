// Khiet Tam Nguyen (z5313514)
// 30.08.20

// Program to test the array structure.

#include <stdio.h>

#include "Array.h"

#define SIZE 100

int main(void) {
    Array A = ArrayNew(SIZE);
    int nread = ArrayRead(A);

    int lo = 0;
    int hi = nread - 1;

    printf("Original:\n");
    ArrayPrintHori(A, lo, hi);
    if (ArrayIsSorted(A, lo, hi)) printf("Array is sorted.\n");
    else printf("Array is NOT sorted.\n");

    printf("Reverse:\n");
    ArrayReverse(A, lo, hi);
    ArrayPrintHori(A, lo, hi);

    printf("Shuffled:\n");
    ArrayShuffle(A, lo, hi);
    ArrayPrintHori(A, lo, hi);

    printf("Copy Array:\n");
    Array CopyA = ArrayCopy(A, lo, hi);
    // New size is hi - lo + 1.
    ArrayPrintHori(CopyA, 0, hi - lo);

    ArrayFree(A, nread);

    return 0;
}
