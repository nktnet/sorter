// Khiet Tam Nguyen (z5313514)

// 25.08.20 - Added tools.h.
// 26.08.20 - Added more functions.
// 27.08.20 - Renamed to Array.c.
// 28.08.20 - Added print vert/hori.
//          - Added shuffle, check sorted.
// 30.08.20 - Added reverse.
//          - Added copy.
// 01.09.20 - Added GetMin and GetMax.

// Tools for sorting implementations.

#include <stdio.h>
#include <stdlib.h>

#include "Array.h"

// Return the pointer to a new malloced array.
Array ArrayNew(int size) {
    if (size <= 0) return NULL;
    Array A = malloc(sizeof(Item) * size);
    assert(A);
    return A;
}

// Free the array and all memories associated.
void ArrayFree(Array A, int size) {
    if (!A) return;
    for (int i = 0; i < size; i++) {
        ItemFree(A[i]);
    }
    free(A);
}

// Scan key and values of item.
int ArrayRead(Array A) {
    int l = 0;
    Item item;
    while ((item = ItemRead()) != NULL && l <= MAX_SIZE) {
        // pointer to current student record
        A[l] = item;
        l++;
    }
    return l;
}

// Return a new array copied from the original.
Array ArrayCopy(Array A, int lo, int hi) {
    int size = A_SIZE(lo, hi);
    Array CopyA = ArrayNew(size);
    for (int i = 0; i < size; i++) {
        CopyA[i] = A[lo + i];
    }
    return CopyA;
}

// Perform uniform shuffle from A[lo] .. A[hi].
void ArrayShuffle(Array A, int lo, int hi) {
    // srand(time(NULL));
    for (int i = hi; i > lo; i--) { 
        // Pick a random index from 0 to i 
        int j = rand() % (i + 1); 
        ItemSwap(&A[i], &A[j]); 
    } 
}

// Reverse the items in the array within the given range.
void ArrayReverse(Array A, int lo, int hi) {
    int numSwaps = A_SIZE(lo, hi) / 2;
    for (int i = 0; i < numSwaps; i++) {
        ItemSwap(&A[lo + i], &A[hi - i]);
    }
}

// Returns true if the array is sorted in ascending
// order, and false otherwise. 
bool ArrayIsSorted(Array A, int lo, int hi) {
    for (int i = lo; i < hi; i++) {
        if (ItemCompare(A[i], A[i + 1]) > 0) {
            return false;
        }
    }
    return true;
}

// Find the maximum item in the given range.
Item ArrayGetMax(Array A, int lo, int hi) {
    if (!A) return UNKNOWN_ITEM;
    Item max = A[lo];
    for (int i = lo + 1; i <= hi; i++) {
        if (ItemCompare(max, A[i]) < 0) {
            max = A[i];
        }
    }
    return max;
}

// Find the minimum item in the given range
Item ArrayGetMin(Array A, int lo, int hi) {
    if (!A) return UNKNOWN_ITEM;
    Item min = A[lo];
    for (int i = lo + 1; i <= hi; i++) {
        if (ItemCompare(min, A[i]) > 0) {
            min = A[i];
        }
    }
    return min;
}

// Print the given array one entry per line.
void ArrayPrintVert(Array A, int lo, int hi) {
    if (!A) return;
    for (int i = lo; i <= hi; i++) {
        ItemShow(A[i]);
        printf("\n");
    }
}

// Print the given array in one line.
void ArrayPrintHori(Array A, int lo, int hi) {
    if (!A) return;
    for (int i = lo; i <= hi; i++) {
        ItemShow(A[i]);
        printf(" ");
    }
    printf("\n");
}
