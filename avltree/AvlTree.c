// Khiet Tam Nguyen (z5313514)
// 28.08.20

// AVL Avl ADT implementations

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "AvlTree.h"

#define MAX(x, y) ((x > y) ? x : y)

typedef struct node *Node;
struct node {
    Item item;
    Node left;
    Node right;
    int  height;
};

struct tree {
    Node root;
};

////////////////////////////////////////////////////////////////////////

// Helper functions
static void doFree(Node n);
static void doFreeStructureOnly(Node n);
static Node doInsert(Node n, Item item);
static Node rotateLeft(Node n);
static Node rotateRight(Node n);
static int height(Node n);
static Node newNode(Item item);
static Item findFloor(Node n, Item item);
static Item findCeil(Node n, Item item);
static void doFillFirstN(Node n, Item *arr, int size, int *i);
static Node doCopyAvlNode(Node n);
static Node findKthNode(Node n, int k, int *counter);

typedef unsigned long long uint64;
static void doList(Node n);
static void doShow(Node n, int level, uint64 arms);

////////////////////////////////////////////////////////////////////////

// Creates a new, empty tree.
Avl AvlNew(void) {
    Avl t = malloc(sizeof(struct tree));
    assert(t);
    t->root = NULL;
    return t;
}

// Frees all memory associated with the given tree
void AvlFree(Avl t) {
    if (!t) return;
    doFree(t->root);
    free(t);
}

// Free the tree structure, leaving the items untouched.
void AvlFreeStructreOnly(Avl t) {
    if (!t) return;
    doFreeStructureOnly(t->root);
    free(t);
}

////////////////////////////////////////////////////////////////////////

// Insert an item into the tree.
void AvlInsert(Avl t, Item item) {
    assert(t);
    t->root = doInsert(t->root, item);
}

////////////////////////////////////////////////////////////////////////

// Return the highest valued item below the given Item.
Item AvlFloor(Avl t, Item item) {
    if (!t || !item) return NULL;
    return findFloor(t->root, item);
}

// Return the lowest valued item below the given Item.
Item AvlCeiling(Avl t, Item item) {
    if (!t || !item) return NULL;
    return findCeil(t->root, item);
}

////////////////////////////////////////////////////////////////////////

// Fill the first N slots of the array with the items (in order) 
// from the tree. Return the number of items inserted.
int AvlFillFirstN(Avl t, Item *arr, int n) {
    int i = 0;
    doFillFirstN(t->root, arr, n, &i);
    return i;
}

// Creates a copy of a tree and return its pointer.
Avl AvlCopy(Avl t) {
    if (!t) return NULL;
    Avl copyAvl = AvlNew();
    assert(copyAvl);
    doCopyAvlNode(t->root);
    return t;
}

// Get the kth item 'value' in the tree (inorder)
Item AvlGetKth(Avl t, int k) {
    if (!t) return NULL;
    int counter = -1;
    Node kIt = findKthNode(t->root, k, &counter);
    if (kIt) return kIt->item;
    return NULL;
}


////////////////////////////////////////////////////////////////////////

// Display the tree as a list.
void AvlList(Avl t) {
    if (!t) return;
    doList(t->root);
}

// Display the tree structure.
void AvlShow(Avl t) {
    if (!t) return;
    if (t->root != NULL && t->root->height >= 64) {
        printf("Avl is too tall!\n");
    } else {
        doShow(t->root, 0, 0);
    }
}

////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
//                      Helpers                      //
///////////////////////////////////////////////////////

// Free all nodes in the tree and associated memories.
static void doFree(Node n) {
    if (!n) return;
    doFree(n->left);
    doFree(n->right);
    ItemFree(n->item);
    free(n);
}

// Free all nodes in the tree, but leave item untouched.
static void doFreeStructureOnly(Node n) {
    if (!n) return;
    doFreeStructureOnly(n->left);
    doFreeStructureOnly(n->right);
    free(n);
}

// Recursively inserts the new item.
static Node doInsert(Node n, Item item) {
    if (!n) return newNode(item);

    // insert recursively
    int cmp = ItemCompare(item, n->item);
    if (cmp < 0) {
        n->left = doInsert(n->left, item);
    } else {
        // Insert to the right subtree, even if it's a duplicate.
        n->right = doInsert(n->right, item);
    }

    // correct the height of the current subtree
    n->height = 1 + MAX(height(n->left), height(n->right));

    // rebalance the tree
    int LHeight = height(n->left);
    int RHeight = height(n->right);
    if (LHeight - RHeight > 1) {
        if (ItemCompare(item, n->left->item) > 0)
            n->left = rotateLeft(n->left); 
        n = rotateRight(n);
    } else if (RHeight - LHeight > 1) {
        if (ItemCompare(item, n->right->item) < 0)
            n->right = rotateRight(n->right); 
        n = rotateLeft(n);
    }
    return n;
}

// Returns a new malloced node.
static Node newNode(Item item) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->item = item;
    n->height = 0;
    n->left = NULL;
    n->right = NULL;
    return n;
}

// Rotates the given subtree left and returns 
// the root of the updated subtree.
static Node rotateLeft(Node n) {
    if (!n || !n->right) return n;
    Node newRoot = n->right;
    n->right = newRoot->left;
    newRoot->left = n;
    n->height = MAX(height(n->left), height(n->right)) + 1;
    newRoot->height = MAX(height(newRoot->left), height(newRoot->right)) + 1;
    return newRoot;
}

// Rotates the given subtree right and returns 
// the root of the updated subtree.
static Node rotateRight(Node n) {
    if (!n || !n->left) return n;
    Node newRoot = n->left;
    n->left = newRoot->right;
    newRoot->right = n;
    n->height = MAX(height(n->left), height(n->right)) + 1;
    newRoot->height = MAX(height(newRoot->left), height(newRoot->right)) + 1;
    return newRoot;
}

// Returns the height of a subtree while assuming that the height field
// of the root node of the subtree is correct
static int height(Node n) {
    return (!n) ? -1 : n->height;
}

///////////////////////////////////////////////////////////////////

// 29.08.20 - Changed to better looking code for floor and ceiling.
// Below Iterative and Recursive approaches are courtesy of 
// Josie Anugerah, Tutor at UNSW COMP2521 20T2

// Find the floor item ITERATIVELY and return that item.
static Item findFloor(Node n, Item item) {
    Node curr = n;
    Node floor = NULL;
    while (curr) {
        if (ItemCompare(item, curr->item) >= 0) {
            floor = curr;
            curr = curr->right;
        } else {
            curr = curr->left;
        }
    }
    return (floor) ? floor->item : NULL;
}

// Find the ceiling item RECURSIVELY and return that item.
static Item findCeil(Node n, Item item) {
    if (!n) return NULL;
    if (ItemCompare(item, n->item) <= 0) {
        Item left = findCeil(n->left, item);
        if (!left) return n->item;
        return left;
    }
    return findCeil(n->right, item);
}

///////////////////////////////////////////////////////////////////

// Recursive function to copy nodes from a tree.
static Node doCopyAvlNode(Node n) {
    if (!n) return NULL;
    Node copyNode = newNode(n->item);
    copyNode->height = n->height;
    copyNode->left = doCopyAvlNode(n->left);
    copyNode->right = doCopyAvlNode(n->right);
    return copyNode;
}

// Fill the given array with "size" items, or the max possible.
static void doFillFirstN(Node n, Item *arr, int size, int *i) {
    if (!n) return;
    if (*i > size) return;
    doFillFirstN(n->left, arr, size, i);
    arr[*i] = n->item;
    *i += 1;
    doFillFirstN(n->right, arr, size, i);
}

// Find the kth valued node in the tree.
static Node findKthNode(Node n, int k, int *counter) {
    if (!n) return NULL;
    Node left = findKthNode(n->left, k, counter);
    *counter += 1;
    if (left) return left;
    if (*counter == k) return n;
    return findKthNode(n->right, k, counter);
}

////////////////////////////////////////////////////////////////////////

// Recursively show the tree as a list.
static void doList(Node n) {
    if (!n) return;
    doList(n->left);
    ItemShow(n->item);
    printf("\n");
    doList(n->right);
}

// This  function  uses a hack to determine when to draw the arms of the
// tree and relies on the tree being reasonably balanced. Don't try to
// use this function if the tree is not an AVL tree!
static void doShow(Node n, int level, uint64 arms) {
    if (n == NULL) return;
    ItemShow(n->item);
    printf(" (height: %d)", n->height);
    printf("\n");

    if (n->left != NULL) {
        for (int i = 0; i < level; i++) {
            if ((1LLU << i) & arms) {
                printf("│     ");
            } else {
                printf("      ");
            }
        }
        printf("%s", n->right != NULL ? "┝━╸L: " : "┕━╸L: ");
        if (n->right != NULL) {
            arms |= (1LLU << level);
        } else {
            arms &= ~(1LLU << level);
        }
        doShow(n->left, level + 1, arms);
    }
    if (n->right != NULL) {
        for (int i = 0; i < level; i++) {
            if ((1LLU << i) & arms) {
                printf("│     ");
            } else {
                printf("      ");
            }
        }
        printf("┕━╸R: ");
        arms &= ~(1LLU << level);
        doShow(n->right, level + 1, arms);
    }
}
