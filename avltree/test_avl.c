// Khiet Tam Nguyen (z5313514)
// 27.08.20

// Main function to test avl tree.

#include <stdio.h>

#include "AvlTree.h"
#define MAX_LINE 30

// Stop after this many items.
#define TEST_MAX_TREE 1000

int main(void) {
    Avl t = AvlNew();
    int n = 0;
    Item it; 
	while ((it = ItemRead()) && n < TEST_MAX_TREE) {
        AvlInsert(t, it);
        printf("-----------------------------------\n");
        AvlShow(t);
        n++;
    }
/*
    printf("-----------------------------------\n");
    printf("Avl Copy\n");
    Avl copyAvl = AvlCopy(t);
    AvlShow(copyAvl);
    AvlFree(copyAvl);
    printf("-----------------------------------\n");
*/

/*
    printf("Avl Fill First N\n");
    Item arr[n];
    int nReturned = AvlFillFirstN(t, arr, n);
    for (int i = 0; i < nReturned; i++) {
        ItemShow(arr[i]);
        printf(" ");
    }
    printf("\n");
    AvlFree(t);
*/

/*
    printf("Avl floor and ceiling\n");
    while ((it = ItemRead())) {
        Item ceil = AvlCeiling(t, it);
        Item floor = AvlFloor(t, it);

        printf("Ceil: ");
        ItemShow(ceil);

        printf("Item: ");
        ItemShow(it);

        printf("Floor: ");
        ItemShow(floor);

        printf("\n");
    }
*/
    printf("Item at kth position\n");
    while (true) {
        int k;
        scanf("%d", &k);
        Item kIt = AvlGetKth(t, k);
        printf("Item at position %d is: ", k);
        ItemShow(kIt);
        printf("\n");

    }
    return 0;
}
