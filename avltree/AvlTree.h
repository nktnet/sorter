// Khiet Tam Nguyen (z5313514)
// 28.08.20

// Interface for the AvlAvl ADT

#ifndef AVL_TREE_H
#define AVL_TREE_H

#include "Item.h"

typedef struct tree *Avl;

// Creates a new empty tree
Avl AvlNew(void);

// Frees all memory associated with the given tree
void AvlFree(Avl t);

// Free tree structure, but not memories of items.
void AvlFreeStructreOnly(Avl t);

// Inserts copy of item into tree. 
void AvlInsert(Avl t, Item item);

// Return latest item below given time, or NULL.
Item AvlFloor(Avl t, Item item);

// Return earliest item after given time, or NULL.
Item AvlCeiling(Avl t, Item item);

// Fill given array with first n items, or max available
// and return the amount filled.
int AvlFillFirstN(Avl t, Item *arr, int n);

// Get the kth item 'value' in the tree (inorder)
Item AvlGetKth(Avl t, int k);

// Copy a given tree up to the given depth
Avl AvlCopy(Avl t);

// Lists all the items in the tree, one per line
void AvlList(Avl t);

// Shows the tree structure
void AvlShow(Avl t);

////////////////////////////////////////////////////////////////////////

#endif
