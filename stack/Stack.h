// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Stack ADT interface.

#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>

#include "Item.h"

typedef struct stack *Stack;

// Create a new stack
Stack StackNew(void);

// Free all resources allocated for the stack
void StackDrop(Stack s);

// Add an item to the end of the stack
void StackPush(Stack s, Item item);

// Remove an element from the front of the stack and return it
Item StackPop(Stack s);

// Get the element at the front of the stack (without removing it)
Item StackPeek(Stack s);

// Get the number of elements in the stack
int StackSize(Stack s);

// Check if the stack is empty
bool StackIsEmpty(Stack s);

// Prints the stack to stdin
void StackShow(Stack s);

// Print the stack to an open file (for debugging)
void StackDump(Stack s, FILE *fp);

#endif
