// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Main function to test Stack ADT.

#include <stdio.h>

#include "Stack.h"

typedef enum command {
    PRINT,
    PUSH,
    POP,
    QUIT,
    HELP,
} Command;

static void printHelp(void);

int main(void) {
    Item item, pop;
    int cmd;
    Stack s = StackNew();

    printf("Push first item: ");
    item = ItemRead();
    if (!item) return 1;
    StackPush(s, item);
    StackShow(s);
    printf("--------------------------\n");

    printf("Enter '%d' for a list of commands\n", HELP);
    while (!StackIsEmpty(s)) {
        printf("Enter command: ");
        if (scanf("%d", &cmd) != 1) return 1;
        switch (cmd) {
        case PRINT: 
            break;
        case PUSH:
            item = ItemRead();
            if (!item) return 1;
            printf("Pushing ");
            ItemShow(item);
            printf(" to stack\n");
            StackPush(s, item);
            break;
        case POP:
            pop = StackPop(s);
            ItemShow(pop);
            printf(" was popped from the stack\n");
            break;
        case QUIT:
            printf("Sayonara\n");
            return 0;
        case HELP:
        default:
            printHelp();
        }
        StackShow(s);
        printf("--------------------------\n");
    }
    StackDrop(s);

    return 0;
}

static void printHelp(void) {
    printf("-----------------------------------------------\n");
    printf(
        "\tCommands:\n"
        "\t%d                print the stack\n"
        "\t%d <item>         push item to stack\n"
        "\t%d                pop item from stack\n"
        "\t%d                quits the program\n"
        "\t%d                print this help message\n",
        PRINT,
        PUSH,
        POP,
        QUIT,
        HELP
    );
    printf("-----------------------------------------------\n");
}
