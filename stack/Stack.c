// Khiet Tam Nguyen (z5313514)
// 24.08.20

// Stack ADT implementation.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Stack.h"

//////////////////////////////////////////////////////////

typedef struct node *Node;

//////////////////////////////////////////////////////////

struct node {
    Item item;
    Node next;
};

struct stack {
    Node front;
    int size;
};

//////////////////////////////////////////////////////////

// Returns a malloced node.
static Node newNode(Item item) {
    Node n = malloc(sizeof(struct node));
    assert(n);
    n->item = item;
    n->next = NULL;
    return n;
}

// Returns a malloced stack.
Stack StackNew(void) {
    Stack s = malloc(sizeof(struct stack));
    assert(s);
    s->front = NULL;
    s->size = 0;
    return s;
}

// Free all memories allocated to stack.
void StackDrop(Stack s) {
    if (!s) return;
    Node curr = s->front;
    while (curr) {
        Node rem = curr;
        curr = curr->next;
        free(rem);
    }
    free(s);
}

// Add an item to the front of the stack.
void StackPush(Stack s, Item item) {
    assert(s);
    Node n = newNode(item);
    if (s->front) {
        n->next = s->front;
    }
    s->front = n;
    s->size++;
}

// Remove the first item in the stack and 
// return its value.
Item StackPop(Stack s) {
    assert(s && !StackIsEmpty(s));
    Node rem = s->front;
    Item pop = rem->item;
    s->front = rem->next;
    free(rem);
    s->size--;
    return pop;
}

// Get the element at the front of 
// the stack (without removing it)
Item StackPeek(Stack s) {
    assert(s && !StackIsEmpty(s));
    return s->front->item;
}

// Gets the size of the stack.
int StackSize(Stack s) {
    assert(s);
    return s->size;
}

// Checks if the stack is empty.
bool StackIsEmpty(Stack s) {
    assert(s);
    return (s->size == 0);
}

// Prints the stack to standard output.
void StackShow(Stack s) {
    assert(s);
    printf("Stack:\t");
    Node curr = s->front;
    while (curr) {
        ItemShow(curr->item);
        printf(" --> ");
        curr = curr->next;
    }
    printf("X\n");
    printf("Size:\t%d\n", StackSize(s));
}
