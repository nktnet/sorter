// Khiet Tam Nguyen (z5313514)

// 26.08.20 - Added vanilla merge sort.
// 27.08.20 - Added naive inplace merge sort.
// 28.08.20 - Added Optimised inplace merge sort.
// 29.08.20 - Added bottom-up merge sort (iterative).
// 30.08.20 - Added Timsort implementation

// Algorithms adapted from COMP2521 20T2 Lecture Slides,
// Wikipedia, Stackoverflow and GeeksToGeeks. Links/urls
// are placed in the respective sections.

#include "sorter.h"

#define MIN(x,y) ((x < y) ? x : y)

// For timsort
#define RUN 32

/////////////////////////////////////////////////////////

// Vanilla Helpers
static void break_and_merge(Array A, int lo, int mid, int hi);
static void ms_vanilla(Array A, int lo, int hi);

// Bottom up helpers
static void ms_bottom_up(Array A, int lo, int hi);
static void bottom_merge(Item a[], int aN, Item b[], int bN, Item c[]);
static void copy(Array A, Array C, int n);

// Naive Inplace Helpers
static void naive_ipmerge(Array A, int start, int mid, int end);
static void naive_imsort(Array A, int lo, int hi);

// Optimised Inplace Helpers
static void opt_wmerge(Array A, int i, int mid, int j, int n, int w);
static void opt_imsort(Array A, int lo, int size);
static void opt_wsort(Array A, int lo, int size, int w);

// Iterative trivial helper function.
static void iterative_timsort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Wrapper for vanilla merge sort.
void merge_sort_vanilla(Array A, int lo, int hi) {
    if (!A) return;
    ms_vanilla(A, lo, hi);
}

// Bottom up (non-recursive) merge sort.
void merge_sort_bottom_up(Array A, int lo, int hi) {
    if (!A) return;
    ms_bottom_up(A, lo, hi);
}

// Wrapper around naive inplace merge sort.
void merge_sort_naive_inplace(Array A, int lo, int hi) { 
    if (!A) return;
    naive_imsort(A, lo, hi);
} 

// Wrapper around optimised inplace merge sort.
// Courtesy of LAy Liu Xinyu from Stack Overflow (Url at the end).
void merge_sort_optimised_inplace(Array A, int lo, int hi) {
    if (!A) return;
    opt_imsort(A, lo, hi + 1);
}

// Sort small blocks using insertion sort, then merge those blocks.
void merge_timsort(Array A, int lo, int hi) {
    if (!A) return;
    iterative_timsort(A, lo, hi);
}

/////////////////////////////////////////////////////////////////
//                      Vanilla Merge Sort                     //
/////////////////////////////////////////////////////////////////

// Algorithms below were adapted from both COMP2521 20T2 Lecture
// slides and GeeksToGeeks: 
// https://www.geeksforgeeks.org/merge-sort/

// Recursively merge-sort the given array between lo and hi.
static void ms_vanilla(Array A, int lo, int hi) {
    if (hi <= lo) return;
    int mid = lo + (hi - lo) / 2; 
    ms_vanilla(A, lo, mid); 
    ms_vanilla(A, mid + 1, hi); 
    break_and_merge(A, lo, mid, hi); 
} 

// Break the Array up and merge it back in the right order.
static void break_and_merge(Array A, int lo, int mid, int hi) { 
    int n1 = mid - lo + 1; 
    int n2 = hi - mid; 

    // Creates two temp array before merging.
#if 1
    Array temp1 = ArrayNew(n1);
    Array temp2 = ArrayNew(n2);
#else 
    Item temp1[n1], temp2[n2];
#endif
    for (int i = 0; i < n1; i++) 
        temp1[i] = A[lo + i]; 
    for (int j = 0; j < n2; j++) 
        temp2[j] = A[mid + 1 + j]; 

    int i = 0;      
    int j = 0;      
    int k = lo;    
    // Merging the two subArrays back into the main.
    while (i < n1 && j < n2) { 
        if (ItemCompare(temp1[i], temp2[j]) < 0) { 
            A[k] = temp1[i++]; 
        } else { 
            A[k] = temp2[j++]; 
        } 
        k++; 
    } 

    // Copy any leftovers from Array 1.
    while (i < n1) {
        A[k++] = temp1[i++]; 
    }

    // Copy leftovers from Array 2.
    while (j < n2) {
        A[k++] = temp2[j++]; 
    }
#if 1
    // Won't use ArrayFree since we don't want to
    // free the items.
    free(temp1);
    free(temp2);
#endif
} 

/////////////////////////////////////////////////////////////////
//                    Bottom Up Merge Sort                     //
/////////////////////////////////////////////////////////////////

// Code adapted from COMP2521 Lecture slides (By John Shepherd).

// Merge function for bottom up approach.
static void bottom_merge(Array A, int aN, Array B, int bN, Array C) {
    // Index into a, b and c Array respectively.
    int i, j, k;
    for (i = j = k = 0; k < aN + bN; k++) {
        if (i == aN) {
            C[k] = B[j++];
        } else if (j == bN) {
            C[k] = A[i++];
        } else if (ItemCompare(A[i], B[j]) < 0) {
            C[k] = A[i++];
        } else {
            C[k] = B[j++];
        }
    }
}

// Copy n items from Array C into Array A.
static void copy(Array A, Array C, int n) {
    for (int i = 0; i < n; i++) {
        A[i] = C[i];
    }
}

// Iterative bottom up merge sort helper function.
static void ms_bottom_up(Array A, int lo, int hi) {
    for (int m = 1; m <= hi - lo; m = 2*m) {
        for (int i = lo; i <= hi - m; i += 2*m) {
            int len = MIN(m, hi - (i + m) + 1);
            Item C[m + len];
            bottom_merge(&A[i], m, &A[i + m], len, C);
            copy(&A[i], C, m + len);
        }
    }
}

/////////////////////////////////////////////////////////////////
//                 Naive Inplace Merge Sort                    //
/////////////////////////////////////////////////////////////////

// Inplace Merge Sort with O(n^2) time adapted from GeeksToGeeks:
// https://www.geeksforgeeks.org/in-place-merge-sort/

// Recursive helper for inplace merge sort.
static void naive_imsort(Array A, int lo, int hi) {
    if (hi <= lo) return; 
    int mid = lo + (hi - lo) / 2; 
    naive_imsort(A, lo, mid); 
    naive_imsort(A, mid + 1, hi); 
    naive_ipmerge(A, lo, mid, hi); 
}

// Performing merge without creating new Arrays.
static void naive_ipmerge(Array A, int start, int mid, int end) { 
    int start2 = mid + 1; 
    if (ItemCompare(A[mid], A[start2]) <= 0) return; 
    while (start <= mid && start2 <= end) { 
        if (ItemCompare(A[start], A[start2]) <= 0) { 
            start++; 
        } else { 
            Item value = A[start2]; 
            int i = start2; 
            while (i != start) { 
                A[i] = A[i - 1]; 
                i--; 
            } 
            A[start] = value; 
            start++; 
            mid++; 
            start2++; 
        } 
    } 
} 

/////////////////////////////////////////////////////////////////
//                 Optimised Inplace Merge Sort                //
/////////////////////////////////////////////////////////////////

// Algorithm adapted from LAy Liu Xinyu's answer at Stack Overflow:
// https://stackoverflow.com/questions/2571049/how-to-sort-in-place-using-the-merge-sort-algorithm

// Inplace merge helper function.
static void opt_wmerge(Array A, int i, int mid, int j, int n, int w) {
    while (i < mid && j < n) {
        if (ItemCompare(A[i], A[j]) < 0) {
            ItemSwap(&A[w++], &A[i++]);
        } else {
            ItemSwap(&A[w++], &A[j++]);
        }
    }
    while (i < mid) {
        ItemSwap(&A[w++], &A[i++]);
    }
    while (j < n) {
        ItemSwap(&A[w++], &A[j++]);
    }
}  

// Sort A[lo, size) and put result to working area "w".
// Constraint: length(w) = u - l.
static void opt_wsort(Array A, int lo, int size, int w) {
    if (size - lo > 1) {
        int mid = lo + (size - lo) / 2;
        opt_imsort(A, lo, mid);
        opt_imsort(A, mid, size);
        opt_wmerge(A, lo, mid, mid, size, w);
    } else {
        while (lo < size) {
            ItemSwap(&A[lo++], &A[w++]);
        }
    }
}

// Inplace merge recursive function (double recursion 
// with opt_wsort).
static void opt_imsort(Array A, int lo, int size) {
    if (size - lo <= 1) return;
    int mid = lo + (size - lo) / 2;
    int w = lo + size - mid;
    // The last half contains sorted elements
    opt_wsort(A, lo, mid, w); 
    while (w - lo > 2) {
        int n = w;
        w = lo + (n - lo + 1) / 2;
        // The first half of the previous working area contains sorted elements
        opt_wsort(A, w, n, lo);  
        opt_wmerge(A, lo, lo + n - w, n, size, w);
    }
    // switch to insertion sort
    for (int n = w; n > lo; n--) {
        mid = n;
        while (mid < size && ItemCompare(A[mid], A[mid - 1]) < 0) {
            ItemSwap(&A[mid], &A[mid - 1]);
            mid++;
        }
    }
}

/////////////////////////////////////////////////////////////////
//                          Tim Sort                           //
/////////////////////////////////////////////////////////////////

// 30.08.20 - Timsort implementation

// Algorithm Adapted from GeeksToGeeks's C++ program:
// https://www.geeksforgeeks.org/timsort/

// iterative Timsort function to sort the 
// Array[0...n-1] (similar to merge sort) 
static void iterative_timsort(Array A, int lo, int hi) { 
    // Divide into blocks of RUN and use insertion sort.
    // on small section of arrays.
    for (int i = lo; i <= hi; i += RUN) {
        int right = MIN((i + RUN - 1), hi);
        insertion_sort_vanilla(A, i, right); 
    }
    // Run merge sort on the "Runs" sorted by insertion.
    for (int size = RUN; size <= hi; size = 2*size) { 
        for (int left = lo; left <= hi; left += 2*size) { 
            int mid = left + size - 1; 
            int right = MIN((left + 2*size - 1), hi); 
            break_and_merge(A, left, mid, right); 
        } 
    } 
} 
