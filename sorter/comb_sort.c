// Khiet Tam Nguyen (z5313514)
// 27.08.20

// Adaptive Comb Sort (Swap by intervals), improved on bubble sort.
// Same relationship as shell short to insertion sort.

// This algorithm is surprisingly fast for a bubble sort derivative :O.

// Algorithm Adapted from GeeksToGeeks C++ Program:
// https://www.geeksforgeeks.org/comb-sort/

// Wikipedia Link
// https://en.wikipedia.org/wiki/Comb_sort


#include "sorter.h"

//////////////////////////////////////

// Helpers
static int get_next_gap(int gap);

//////////////////////////////////////

// Bubble elements by gap intervals, which gradually
// decreases to gap = 1 (regular bubble sort.
void comb_sort(Array A, int lo, int hi) {
    if (!A) return;
    int gap = hi + 1;
    bool swapped = true;
    while (gap > 1 || swapped) {
        gap = get_next_gap(gap);
        swapped = false;
        for (int i = lo; i <= hi - gap; i++) {
            if (ItemCompare(A[i], A[i + gap]) > 0) {
                ItemSwap(&A[i], &A[i + gap]);
                swapped = true;
            }
        }
    }
}

// Helper function to calculate the next gap for comb sort.
static int get_next_gap(int gap) {
    int nextGap = (gap * 10) / 13;
    return (nextGap > 1) ? nextGap : 1;
}
