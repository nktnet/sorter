// Khiet Tam Nguyen (z5313514)

// 26.08.20 - Initial commit

// Cock Tail Sort Implementation (Variation of Bubble)

// Algorithm adapted from GeeksToGeeks:
// https://www.geeksforgeeks.org/cocktail-sort/

// Wikipedia Link for pseudocode:
// https://en.wikipedia.org/wiki/Cocktail_shaker_sort

#include "sorter.h"

// Variation of bubble sort that alternates in direction.
// Performs slightly better, with fewer traversals.

// Ordering from the sides, narrowing start and end until
// reaching the middle.
void cocktail_shaker_sort(Array A, int lo, int hi) { 
    if (!A) return;
    int start = lo; 
    int end = hi; 
    bool swapped = true;; 
    while (swapped) { 
        swapped = false;
        for (int i = start; i < end; i++) { 
            if (ItemCompare(A[i], A[i + 1]) > 0) { 
                ItemSwap(&A[i], &A[i + 1]); 
                swapped = true; 
            } 
        } 
        if (!swapped) break; 
        end--;
        swapped = false;
        for (int i = end - 1; i >= start; i--) { 
            if (ItemCompare(A[i], A[i + 1]) > 0) { 
                ItemSwap(&A[i], &A[i + 1]); 
                swapped = true; 
            } 
        } 
        start++;
    } 
}
