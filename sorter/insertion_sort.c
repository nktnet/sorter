// Khiet Tam Nguyen (z5313514)

// 25.08.20 - implementation of insertion sort.
// 27.08.20 - Added Binary Search Insertion Sort.
// 03.09.20 - Added Insertion Sort + Bitonic Sort.

// Algorithms adapted from COMP2521 20T2 Lecture Slides
// and GeeksToGeeks.

// Note: Sort Bitonic is not actually a thing. I made it up
// since bitonic sort only works for inputs with exactly 
// 2^k elements.
// Here is the GeeksToGeeks vanilla Bitonic Sort link (C++):
// https://www.geeksforgeeks.org/bitonic-sort/

#include "sorter.h"

//////////////////////////////////////////////////////////

// Helper 
static int binarySearch(Array A, Item item, int low, int high);

//////////////////////////////////////////////////////////

// Helper Functons for bitonic sort.
static int getBinarySize(int size);
static void compareSwap(Array A, int i, int j, int dir);
static void bitonicmerge(Array A, int lo, int size, int dir);
static void recurseBitonic(Array A, int lo, int size, int dir);

////////////////////////////////////////////////////////

// Vanilla insertion sort.
void insertion_sort_vanilla(Array A, int lo, int hi) { 
    if (!A) return;
    for (int i = lo + 1; i <= hi; i++) { 
        Item key = A[i]; 
        // last element of sorted partition.
        int j = i - 1; 
        while (j >= lo && ItemCompare(A[j], key) > 0) { 
            // Shift elements greater than 'key' up.
            A[j + 1] = A[j]; 
            j--;
        } 
        A[j + 1] = key; 
    } 
} 

//////////////////////////////////////////////////////////

// Optimised with binary search insertion sort.
// Links for Base Code:
// https://www.geeksforgeeks.org/binary-insertion-sort/
void insertion_sort_binary(Array A, int lo, int hi) { 
    if (!A) return;
    for (int i = lo + 1; i <= hi; i++) { 
        Item selected = A[i]; 
        int j = i - 1; 
        int loc = binarySearch(A, selected, lo, j); 
        while (j >= loc) { 
            A[j + 1] = A[j]; 
            j--; 
        } 
        A[j + 1] = selected; 
    } 
} 

//////////////////////////////////////////////////////////

// Use Bitonic Sort (only works on power of 2, so sort
// as many elements as possible), then binary insertion 
// sort on remaining elements.
// Can be improved by giving insertion sort information 
// on where the already sorted array is.
void insertion_sort_bitonic(Array A, int lo, int hi) {
    if (!A) return;
    int size = A_SIZE(lo, hi);
    int newSize = getBinarySize(size);
    recurseBitonic(&A[lo], 0, newSize, 1);
    if (newSize != size) {
        // size is not a power of 2, use binary insertion.
        insertion_sort_binary(A, lo, hi);
    }
}

//////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
//                   Binary Helpers                  //
///////////////////////////////////////////////////////

// Find the position for insertion in O(log(n)) time.
static int binarySearch(Array A, Item item, int low, int high) { 
    if (high <= low) 
        return (ItemCompare(item, A[low]) > 0) ?  (low + 1): low; 
    int mid = (low + high) / 2; 
    int cmp = ItemCompare(item, A[mid]);
    if (cmp == 0) return mid + 1; 
    if (cmp > 0) return binarySearch(A, item, mid + 1, high); 
    return binarySearch(A, item, low, mid - 1); 
} 

///////////////////////////////////////////////////////
//                   Bitonic Helpers                 //
///////////////////////////////////////////////////////

// Get the highest k where 2^k < size.
static int getBinarySize(int size) {
    if (size < 2) return 0;
    int newSize = 2;
    while (newSize * 2 <= size) {
        newSize *= 2;
    }
    return newSize;
}

// Compare two items and swap them if the direction matches
// the comparison.
static void compareSwap(Array A, int i, int j, int dir) {
    if (dir == (ItemCompare(A[i], A[j]) > 0)) {
        ItemSwap(&A[i], &A[j]);
    }
}

// Recursively sorts a bitonic sequence in ascending order, 
// if dir =- 1, and in descending order if dir == 0.
static void bitonicmerge(Array A, int lo, int size, int dir) {
    if (size <= 1) return;
    int k = size / 2;
    for (int i = lo; i < lo + k ;i++) {
        compareSwap(A, i, i+k, dir);    
    }
    bitonicmerge(A, lo, k, dir);
    bitonicmerge(A, lo + k, k, dir);    
}

// produce a bitonic sequence by recursively two halves in 
// opposite sorting dir, then use bitonicMerge to make the
// dirs the same.
static void recurseBitonic(Array A, int lo, int size, int dir) {
    if (size <= 1) return;
    int k = size / 2;
    recurseBitonic(A, lo, k, 1);
    recurseBitonic(A, lo + k, k, 0);
    bitonicmerge(A, lo, size, dir);
}
