// Khiet Tam Nguyen (z5313514)

// 26.08.20 - AVL Tree Sort.
// 29.08.20 - Regular Binary Search Tree
// 30.08.20 - Added Splay Tree
// 31.08.20 - Added Red Black Tree
// 01.09.20 - Added Two-Three-Four (B)tree.

// Splay Tree and Regular BSTree crashes at around 175000 
// if the input is strictly ascending/desencending, E.g
// $ ./gen a 175000 | ./sorter <TREE_SPLAY_ID>
// No issue with AVL, redblack or two-three-four trees
// since they are balanced.

// Tree ADTs were rewritten from a variety of sources, 
// including Github, COMP2521 20T2 Labs, Wikipedia, 
// GeeksToGeeks, Stackoverflow, etc. See the respective
// files for further details.

#include "sorter.h"

#include "AvlTree.h"
#include "BSTree.h"
#include "RBTree.h"
#include "SplayTree.h"
#include "TtfTree.h"

// Create and traverse an avl height-balanced tree
// to sort the array.
void tree_sort_avl(Array A, int lo, int hi) {
    if (!A) return;
    Avl t = AvlNew();
    for (int i = lo; i <= hi; i++) {
        AvlInsert(t, A[i]);
    }
    AvlFillFirstN(t, &A[lo], A_SIZE(lo, hi));
    AvlFreeStructreOnly(t);
}

// Create and traverse a regular bstree with
// no balancing mechanisms
void tree_sort_bst(Array A, int lo, int hi) {
    if (!A) return;
    BSTree t = BSTreeNew();
    for (int i = lo; i <= hi; i++) {
        BSTreeInsert(t, A[i]);
    }
    BSTreeFillFirstN(t, &A[lo], A_SIZE(lo, hi));
    BSTreeFreeStructreOnly(t);
}

// Red Black Tree structure for sorting.
void tree_sort_red_black(Array A, int lo, int hi) {
    if (!A) return;
    RBTree t = RBTreeNew();
    for (int i = lo; i <= hi; i++) {
        RBTreeInsert(t, A[i]);
    }
    RBTreeFillFirstN(t, &A[lo], A_SIZE(lo, hi));
    RBTreeFreeStructreOnly(t);
}

// Splay Tree Structure for sorting.
void tree_sort_splay(Array A, int lo, int hi) {
    if (!A) return;
    Splay t = SplayNew();
    for (int i = lo; i <= hi; i++) {
        SplayInsert(t, A[i]);
    }
    SplayFillFirstN(t, &A[lo], A_SIZE(lo, hi));
    SplayFreeStructreOnly(t);
}

// Two-Three-Four [B]Tree Structure for sorting.
void tree_sort_ttf(Array A, int lo, int hi) {
    if (!A) return;
    Ttf t = TtfNew();
    for (int i = lo; i <= hi; i++) {
        TtfInsert(t, A[i]);
    }
    TtfFillFirstN(t, &A[lo], A_SIZE(lo, hi));
    TtfFreeStructureOnly(t);
}
