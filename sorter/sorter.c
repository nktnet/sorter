// Khiet Tam Nguyen (z5313514)
// 25.08.20 - 05.09.20

// Main program to test all the different 
// sorting implementations

#include <stdio.h>
#include <stdlib.h>

#include "sorter.h"
#include "Array.h"

// Display name of algorithm before and after sorting.
#define BOOL_PRINT_TYPE_NAME 1

/////////////////////////////////////////////////

// Helpers
static bool is_valid_type(Type sortType);
static void exit_usage(char *program);
static void print_sort_type(Type sortType);

/////////////////////////////////////////////////

int main(int argc, char *argv[]) {
    if (argc != 2) exit_usage(argv[0]);

    char *end;
    Type sortType = strtol(argv[1], &end, 10);
    if (*end || !is_valid_type(sortType)) exit_usage(argv[0]);

    // Random Seed for sorting algorithms 
    // that relies on randomness.
    srand(time(NULL));

    Array A = ArrayNew(MAX_SIZE);
    int size = ArrayRead(A);

    // Will always sort the entire array in this program.
    int lo = 0;
    int hi = size - 1;

#if 0
    // Testing sort between range.
    lo = 2;
    hi = 8;
#endif

    print_sort_type(sortType);
    switch (sortType) {
    case BOGO_ORIGINAL:         bogo_sort_original(A, lo, hi);              break;
    case BOGO_PERMUTATION:      bogo_sort_permutation(A, lo, hi);           break;
    case BOGO_PSEUDO:           bogo_sort_pseudo(A, lo, hi);                break;
    case BOGO_PANCAKE:          bogo_sort_pancake(A, lo, hi);               break;
    case BOGO_SLOW:             bogo_sort_slow(A, lo, hi);                  break;
    case BOGO_STOOGE:           bogo_sort_stooge(A, lo, hi);                break;
    case BUBBLE_ADAPTIVE:       bubble_sort_adaptive(A, lo, hi);            break;
    case BUBBLE_OBLIVIOUS:      bubble_sort_oblivious(A, lo, hi);           break;
    case COCKTAIL:              cocktail_shaker_sort(A, lo, hi);            break;
    case COMB:                  comb_sort(A, lo, hi);                       break;
    case COUNTING:              counting_sort(A, lo, hi);                   break;
    case CYCLE:                 cycle_sort(A, lo, hi);                      break;
    case GNOME_OPTIMISED:       gnome_sort_optimised(A, lo, hi);            break;
    case GNOME_VANILLA:         gnome_sort(A, lo, hi);                      break;
    case HEAP_ADT:              heap_sort_adt(A, lo, hi);                   break;
    case HEAP_INPLACE:          heap_sort_inplace(A, lo, hi);               break;
    case INSERTION_BINARY:      insertion_sort_binary(A, lo, hi);           break;
    case INSERTION_BITONIC:     insertion_sort_bitonic(A, lo, hi);          break;
    case INSERTION_VANILLA:     insertion_sort_vanilla(A, lo, hi);          break;
    case INTROSPECTIVE:         introspective_sort(A, lo, hi);              break;
    case MERGE_BOTTOM_UP:       merge_sort_bottom_up(A, lo, hi);            break;
    case MERGE_INPLACE_NAIVE:   merge_sort_naive_inplace(A, lo, hi);        break;
    case MERGE_INPLACE_OPT:     merge_sort_optimised_inplace(A, lo, hi);    break;
    case MERGE_TIMSORT:         merge_timsort(A, lo, hi);                   break;
    case MERGE_VANILLA:         merge_sort_vanilla(A, lo, hi);              break;
    case ODD_EVEN:              odd_even_sort(A, lo, hi);                   break;
    case PIGEON:                pigeonhole_sort(A, lo, hi);                 break;
    case PQUEUE:                priority_queue_sort(A, lo, hi);             break;
    case QUICK_C_LIBRARY:       quick_sort_c_library(A, lo, hi);            break;
    case QUICK_DUAL_PIVOT:      quick_sort_dual_pivot(A, lo, hi);           break;
    case QUICK_HYBRID:          quick_sort_hybrid_insertion(A, lo, hi);     break;
    case QUICK_ITERATIVE:       quick_sort_iterative(A, lo, hi);            break;
    case QUICK_MEDIAN:          quick_sort_median(A, lo, hi);               break;
    case QUICK_RANDOM_PIVOT:    quick_sort_random_pivot(A, lo, hi);         break;
    case QUICK_SHUFFLE:         quick_sort_shuffle(A, lo, hi);              break;
    case QUICK_THREE_WAY:       quick_sort_three_way(A, lo, hi);            break;
    case QUICK_VANILLA:         quick_sort_vanilla(A, lo, hi);              break;
    case RADIX:                 radix_sort(A, lo, hi);                      break;
    case SELECTION_DUAL:        selection_sort_dual(A, lo, hi);             break;
    case SELECTION_VANILLA:     selection_sort_vanilla(A, lo, hi);          break;
    case SHELL_POWER_FOUR:      shell_sort_power_four(A, lo, hi);           break;
    case SHELL_SEDGEWICK:       shell_sort_sedgewick(A, lo, hi);            break;
    case SHELL_VANILLA:         shell_sort_vanilla(A, lo, hi);              break;
    case TREE_AVL:              tree_sort_avl(A, lo, hi);                   break;
    case TREE_BST:              tree_sort_bst(A, lo, hi);                   break;
    case TREE_REDBLACK:         tree_sort_red_black(A, lo, hi);             break;
    case TREE_SPLAY:            tree_sort_splay(A, lo, hi);                 break;
    case TREE_TTF:              tree_sort_ttf(A, lo, hi);                   break;
    default:                    exit_usage(argv[0]);                        break;
    }
    ArrayPrintVert(A, 0, size - 1);
    print_sort_type(sortType);
    ArrayFree(A, size);
    return 0;
}

// Check if the sort program type is valid.
static bool is_valid_type(Type sortType) {
    return (sortType >= MIN_TYPE && sortType <= MAX_TYPE);
}

// Print the sorting implementation used.
static void print_sort_type(Type sortType) {
    if (!BOOL_PRINT_TYPE_NAME) return;
    // printf("--------------------------------\n");
    printf("> ");
    switch (sortType) {
    case BOGO_ORIGINAL:         printf("Bogo Sort (Original)");                 break;
    case BOGO_PANCAKE:          printf("Bogo Sort (Pancake)");                  break;
    case BOGO_PERMUTATION:      printf("Bogo Sort (Permutation)");              break;
    case BOGO_PSEUDO:           printf("Bogo Sort (Pseudo/Bozo)");              break;
    case BOGO_SLOW:             printf("Bogo Sort (Slow Sort)");                break;
    case BOGO_STOOGE:           printf("Bogo Sort (Stooge)");                   break;
    case BUBBLE_ADAPTIVE:       printf("Bubble Sort (Adaptive)");               break;
    case BUBBLE_OBLIVIOUS:      printf("Bubble Sort (Oblivious)");              break;
    case COCKTAIL:              printf("Cocktail Shaker Sort");                 break;
    case COMB:                  printf("Comb Sort");                            break;
    case COUNTING:              printf("Counting Sort (Non-Comparison)");       break;
    case CYCLE:                 printf("Cycle Sort");                           break;
    case GNOME_OPTIMISED:       printf("Gnome Sort Optimised");                 break;
    case GNOME_VANILLA:         printf("Gnome Sort (Vanilla)");                 break;
    case HEAP_ADT:              printf("Heap Sort (ADT)");                      break;
    case HEAP_INPLACE:          printf("Heap Sort (Inplace)");                  break;
    case INSERTION_BINARY:      printf("Insertion Sort (Binary)");              break;
    case INSERTION_BITONIC:     printf("Insertion Sort (Bitonic)");             break;
    case INSERTION_VANILLA:     printf("Insertion Sort (Vanilla)");             break;
    case INTROSPECTIVE:         printf("Introspective Sort");                   break;
    case MERGE_BOTTOM_UP:       printf("Merge Sort (Bottom Up, Iterative)");    break;
    case MERGE_INPLACE_NAIVE:   printf("Merge Sort (Naive Inplace)");           break;
    case MERGE_INPLACE_OPT:     printf("Merge Sort (Optimised Inplace)");       break;
    case MERGE_TIMSORT:         printf("Tim Sort (Insertion + Merge)");         break;
    case MERGE_VANILLA:         printf("Merge Sort (Vanilla)");                 break;
    case ODD_EVEN:              printf("Odd-Even Sort");                        break;
    case PIGEON:                printf("Pigeonhole Sort (Non-Comparison)");     break;
    case PQUEUE:                printf("Priority Queue Sort");                  break;
    case QUICK_C_LIBRARY:       printf("Quick Sort (C Library qsort)");         break;
    case QUICK_DUAL_PIVOT:      printf("Quick Sort (Dual Pivot)");              break;
    case QUICK_HYBRID:          printf("Quick Sort (Hybrid Insertion)");        break;
    case QUICK_ITERATIVE:       printf("Quick Sort (Iterative, Stack)");        break;
    case QUICK_MEDIAN:          printf("Quick Sort (Median of Three)");         break;
    case QUICK_RANDOM_PIVOT:    printf("Quick Sort (Random Pivot)");            break;
    case QUICK_SHUFFLE:         printf("Quick Sort (Shuffle)");                 break;
    case QUICK_THREE_WAY:       printf("Quick Sort (Three way)");               break;
    case QUICK_VANILLA:         printf("Quick Sort (Vanilla)");                 break;
    case RADIX:                 printf("Radix Sort (Non-Comparison)");          break;
    case SELECTION_DUAL:        printf("Selection Sort (Dual Min/Max)");        break;
    case SELECTION_VANILLA:     printf("Selection Sort (Vanilla)");             break;
    case SHELL_POWER_FOUR:      printf("Shell Sort (Power of Four)");           break;
    case SHELL_SEDGEWICK:       printf("Shell Sort (Sedgewick-like)");          break;
    case SHELL_VANILLA:         printf("Shell Sort (Vanilla, Halving)");        break;
    case TREE_AVL:              printf("Tree Sort (AVL Height-Balanced)");      break;
    case TREE_BST:              printf("Tree Sort (BSTree Vanilla)");           break;
    case TREE_REDBLACK:         printf("Tree Sort (Red Black Tree)");           break;
    case TREE_SPLAY:            printf("Tree Sort (Splay Tree)");               break;
    case TREE_TTF:              printf("Tree Sort (Two-Three-Four BTree)");     break;
    default:                    printf("Unknown sort type");                    break;
    }
    printf("\n");
    // printf("--------------------------------\n");
}

// Print the usage message to standard error and exits.
static void exit_usage(char *program) {
    fprintf(/*stderr,*/ stdout,
        // "--------------------------------------------\n"
        " ID         ALGORITHM                  VARIANT\n\n"
        "%3d         Bogo Sort                  (Original)\n"
        "%3d         Bogo Sort                  (Pancake)\n"
        "%3d         Bogo Sort                  (Permutation)\n"
        "%3d         Bogo Sort                  (Pseudo/Bozo)\n"
        "%3d         Bogo Sort                  (Slow Sort)\n"
        "%3d         Bogo Sort                  (Stooge Sort)\n"
        "%3d         Bubble Sort                (Adaptive)\n"
        "%3d         Bubble Sort                (Oblivious)\n"
        "%3d         Cocktail Shaker Sort\n"    
        "%3d         Comb Sort\n"
        "%3d         Counting Sort              (Non-comparison, Stable)\n"
        "%3d         Cycle Sort\n"
        "%3d         Gnome Sort                 (Optimised)\n"
        "%3d         Gnome Sort                 (Vanilla)\n"
        "%3d         Heap Sort                  (ADT)\n"
        "%3d         Heap Sort                  (Inplace)\n"
        "%3d         Insertion Sort             (Binary Search)\n"
        "%3d         Insertion Sort             (Bitonic)\n"
        "%3d         Insertion Sort             (Vanilla)\n"
        "%3d         Introspective Sort         (Insertion, Heap, Quick)\n"
        "%3d         Merge Sort                 (Bottom Up, Iterative)\n"
        "%3d         Merge Sort                 (Naive Inplace)\n"
        "%3d         Merge Sort                 (Optimised Inplace)\n"
        "%3d         Merge Sort                 (Timsort, with Insertion)\n"
        "%3d         Merge Sort                 (Vanilla)\n"
        "%3d         Odd-Even Sort\n"
        "%3d         Pigeonhole Sort            (Non-Comparison, Stable)\n"
        "%3d         Priority Queue Sort\n"
        "%3d         Quick Sort                 (C-Library qsort)\n"
        "%3d         Quick Sort                 (Dual Pivot)\n"
        "%3d         Quick Sort                 (Hybrid with Insertion)\n"
        "%3d         Quick Sort                 (Iterative, Stack)\n"
        "%3d         Quick Sort                 (Median of Three)\n"
        "%3d         Quick Sort                 (Random Pivot)\n"
        "%3d         Quick Sort                 (Shuffle First)\n"
        "%3d         Quick Sort                 (Three Way)\n"
        "%3d         Quick Sort                 (Vanilla)\n"
        "%3d         Radix Sort                 (Non-Comparison, Stable)\n"
        "%3d         Selection Sort             (Min/Max Dual)\n"
        "%3d         Selection Sort             (Vanilla)\n"
        "%3d         Shell Sort                 (Power of Four)\n"
        "%3d         Shell Sort                 (Sedgewick-like)\n"
        "%3d         Shell Sort                 (Vanilla, Halving)\n"
        "%3d         Tree Sort                  (AVL Tree)\n"
        "%3d         Tree Sort                  (BSTree Vanilla)\n"
        "%3d         Tree Sort                  (Red Black Tree)\n"
        "%3d         Tree Sort                  (Splay Tree)\n"
        "%3d         Tree Sort                  (Two-Three-Four BTree)\n"
        "\n"
        "Usage: %s ID\n",
        BOGO_ORIGINAL,
        BOGO_PANCAKE,
        BOGO_PERMUTATION,
        BOGO_PSEUDO,
        BOGO_SLOW,
        BOGO_STOOGE,
        BUBBLE_ADAPTIVE,
        BUBBLE_OBLIVIOUS,
        COCKTAIL,
        COMB,
        COUNTING,
        CYCLE,
        GNOME_OPTIMISED,
        GNOME_VANILLA,
        HEAP_ADT,
        HEAP_INPLACE,
        INSERTION_BINARY,
        INSERTION_BITONIC,
        INSERTION_VANILLA,
        INTROSPECTIVE,
        MERGE_BOTTOM_UP,
        MERGE_INPLACE_NAIVE,
        MERGE_INPLACE_OPT,
        MERGE_TIMSORT,
        MERGE_VANILLA,
        ODD_EVEN,
        PIGEON,
        PQUEUE,
        QUICK_C_LIBRARY,
        QUICK_DUAL_PIVOT,
        QUICK_HYBRID,
        QUICK_ITERATIVE,
        QUICK_MEDIAN,
        QUICK_RANDOM_PIVOT,
        QUICK_SHUFFLE,
        QUICK_THREE_WAY,
        QUICK_VANILLA,
        RADIX,
        SELECTION_DUAL,
        SELECTION_VANILLA,
        SHELL_POWER_FOUR,
        SHELL_SEDGEWICK,
        SHELL_VANILLA,
        TREE_AVL,
        TREE_BST,
        TREE_REDBLACK,
        TREE_SPLAY,
        TREE_TTF,
        program
    );
    exit(1);
}
