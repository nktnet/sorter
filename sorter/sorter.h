// Khiet Tam Nguyen (z5313514)
// 25.08.20 - 05.09.20

// Sorting algorithm interface.

/*
    -------------------------------------------------------------------

    By default, only the key of each item is being compared, as the
    initial purpose of gen and sorter was to help distinguish between
    different sorting algorithms. Sorting by keys only will help to 
    discern whether the sort is stable or not.  This behaviour can be 
    altered by flipping the switch BOOL_COMPARE_KEY_ONLY in Item.h. 

    However, algorithms like Counting Sort, Pigeonhole Sort and Radix 
    Sort will not change their behaviour as desired as they are 
    non-comparison based algorithm.

    -------------------------------------------------------------------

    For Tree Sorts, the Regular Binary Search tree and Splay Tree will
    crash for inputs of ascending or descending keys when the number of
    keys exceed (approximately) 175000.
    This does not occur with AVL, Red-Black or 2-3-4 Trees since they
    are self-balancing.

    -------------------------------------------------------------------

    Pigeon Sort may appear to be the fastest, E.g when running

    $ time ./gen r 20000000 | sorter ID

    but this is only because the number generator written only generate keys
    from 0 .. N - 1 (where N is the number of items). Follow the links provided
    to understand how this favours pigeon sort (and also counting sort).

    -------------------------------------------------------------------

    Comb sort is surprising fast despite being a derivative of bubble
    sort :O. Has very little code yet still outperforms binary insertion
    sort and some of the other (seemingly) powerful sorts.

    -------------------------------------------------------------------

    Makefiles were generated with the perl script makemake.pl from COMP2041.
    Not relevant, but another cool perl program to make/clean/clobber on
    multiple directories is make_tree.pl.

    -------------------------------------------------------------------
*/

#ifndef SORTER_H
#define SORTER_H

/////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "Item.h"
#include "Array.h"

/////////////////////////////////////////////////////////

#define BOGO_OFFSET -6

typedef enum type {
    BOGO_ORIGINAL = BOGO_OFFSET,
    BOGO_PANCAKE,
    BOGO_PERMUTATION,
    BOGO_PSEUDO,
    BOGO_SLOW,
    BOGO_STOOGE,
    BUBBLE_ADAPTIVE,
    BUBBLE_OBLIVIOUS,
    COCKTAIL,
    COMB,
    COUNTING,
    CYCLE,
    GNOME_OPTIMISED,
    GNOME_VANILLA,
    HEAP_ADT,
    HEAP_INPLACE,
    INSERTION_BINARY,
    INSERTION_BITONIC,
    INSERTION_VANILLA,
    INTROSPECTIVE,
    MERGE_BOTTOM_UP,
    MERGE_INPLACE_NAIVE,
    MERGE_INPLACE_OPT,
    MERGE_TIMSORT,
    MERGE_VANILLA,
    ODD_EVEN,
    PIGEON,
    PQUEUE,
    QUICK_C_LIBRARY,
    QUICK_DUAL_PIVOT,
    QUICK_HYBRID,
    QUICK_ITERATIVE,
    QUICK_MEDIAN,
    QUICK_RANDOM_PIVOT,
    QUICK_SHUFFLE,
    QUICK_THREE_WAY,
    QUICK_VANILLA,
    RADIX,
    SELECTION_DUAL,
    SELECTION_VANILLA,
    SHELL_POWER_FOUR,
    SHELL_SEDGEWICK,
    SHELL_VANILLA,
    TREE_AVL,
    TREE_BST,
    TREE_REDBLACK,
    TREE_SPLAY,
    TREE_TTF,   /* Two-Three-Four Tree */
} Type;

#define MIN_TYPE (BOGO_ORIGINAL)
#define MAX_TYPE (TREE_TTF)

/////////////////////////////////////////////////////////

// Continuously shuffle the array and doing a linear
// scan until the array is sorted. Might want to grab
// a cup of coffee (make that several).
void bogo_sort_original(Array A, int lo, int hi);

// Sorts the array with only the reverse operations.
// Aims to do the minimum number of reverses.
void bogo_sort_pancake(Array A, int lo, int hi);

// Generate every permutation until the Avengers win
// as Doctor Strange have predicted. (Oh, and also 
// sort the array in the process).
void bogo_sort_permutation(Array A, int lo, int hi);

// Choose two random array elements, if out-of-order 
// then swap. Repeat until the list is sorted.
// This is actually not that bad.
void bogo_sort_pseudo(Array A, int lo, int hi);

// Multiply and Surrender Strategy - Unfortunate
// little brother of Merge Sort's Divide and Conquer.
// Official name is slowsort.
void bogo_sort_slow(Array A, int lo, int hi);

// Stoogesort is worse than bubble sort, but better 
// than the other bogos. See bogo_sort.c for details.
void bogo_sort_stooge(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Smarter bubble that terminates when there are no 
// swaps after an iteration.
void bubble_sort_adaptive(Array A, int lo, int hi);

// Oblivious bubble sort that always take O(n^2) time.
void bubble_sort_oblivious(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Bubble Sort that goes both ways, reducing traversals
// and avoid cases like {1,3,3,3,2} and {4,3,3,3,2}.
void cocktail_shaker_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Bubble sort with h intervals, similar to the
// relationtip between Shell Sort and Insertion Sort.
// Surprisingly very fast.
void comb_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Counting Sort Non-comparison Based Sorting Algorithm.
// (Except I cheated a bit with the ArrayGetMax function).
// NOTE: Only work when sorting keys, and keys must be >= 0.
void counting_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Cycle Sort - minimum writes.
void cycle_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// If two elements are in the right order, move forward.
// Otherwise, swap them and move backwards.
void gnome_sort(Array A, int lo, int hi); 

// Gnome sort with optimisation to become a variant
// of insertion sort.
void gnome_sort_optimised(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Heap sort using Heap ADT structure.
void heap_sort_adt(Array A, int lo, int hi);

// Heap sort without using additional storage space.
void heap_sort_inplace(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Insertion sort original implementation.
void insertion_sort_vanilla(Array A, int lo, int hi);

// Binary Insertion Sort - Halving the sorted array to find pos.
void insertion_sort_binary(Array A, int lo, int hi);

// Bitonic Sort (only works on 2^k elements, so sort as many 
// elements as possible, then binary insertion any remaining).
void insertion_sort_bitonic(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Hybrid between insertion sort, heapsort and quicksort.
// Always avoid the worst case of heap sort.
void introspective_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Bottom up merge sort (without recursion).
void merge_sort_bottom_up(Array A, int lo, int hi);

// In-place merge sort with O(n^2).
void merge_sort_naive_inplace(Array A, int lo, int hi);

// Optimised merge sort (see merge_sort.c for details).
void merge_sort_optimised_inplace(Array A, int lo, int hi);

// Vanilla implementation of merge sort (sub arrays).
void merge_sort_vanilla(Array A, int lo, int hi);

// Combines insertion sort and merge sort
void merge_timsort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Swapping in two phases: Odd indexes then Even Indexes
void odd_even_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

void pigeonhole_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Priority Queue Sort (Similar to Heap).
void priority_queue_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Using qsort from <stdlib.h>.
void quick_sort_c_library(Array A, int lo, int hi);

// Dual Pivot Quick Sort
void quick_sort_dual_pivot(Array A, int lo, int hi);

// Idea: Insertion sort is fast for small #nelements,
// Quick sort is quicker for larger #nelements.
void quick_sort_hybrid_insertion(Array A, int lo, int hi);

// Uses a stack-like array instead of recursion.
// Why? don't ask :'(.
void quick_sort_iterative(Array A, int lo, int hi);

// Median of three quicksort.
void quick_sort_median(Array A, int lo, int hi);

// Picks a random pivot between lo and hi.
void quick_sort_random_pivot(Array A, int lo, int hi);

// Uniformly shuffle the array once, then using 
// vanilla quick sort with last item as pivot.
void quick_sort_shuffle(Array A, int lo, int hi);

// Vanilla quick sort implementation - last item pivot.
void quick_sort_vanilla(Array A, int lo, int hi);

// 3-way quick sort - Partitions < | = | > pivot.
// Used to deal with duplicates (although is somehow slower).
void quick_sort_three_way(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Groups element's individual digits of equal place value 
// and rearranging according to increasing/decreasing order.
void radix_sort(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Selection sort.
void selection_sort_vanilla(Array A, int lo, int hi);

// Selection Sort Dual (Tracks both min and max).
void selection_sort_dual(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// Power Four's 4096, 1024, 256, 64, 16, 4, 1.
void shell_sort_power_four(Array A, int lo, int hi);

// Sedgewick's 4193, 107, 281, 77, 23, 8, 1 (The superior gaps!)
void shell_sort_sedgewick(Array A, int lo, int hi);

// Shell sort vanilla - Halving the gap each time (from size/2).
void shell_sort_vanilla(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

// AVL Tree Sort - Height balanced
void tree_sort_avl(Array A, int lo, int hi);

// Regular binary search tree sort.
void tree_sort_bst(Array A, int lo, int hi);

// Red-Black Tree Sort.
void tree_sort_red_black(Array A, int lo, int hi);

// Splay Tree Sort.
void tree_sort_splay(Array A, int lo, int hi);

// Two-Three-Four (B)tree sort.
void tree_sort_ttf(Array A, int lo, int hi);

/////////////////////////////////////////////////////////

#endif
