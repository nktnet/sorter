// Khiet Tam Nguyen (z5313514)
// 03.09.20

// Introspective sort (Or just IntroSort).
// Hybrid between insertion sort, heapsort and quicksort.
// Always avoid the worst case of heap sort.

// Pseudo Code:
// https://en.wikipedia.org/wiki/Introsort

// GeeksToGeeks Implementation (Java/Python) 
// https://www.geeksforgeeks.org/introsort-or-introspective-sort/

// Implementation in C++:
// https://www.techiedelight.com/introsort-algorithm/

#include "sorter.h"
#include <math.h>

/////////////////////////////////////////////////

// Helper
static int partition(Array A, int lo, int hi);
static void introRecursive(Array A, int lo, int hi, int maxdepth);

/////////////////////////////////////////////////

// Wrapper for hybrid between insertion sort, heapsort and quicksort.
void introspective_sort(Array A, int lo, int hi) {
    if (!A) return;
    int maxdepth = 2 * log(A_SIZE(lo, hi));
    introRecursive(A, lo, hi, maxdepth);
}

// Recursive hybrid introsort function.
static void introRecursive(Array A, int lo, int hi, int maxdepth) {
    if (hi - lo < 16) {
        insertion_sort_vanilla(A, lo, hi);
    } else if (maxdepth == 0) {
        heap_sort_inplace(A, lo, hi);
    } else {
        // Quicksort with decreasing max depth.
        int p = partition(A, lo, hi);
        introRecursive(A, lo, p - 1, maxdepth - 1); 
        introRecursive(A, p + 1, hi, maxdepth - 1); 
    }
}

// Same partition function as quicksort.
static int partition(Array A, int lo, int hi) { 
    Item pivot = A[hi];
    int i = lo - 1; 
    for (int j = lo; j < hi; j++) { 
        if (ItemCompare(A[j], pivot) < 0) { 
            i++;
            ItemSwap(&A[i], &A[j]); 
        } 
    } 
    ItemSwap(&A[i + 1], &A[hi]); 
    return i + 1; 
} 

