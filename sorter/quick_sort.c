// Khiet Tam Nguyen (z5313514)

// 25.08.20 - Added vanilla.
// 26.08.20 - Added random pivot and median of three.
// 27.08.20 - Added C library.
// 28.08.20 - Added shuffle quick sort.
// 29.08.20 - Added hybrid insertion quick sort.
// 01.09.20 - Added iterative quick sort using a stack.
// 03.09.20 - Added three way and dual pivot quick sort.

// Inspired by vanilla Quick Sort from GeeksToGeeks
// https://www.geeksforgeeks.org/quick-sort/

// Ideas for different variations (i.e. median of three, 
// shuffle, etc) were suggested by COMP2521 Lecture Slides.

// Quicksort implementations:
/*
   > Vanilla - Picks last element as pivot

   > Random Pivot - Always choose a random pivot
   to avoid the worst case.

   > Do a uniform shuffle on the array before performing
   vanilla quick sort.

   > Median of Three - Picks a better pivot 
   (middle-valued element) to avoid the worst case.

   > C Library - Using the given qsort function

   > Hybrid Insertion - Combination of regular insertion 
   sort and quick sort.

*/

#include "sorter.h"

#define HYBRID_THRESHOLD 10

////////////////////////////////////////////////////////

// Helper functions
// Partition function is in tools.c
static int partition(Array A, int lo, int hi);
static void median_of_three(Array A, int lo, int hi);
static void pick_random_pivot(Array A, int lo, int hi);
static void partition_three_way(Array A, int lo, int hi, int *i, int *j);
static int partition_dual_pivot(Array A, int lo, int hi, int* lp);

// Recursive Quicksort Helpers
static void qs_dual_pivot(Array A, int lo, int hi);
static void qs_hybrid(Array A, int lo, int hi);
static void qs_median(Array A, int lo, int hi);
static void qs_random(Array A, int lo, int hi);
// no qs_shuffle since it is just qs_vanilla after shuffling
static void qs_three_way(Array A, int lo, int hi);
static void qs_vanilla(Array A, int lo, int hi);

// Iterative Quicksort Using Stack
static void iqs_stack(Array A, int lo, int hi);

////////////////////////////////////////////////////////

// Using Qsort from Standard Lib to Solve recursively.
void quick_sort_c_library(Array A, int lo, int hi) {
    if (!A) return;
    qsort(&A[lo], A_SIZE(lo, hi), sizeof(Item), ItemCompareQsort); 
}

// Uses two pivots instead of one.
// Partitions ... pivotL | .... | PivotR ...
void quick_sort_dual_pivot(Array A, int lo, int hi) { 
    if (!A) return;
    qs_dual_pivot(A, lo, hi);
} 


// Idea: Insertion sort is fast for small #nelements,
// Quick sort is quicker for larger #nelements.
void quick_sort_hybrid_insertion(Array A, int lo, int hi) { 
    if (!A) return;
    qs_hybrid(A, lo, hi);
} 

// Vanilla quicksort using a stack ADT (for fun?)
void quick_sort_iterative(Array A, int lo, int hi) {
    if (!A) return;
    iqs_stack(A, lo, hi);
}

// Pick the median-valued item between lo, hi and mid
void quick_sort_median(Array A, int lo, int hi) { 
    if (!A) return;
    qs_median(A, lo, hi);
} 

// Always pick a pivot at random
void quick_sort_random_pivot(Array A, int lo, int hi) {
    if (!A) return;
    // srand(time(NULL));
    qs_random(A, lo, hi);
}

// Uniformly shuffle the array once, then using 
// vanilla quick sort with last item as pivot.
void quick_sort_shuffle(Array A, int lo, int hi) {
    if (!A) return;
    ArrayShuffle(A, lo, hi);
    qs_vanilla(A, lo, hi);
}

// Divides array into three part 
void quick_sort_three_way(Array A, int lo, int hi) {
    if (!A) return;
    qs_three_way(A, lo, hi);
}

// Always pick the last item as the pivot.
void quick_sort_vanilla(Array A, int lo, int hi) { 
    if (!A) return;
    qs_vanilla(A, lo, hi);
} 

//////////////////////////////////////////////////////////
//                  Recursive Helpers                   //
//////////////////////////////////////////////////////////

// Recursive helper for quick insertion sort hybrid 
static void qs_hybrid(Array A, int lo, int hi) {
    while (lo < hi) { 
        if (A_SIZE(lo, hi) < HYBRID_THRESHOLD) { 
            insertion_sort_vanilla(A, lo, hi); 
            break; 
        } else { 
            int pivot = partition(A, lo, hi) ; 
            if (pivot - lo < hi - pivot) { 
                qs_hybrid(A, lo, pivot - 1);  
                lo = pivot + 1; 
            } else { 
                qs_hybrid(A, pivot + 1, hi); 
                hi = pivot - 1; 
            } 
        } 
    } 
}

// Recursive helper for median of three quicksort.
static void qs_median(Array A, int lo, int hi) {
    if (hi <= lo) return;
    median_of_three(A, lo, hi);
    int p = partition(A, lo, hi); 
    quick_sort_median(A, lo, p - 1); 
    quick_sort_median(A, p + 1, hi); 
}

// Recursive helper for random pivot quicksort.
static void qs_random(Array A, int lo, int hi) {
    if (hi <= lo) return;
    pick_random_pivot(A, lo, hi);
    int p = partition(A, lo, hi); 
    quick_sort_random_pivot(A, lo, p - 1); 
    quick_sort_random_pivot(A, p + 1, hi); 
}

// Recursive helper for three way quicksort.
static void qs_three_way(Array A, int lo, int hi) {
    if (hi <= lo) return;
    int i, j;
    partition_three_way(A, lo, hi, &i, &j);
    qs_three_way(A, lo, j);
    qs_three_way(A, i, hi);
}

// Recursive helper for vanilla quicksort.
static void qs_vanilla(Array A, int lo, int hi) {
    if (hi <= lo) return;
    int p = partition(A, lo, hi); 
    quick_sort_vanilla(A, lo, p - 1); 
    quick_sort_vanilla(A, p + 1, hi); 
}

////////////////////////////////////////////////////////
//                   Other Helpers                    //
////////////////////////////////////////////////////////

// Partition function for the different quick sorts
// Always take value at index "hi" as the pivot.
static int partition(Array A, int lo, int hi) { 
    Item pivot = A[hi];
    int i = lo - 1; 
    for (int j = lo; j < hi; j++) { 
        if (ItemCompare(A[j], pivot) < 0) { 
            i++;
            ItemSwap(&A[i], &A[j]); 
        } 
    } 
    ItemSwap(&A[i + 1], &A[hi]); 
    return i + 1; 
} 

// Swapping the elements to make sure that
// the median stays at the end (be pivot)
static void median_of_three(Array A, int lo, int hi) {
    // Same as (lo + hi) / 2, but avoids overflo of ints.
    int mid = lo + (hi - lo) / 2;
    // Move median-valued element move to the end
    if (ItemCompare(A[hi], A[lo]) < 0)
        ItemSwap(&A[hi], &A[lo]);
    if (ItemCompare(A[mid], A[hi]) < 0)
        ItemSwap(&A[mid], &A[hi]);
    if (ItemCompare(A[hi], A[lo]) < 0)
        ItemSwap(&A[hi], &A[lo]);
    // Order:  lo      mid     hi
    // Value: A[lo] < A[hi] < A[mid]
}

// Choose a random element to be the pivot, ensuring that
// it is in the range  A[lo] <= pivot <= A[hi].
static void pick_random_pivot(Array A, int lo, int hi) {
    int rI = rand() % (hi - lo) + lo;
    ItemSwap(&A[rI], &A[hi]);
}

////////////////////////////////////////////////////////
//                  Iterative Helper                  //
////////////////////////////////////////////////////////

// Adapted from:
// https://www.geeksforgeeks.org/iterative-quick-sort/

static void iqs_stack(Array A, int lo, int hi) {
    // Create an auxiliary stack 
    int *stack = malloc(sizeof(int) * A_SIZE(lo, hi)); 
    int top = -1;
    // push initial values of lo and hi to stack 
    stack[++top] = lo; 
    stack[++top] = hi; 
    // Keep popping from stack while is not empty 
    while (top >= 0) { 
        // Pop h and l 
        hi = stack[top--]; 
        lo = stack[top--]; 
        // Set pivot element at its correct position 
        // in sorted array 
        int p = partition(A, lo, hi); 
        // If there are elements on left side of pivot, 
        // then push left side to stack 
        if (p - 1 > lo) { 
            stack[++top] = lo; 
            stack[++top] = p - 1; 
        } 
        // If there are elements on right side of pivot, 
        // then push right side to stack 
        if (p + 1 < hi) { 
            stack[++top] = p + 1; 
            stack[++top] = hi; 
        } 
    }
    free(stack);
}

////////////////////////////////////////////////////////
//                  Three Way Helper                  //
////////////////////////////////////////////////////////

// Base code provided by Tan:
// https://ide.geeksforgeeks.org/ZDEEjbelWh
// Found as a comment to GeeksToGeeks Quick Sort Three Way.

// Partition for quicksort three way 
// when comparing to pivot, < | = | >
// Works like regular quicksort when there are no duplicates.
static void partition_three_way(Array A, int lo, int hi, int *i, int *j) {
    Item pivot = A[hi];
    int equal = lo;
    int k;
    for (k = lo; k <= hi; k++) {
        if (ItemCompare(A[k], pivot) < 0) {
            ItemSwap(&A[k], &A[equal]);
            equal++;
        } else if (ItemCompare(A[k], pivot) > 0 ) {
            ItemSwap(&A[k], &A[hi]);
            hi--;
            k--;
        }
    }
    *i = k;
    *j = equal - 1;
}

////////////////////////////////////////////////////////
//                  Dual Pivot Helper                 //
////////////////////////////////////////////////////////

// Keeps track of two pivots.
// https://www.geeksforgeeks.org/dual-pivot-quicksort/
static void qs_dual_pivot(Array A, int lo, int hi) {
    if (hi <= lo) return;
    int lp, rp; 
    rp = partition_dual_pivot(A, lo, hi, &lp); 
    qs_dual_pivot(A, lo, lp - 1); 
    qs_dual_pivot(A, lp + 1, rp - 1); 
    qs_dual_pivot(A, rp + 1, hi); 
}

// Helper partitioning function for two pivots.
// Returns the right pivot, changes the left pivot internally.
static int partition_dual_pivot(Array A, int lo, int hi, int* lp) { 
    if (ItemCompare(A[lo], A[hi]) > 0) {
        ItemSwap(&A[lo], &A[hi]); 
    }
    int j = lo + 1; 
    int k = lo + 1;
    int g = hi - 1; 
    Item pivotL = A[lo]; 
    Item pivotR = A[hi];
    while (k <= g) { 
        if (ItemCompare(A[k], pivotL) < 0) { 
            // Elements are less than left pivot.
            ItemSwap(&A[k], &A[j]); 
            j++; 
        } else if (ItemCompare(A[k], pivotR) >= 0) { 
            // Elements are greater than or equal to right pivot.
            while (ItemCompare(A[g], pivotR) > 0 && k < g) {
                g--; 
            }
            ItemSwap(&A[k], &A[g]); 
            g--; 
            if (ItemCompare(A[k], pivotL) < 0) { 
                ItemSwap(&A[k], &A[j]); 
                j++; 
            } 
        } 
        k++; 
    } 
    j--; 
    g++; 
    // Move pivot to approprite position.
    ItemSwap(&A[lo], &A[j]); 
    ItemSwap(&A[hi], &A[g]); 
    *lp = j;
    return g; 
} 
