// Khiet Tam Nguyen (z5313514)

// 25.08.20 - Stupid Pseudo-Bogo sort written for fun.
//          - EDIT: This is the greatest program ever.
// 29.08.20 - Added the original bogo sort.
// 30.08.20 - Added slow sort and stooge sort.
//          - Added permutation sort.
// 01.09.20 - Added pancake sort.

// #include <time.h>
#include <stdlib.h>

#include "sorter.h"

///////////////////////////////////////////////////

// Helpers for wrappers.
static bool permute(Array A, int left, int right, int lo, int hi);
static void iterative_pseudo(Array A, int lo, int hi);
static void slowRecurse(Array A, int lo, int hi);
static void stoogeRecurse(Array A, int lo, int hi);
static void pancake(Array A, int lo, int hi);

// Other Helpers.
static int getMaxIndex(Array A, int lo, int hi);

///////////////////////////////////////////////////

// Keep shuffling until the array is finally sorted.
// May want to grap a cup of coffee (or a couple :P).
void bogo_sort_original(Array A, int lo, int hi) {
    if (!A) return;
    while (!ArrayIsSorted(A, lo, hi)) {
        ArrayShuffle(A, lo, hi);
    }
}

// Sorts the array with only the reverse operations.
// Aims to do the minimum number of reverses.
void bogo_sort_pancake(Array A, int lo, int hi) {
    if (!A) return;
    pancake(A, lo, hi);
}

// Generate every possible permutations one by one until 
// the Avengers win, as Doctor Strange have predicted.
// P.s. it also chooses a sorted array in the process.
void bogo_sort_permutation(Array A, int lo, int hi) {
    if (!A) return;
    bool sorted = permute(A, lo, hi, lo, hi);
    assert(sorted);
}

// Idea: choose two random array elements, if out-of-order 
// then swap, repeat until the array is finally sorted.
void bogo_sort_pseudo(Array A, int lo, int hi) {
    // srand(time(NULL));
    if (!A) return;
    iterative_pseudo(A, lo, hi);
}

// Based on the principle of "Multiply and Surrender", 
// which aims to oppose merge sort's divide and conquer :P.
void bogo_sort_slow(Array A, int lo, int hi) {
    if (!A) return;
    slowRecurse(A, lo, hi);
}

// Worst than bubble sort, but better than slow sort.
// See wikipedia for details (links in helpers).
void bogo_sort_stooge(Array A, int lo, int hi) {
    if (!A) return;
    stoogeRecurse(A, lo, hi);
}

///////////////////////////////////////////////////

///////////////////////////////////////////////////
//                    Helpers                    //
///////////////////////////////////////////////////

// Permutation helper for bogo_sort_permutation
// Find all possible combinations until a sorted one appears.
static bool permute(Array A, int left, int right, int lo, int hi) {
    if (right <= left) {
        return ArrayIsSorted(A, lo, hi);
    }
    for (int i = left; i <= right; i++) {
        ItemSwap(&A[left], &A[i]);
        if (permute(A, left + 1, right, lo, hi)) {
            return true;
        }
        ItemSwap(&A[left], &A[i]);
    }
    return false;
}

// Keep swapping two random elements (if necessary)
// until the array is sorted.
static void iterative_pseudo(Array A, int lo, int hi) {
    if (lo >= hi) return;
    while (!ArrayIsSorted(A, lo, hi)) {
        int r1 = rand() % (hi + 1);
        int r2 = rand() % (hi + 1);
        if (r1 > r2 && ItemCompare(A[r1], A[r2]) < 0) {
            ItemSwap(&A[r1], &A[r2]);
        }
    }
}

// Sorts the array with only the reverse operations.
// Aims to do the minimum number of reverses.
// Wiki Link: https://en.wikipedia.org/wiki/Pancake_sorting
static void pancake(Array A, int lo, int hi) { 
    if (hi <= lo) return;
    for (int currHi = hi; currHi > lo; currHi--) { 
        int maxI = getMaxIndex(A, lo, currHi);
        if (maxI < hi) { 
            ArrayReverse(A, lo, maxI);
            ArrayReverse(A, lo, currHi); 
        } 
    } 
} 

// Pseudo Code:
// https://en.wikipedia.org/wiki/Slowsort
// Recursive helper function for slow sort.
static void slowRecurse(Array A, int lo, int hi) {
    if (lo >= hi) return;
    // Same as mid = (lo + hi) / 2.
    int mid = lo + (hi - lo) / 2;
    slowRecurse(A, lo, mid);
    slowRecurse(A, mid + 1, hi);
    if (ItemCompare(A[hi], A[mid]) < 0) {
        ItemSwap(&A[hi], &A[mid]);
    }
    slowRecurse(A, lo, hi - 1);
}

/*
   From Wikipedia: https://en.wikipedia.org/wiki/Stooge_sort
   The algorithm is defined as follows:
   If the value at the start is larger than the value at the end, swap them.
   If there are 3 or more elements in the list, then:
   Stooge sort the initial 2/3 of the list
   Stooge sort the final 2/3 of the list
   Stooge sort the initial 2/3 of the list again
   It is important to get the integer sort size used in the recursive calls by 
   rounding the 2/3 upwards, e.g. rounding 2/3 of 5 should give 4 rather than 3, 
   as otherwise the sort can fail on certain data. 

   There is also a GeeksToGeeks Implementation:
https://www.geeksforgeeks.org/stooge-sort/
*/

// Recursive helper function for stooge sort.
static void stoogeRecurse(Array A, int lo, int hi) {
    if (ItemCompare(A[lo], A[hi]) > 0) {
        ItemSwap(&A[lo], &A[hi]);
    }
    int n = A_SIZE(lo, hi);
    if (n < 3) return;
    int t = n / 3;
    stoogeRecurse(A, lo, hi - t);
    stoogeRecurse(A, lo + t, hi);
    stoogeRecurse(A, lo, hi - t);
}

/////////////////////////////////////////////////////////
//                    Other Helpers                    //
/////////////////////////////////////////////////////////
static int getMaxIndex(Array A, int lo, int hi) {
    int maxI = lo;
    Item max = A[lo];
    for (int i = lo + 1; i <= hi; i++) {
        if (ItemCompare(max, A[i]) < 0) {
            max = A[i];
            maxI = i;
        }
    }
    return maxI;
}
