// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Implementation of heap sort using
// > a heap adt
// > an inplace heap.

// This is similar to the Priority Queue Sort

// Algorithm adapted from COMP2521 20T2 Lecture Slides
// By John Shepherd
// http://www.cse.unsw.edu.au/~cs2521/20T2/lecs/heapsort/slides.html#s1

#include "Heap.h"
#include "sorter.h"

/////////////////////////////////////////////////////////////

// Helper
static void fixDown(Array A, int i, int N);

/////////////////////////////////////////////////////////////

// Sorting using the heap ADT.
void heap_sort_adt(Array A, int lo, int hi) {
    if (!A) return;
    Heap h = HeapNew(hi + 1);
    for (int i = lo; i <= hi; i++) {
        HeapInsert(h, A[i]);
    }
    for (int i = hi; i >= lo; i--) {
        A[i] = HeapDelete(h);
    }
    HeapFree(h);
}

/////////////////////////////////////////////////////////////

// Heap sort in-place (without needing extra ADTs).
void heap_sort_inplace(Array A, int lo, int hi) {
    if (!A) return;
    // start addr of heap
    Item *h = A + lo - 1; 
    // construct heap in a[0..N-1]
    int N = A_SIZE(lo, hi);
    for (int i = N/2; i > 0; i--) {
        fixDown(h, i, N);
    }
    // use heap to build sorted array
    while (N > 1) {
        // put largest value at end of array
#if 1
        // Basically doing swap(h, 1, N);
        Item temp = h[1];
        h[1] = h[N];
        h[N] = temp;
#else
        ItemSwap(&A[lo - 1], &A[N + lo - 1]);
#endif
        // heap size reduced by one
        N--;
        // restore heap property after swap
        fixDown(h, 1, N);
    }
}

///////////////////////////////////////////////////////////////
//                    Helper for In-place                    //
///////////////////////////////////////////////////////////////

// force value at a[i] into correct position in a[1..N] 
// note that N gives max index *and* number of items
static void fixDown(Array A, int i, int N) {
    while (2*i <= N) {
        // compute address of left child
        int j = 2*i;
        // choose larger of two children
        if (j < N && ItemCompare(A[j], A[j+1]) < 0) j++;
        if (ItemCompare(A[i], A[j]) >= 0) break;
        ItemSwap(&A[i], &A[j]);
        // move one level down the heap
        i = j;
    }
}
