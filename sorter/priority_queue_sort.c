// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Implementation of a priority queue sort 
// using a pq adt (slow since it's a list).
// (This is similar to heap sort).

// Idea from COMP2521 20T2 Lecture Slides, 
// PQueue ADT also rewritten from John Shepherd's version.

#include "PQueue.h"
#include "sorter.h"

// Sorting using a priority queue
void priority_queue_sort(Array A, int lo, int hi) {
    if (!A) return;
    PQueue pq = PQueueNew();
    for (int i = lo; i <= hi; i++) {
        PQueueJoin(pq, A[i]);
    }
    for (int i = hi; i >= lo; i--) {
        A[i] = PQueueLeave(pq);
    }
    PQueueDrop(pq);
}
