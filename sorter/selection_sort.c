// Khiet Tam Nguyen (z5313514)

// 25.08.20 - Added regular selection sort (tracks min).
// 29.08.20 - Added dual selection sort (tracks min and max).

// Base code adapted from GeeksToGeeks:
// Vanilla:
// https://www.geeksforgeeks.org/selection-sort/
// Dual Selection (min/max)
// https://www.geeksforgeeks.org/sorting-algorithm-slightly-improves-selection-sort/

#include "sorter.h"

// Find minimum value and swap it to the front.
void selection_sort_vanilla(Array A, int lo, int hi) {
    if (!A) return;
    for (int i = lo; i <= hi; i++) {
        int min = i;
        for (int j = i + 1; j <= hi; j++) {
            if (ItemCompare(A[j], A[min]) < 0) {
                // Update the minimum value.
                min = j;
            }
        }
        ItemSwap(&A[i], &A[min]);
    }
}

// Swap min and max items into correct position 
// during each iteration.
void selection_sort_dual(Array A, int lo, int hi) {
    if (!A) return;
    for (int i = lo, j = hi; i < j; i++, j--) {
        int iMin = i;
        int iMax = i;
        Item maxIt = A[i];
        for (int k = i; k <= j; k++) {
            if (ItemCompare(A[k], A[iMin]) < 0) {
                // Update the minimum value.
                iMin = k;
            } else if (ItemCompare(A[k], A[iMax]) > 0) {
                iMax = k;
                maxIt = A[k];
            }
        }
        ItemSwap(&A[i], &A[iMin]);
        if (ItemCompare(A[iMin], maxIt) == 0){
            // Have just swapped max item with min in last step.
            // (Or at least an item with equivalent value).
            ItemSwap(&A[j], &A[iMin]);
        } else {
            ItemSwap(&A[j], &A[iMax]);
        }
    }
}
