// Khiet Tam Nguyen (z5313514)
// 04.09.20

// Inspired by the code here:
// https://programology.wordpress.com/2015/08/28/c-program-for-pigeonhole-sort/
// However, mostly rewritten for clarity and conciseness 
// (plus compatitiblity), since the original is... a little weird.

// For explanation, see GeeksToGeeks:
// https://www.geeksforgeeks.org/pigeonhole-sort/

#include "sorter.h"

/////////////////////////////////////////////////////

// Helpers
static void pigeon(Array A, int n, int min, int max);
static void changeMinMax(Array A, Item *minItem, Item *maxItem, int lo, int hi);

/////////////////////////////////////////////////////

// Sorts the array using the pigeonhle principle.
void pigeonhole_sort(Array A, int lo, int hi) {
    if (!A || hi <= lo) return;
    int size = A_SIZE(lo, hi);
    Item minItem; 
    Item maxItem; 
    changeMinMax(A, &minItem, &maxItem, lo, hi);
    // Give only A[lo] .. A[hi] section of array.
    pigeon(&A[lo], size, Key(minItem), Key(maxItem));
}

/////////////////////////////////////////////////////

// Helper function to update the minimum and maximum valued item
// from the given array and min/max pointers.
static void changeMinMax(Array A, Item *minItem, Item *maxItem, int lo, int hi) {
    *minItem = A[lo];
    *maxItem = A[lo];
    for (int i = lo + 1; i <= hi; i++) {
        if (ItemCompare(*minItem, A[i]) > 0) {
            *minItem = A[i];
        } else if (ItemCompare(*maxItem, A[i]) < 0) {
            *maxItem = A[i];
        }
    }
}

// Helper function that sorts the entire given array 
// using the pigeon hole principle.
// Also work for negative numbers since minusing min
// (double negative) naturally increases the range.
static void pigeon(Array A, int n, int min, int max) {
    int range = A_SIZE(min, max);
    // Array of pigeonholes
    int *holes = calloc(range, sizeof(int));
    // Populate the pigeonholes.
    for (int i = 0; i < n; i++) {
        holes[Key(A[i]) - min]++;
    }
    // Put the elements back into the Array in order.
    int k = 0;
    for (int i = 0; i < range; i++) {
        while (holes[i]-- > 0) {
            ItemChangeKey(A[k++], i + min);
        }
    }
    free(holes);
}
