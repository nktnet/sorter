// Khiet Tam Nguyen (z5313514)
// 27.08.20

// Cycle sort - See explation here:
// https://www.youtube.com/watch?v=1E1Vnq5EsYg

// Code adapted from Geeks for Geeks:
// https://www.geeksforgeeks.org/cycle-sort/

#include "sorter.h"

// Function sort the array using Cycle sort 
void cycle_sort(Array A, int lo, int hi) { 
    if (!A) return;
    // traverse array elements and put it to on the right place 
    for (int cycleStart = lo; cycleStart < hi; cycleStart++) { 
        // initialize item as starting point 
        Item item = A[cycleStart]; 
        // Find position where we put the item. We basically 
        // count all smaller elements on right side of item. 
        int pos = cycleStart; 
        for (int i = cycleStart + 1; i <= hi; i++) {
            if (ItemCompare(A[i], item) < 0) {
                pos++; 
            }
        }
        // If item is already in correct position 
        if (pos == cycleStart) continue; 
        // ignore all duplicate elements 
        while (ItemCompare(item, A[pos]) == 0) pos++; 
        // put the item to it's right position 
        if (pos != cycleStart) { 
            ItemSwap(&item, &A[pos]); 
        } 
        // Rotate rest of the cycle 
        while (pos != cycleStart) { 
            pos = cycleStart; 
            // Find position where we put the element 
            for (int i = cycleStart + 1; i <= hi; i++) {
                if (ItemCompare(A[i], item) < 0) {
                    pos += 1; 
                }
            }
            // ignore all duplicate  elements 
            while (ItemCompare(item, A[pos]) == 0) {
                pos += 1; 
            }
            // put the item to it's right position 
            if (ItemCompare(item, A[pos]) != 0) { 
                ItemSwap(&item, &A[pos]); 
            } 
        } 
    } 
} 
