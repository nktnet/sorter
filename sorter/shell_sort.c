// Khiet Tam Nguyen (z5313514)
// 27.08.20 - Added Powers of Four and Sedgewick
// 03.09.20 - Added Vanilla Shell Sort for comparison (halving gap)

// Implementations of shell short
// Powers of Four and Sedgewick

// Rewritten from COMP2521 Lecture Slides.
// Ideas for hvals are from Lab09 Sort Detective.

// GeeksForGeeks has a similar shell sort program written
// in C++, which halfs the gap each time.
// https://www.geeksforgeeks.org/shellsort/

#include "sorter.h"

#define NUM_POWERS 16

//////////////////////////////////////////////////

// Helper
static void shell_helper(Array A, int lo, int hi, int *hvals);

//////////////////////////////////////////////////

// Power Four's 4096, 1024, 256, 64, 16, 4, 1
void shell_sort_power_four(Array A, int lo, int hi) {
    if (!A) return;
    int hvals[NUM_POWERS] = {
        1073741824, 268435456, 
        67108864, 16777216, 
        4194304, 1048576, 
        262144, 65536, 
        16384, 4097, 
        1024, 256, 
        64, 16, 
        4, 1
    };
    shell_helper(A, lo, hi, hvals);
}

// Sedgewick's 4193, 107, 281, 77, 23, 8, 1
void shell_sort_sedgewick(Array A, int lo, int hi) {
    if (!A) return;
    int hvals[NUM_POWERS] = {
        1073790977, 268460033, 
        67121153, 16783361, 
        4197377, 1050113, 
        262913, 65921, 
        16577, 4193, 
        1073, 281, 
        77, 23, 
        8, 1
    };
    shell_helper(A, lo, hi, hvals);
}

// Similar to insertion sort, but going by 'h' intervals.
static void shell_helper(Array A, int lo, int hi, int *hvals) {
    if (!A) return;
    for (int g = 0; g < NUM_POWERS; g++) {
        int h = hvals[g];
        int start = lo + h;
        for (int i = start; i <= hi; i++) {
            Item temp = A[i];
            int j;
            for (j = i; j >= start; j -= h) {
                if (ItemCompare(A[j - h], temp) < 0) break;
                A[j] = A[j - h];
            }
            A[j] = temp;
        }
    }
}

/////////////////////////////////////////////////////////

// Original implementation which halfs the gap each time.
void shell_sort_vanilla(Array A, int lo, int hi) { 
    if (!A) return;
    int size = A_SIZE(lo, hi);
    for (int h = size/2; h > 0; h /= 2) { 
        int start = lo + h;
        for (int i = start; i <= hi; i += 1) { 
            Item temp = A[i]; 
            int j;             
            for (j = i; j >= start; j -= h) {
                if (ItemCompare(A[j - h], temp) < 0) break;
                A[j] = A[j - h]; 
            }
            A[j] = temp; 
        } 
    } 
} 
