#!/usr/bin/sh

PATH=$PATH:.

RED="\033[91m"
GREEN="\033[92m"
BLUE="\033[94m"
YELLOW="\033[33m"
MAGENTA="\033[35m"
DEFAULT="\033[0m"

gen="gen"
sorter="sorter"

if [ ! -e "$gen" ]; then
    echo "Cannot find number generator: $gen"
    exit 1
elif [ ! -e "$sorter" ]; then
    echo "Cannot find sorting program: $sorter"
    exit 1
fi

if [ $# -lt 2 ]; then
    echo "Usage: $0 SEQUENCE NUM [SEED]"
    echo "See the '$gen' program for details"
    exit 1
fi

sequence=$1
num=$2
seed=$3

resultDir="result"
mkdir -p "$resultDir"

infile="$resultDir/infile.in"
expfile="$resultDir/expected.exp"

./${gen} $sequence $num $seed | cut -d' ' -f1 > "$infile"
sort -n < $infile > "$expfile"

echo "Comparing with the Unix Sort Command:"
echo "$ $gen $sequence $num $seed | cut -d ' ' -f1 | sorter <num>"

numSortAlgo=41
i=0
while [ $i -le $numSortAlgo ]; do
    outfile="${resultDir}/${i}.out"
    ./${sorter} $i < "$infile" | egrep -v "^>" > $outfile

    printf "$sorter %0d " $i
    if diff -qb "$outfile" "$expfile"; then
        printf "${GREEN}PASSED${DEFAULT}\n"
    else
        printf "${RED}FAILED${DEFAULT}\n"
    fi
    i=$(($i + 1))
done
