// Khiet Tam Nguyen (z5313514)
// 27.08.20

// Odd-Even sort - Variation of Bubble Sort.
// Perform bubble sort in two phases - on odd indexes
// and even indexes.

// Rewritten from the C++ Base Code from GeeksToGeeks:
// https://www.geeksforgeeks.org/odd-even-sort-brick-sort/

#include "sorter.h"

////////////////////////////////////////////////

// Helper 
static bool swap_loop(Array A, int init, int hi);

////////////////////////////////////////////////

void odd_even_sort(Array A, int lo, int hi) { 
    if (!A) return;
    bool swapped = true; 
    while (swapped) { 
        // Swapping odd/even in alternation
        swapped = swap_loop(A, lo + 1, hi);
        swapped = swap_loop(A, lo, hi);
    } 
} 

// Returns true if there was a swap, and false otherwise.
static bool swap_loop(Array A, int init, int hi) {
    bool swapped = false;
    for (int i = init; i < hi; i += 2) { 
        if (ItemCompare(A[i], A[i + 1]) > 0) { 
            ItemSwap(&A[i], &A[i + 1]); 
            swapped = true; 
        } 
    } 
    return swapped;
}
