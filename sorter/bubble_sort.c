// Khiet Tam Nguyen (z5313514)

// 25.08.20 - Bubble sort algorithms (Oblivious and Adaptive)

// Algorithms were adapted from COMP2521 20T2 Lecture Slides
// (Courtesy of Dr John Shepherd).

// GeeksToGeeks also has an implementation of Bubble Sort (Up):
// https://www.geeksforgeeks.org/bubble-sort/

// Note: The implementations below are bubble down.

#include "sorter.h"

////////////////////////////////////////////////////////

// Naive bubble sort algorithm that always run on O(n^2)
void bubble_sort_oblivious(Array A, int lo, int hi) { 
    if (!A) return;
    for (int i = lo; i < hi; i++) {
        for (int j = hi; j > i; j--) {
            if (ItemCompare(A[j], A[j - 1]) < 0) {
                ItemSwap(&A[j], &A[j - 1]); 
            }
        }
    }
} 

// Smarter bubble sort which terminates
// when there are no swaps on an iteration.
void bubble_sort_adaptive(Array A, int lo, int hi) {
    if (!A) return;
    for (int i = lo; i < hi; i++) { 
        bool swapped = false;
        for (int j = hi; j > i; j--) { 
            if (ItemCompare(A[j], A[j - 1]) < 0) { 
                ItemSwap(&A[j], &A[j - 1]); 
                swapped = true; 
            } 
        }  
        if (!swapped) break; 
    } 
} 
