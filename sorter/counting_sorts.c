// Khiet Tam Nguyen (z5313514)
// 01.09.20

// Counting Sort (Stable).
// Non-comparison Based Sorting Algorithm

// Kinda cheated a bit with the ArrayGetMax though (used comparisons).

// Adapted from the skeleton code provided by: 
// https://www.techiedelight.com/counting-sort-algorithm-implementation/

// Useful video explanation (By CS Dojo):
// https://www.youtube.com/watch?v=OKd534EWcdk

// NOTE: This algorithm only works when comparing the keys only, and
// the keys must also be non-negative (keys >= 0).

#include "sorter.h"

///////////////////////////////////////////////////

// Helpers
static void count(Array A, int size, int range);

///////////////////////////////////////////////////

// Sorts by calculating start index of each value.
void counting_sort(Array A, int lo, int hi) {
    if (!A) return;
    // (OOPS so much for non-comparison).
    Item max = ArrayGetMax(A, lo, hi);
    int range = Key(max) + 1;
    count(&A[lo], hi - lo + 1, range);
}

///////////////////////////////////////////////////

// Helper function for counting sort
static void count(Array A, int size, int range) {
    Array output = ArrayNew(size);
    int *freq = calloc(range, sizeof(int));
    // using value of integer in the input Array as index,
    // store count of each integer in freq[] Array
    for (int i = 0; i < size; i++) {
        freq[Key(A[i])]++;
    }
    // Calculate the starting index for each integer
    int total = 0;
    for (int i = 0; i < range; i++) {
        int oldCount = freq[i];
        freq[i] = total;
        total += oldCount;
    }
    // copy to output Array, preserving order of inputs with equal keys
    for (int i = 0; i < size; i++) {
        output[freq[Key(A[i])]] = A[i];
        freq[Key(A[i])]++;
    }
    free(freq);

    // copy the output Array back to the input Array
    for (int i = 0; i < size; i++) {
        A[i] = output[i];
    }
    // Don't want to use ArrayFree since that frees the item (still need
    // to print them in the main function);
    free(output);
}
