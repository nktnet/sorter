// Khiet Tam Nguyen (z5313514)
// 25.08.20

// Stupid Gnome Sort written for fun.

// If two elements are in the right order, move forward.
// Otherwise, swap them and move backwards.

// Visit Wikipedia for PseudoCode used for both algorithms:
// https://en.wikipedia.org/wiki/Gnome_sort

// There is also a GeeksToGeeks implementation (similar
// to the first algorithm below):
// https://www.geeksforgeeks.org/gnome-sort-a-stupid-one/

#include "sorter.h"

// Gnome sort - slow sorting algorithm.
void gnome_sort(Array A, int lo, int hi) { 
    if (!A) return;
    int i = lo;
    while (i <= hi) { 
        // Always move forward if we're at the start.
        if (i == lo || ItemCompare(A[i], A[i - 1]) >= 0) {
            i++;
        } else {
            ItemSwap(&A[i], &A[i - 1]);
            i--; 
        }
    } 
}

// Gnome sort with optimisation to become a variant
// of insertion sort. 
void gnome_sort_optimised(Array A, int lo, int hi) {
    if (!A) return;
    for (int i = lo; i <= hi; i++) {
        int j = i;
        while (j > lo && ItemCompare(A[j - 1], A[j]) > 0) {
            ItemSwap(&A[j - 1], &A[j]);
            j--;
        }
    }
}
