// Khiet Tam Nguyen (z5313514)
// 01.09.20

// Radix Sort - Non-Comparison Based Sorting Algorithm

// Although the ArrayGetMax function does use comparisons.

// Algorithm adapted from GeeksForGeeks:
// https://www.geeksforgeeks.org/radix-sort/

// NOTE: This algorithm only works when comparing the keys only, and
// the keys must also be non-negative (keys >= 0).

#include "sorter.h"

// BASE must be at least 2 for the algorithm to work
#define BASE 10

// Helpers
///////////////////////////////////////////////////

static void rdx_count(Array A, int size, int exp);

///////////////////////////////////////////////////

// Groups element's individual digits of equal place value 
// and rearranging according to increasing/decreasing order.
void radix_sort(Array A, int lo, int hi) {
    if (!A) return;
    int max = Key(ArrayGetMax(A, lo, hi));
    // Sorting by digits.
    for (int exp = 1; max/exp > 0; exp *= BASE) {
        rdx_count(&A[lo], A_SIZE(lo, hi), exp); 
    }
}

///////////////////////////////////////////////////

// Helper function for radix sort.
static void rdx_count(Array A, int size, int exp) {
    Item *output = malloc(size * sizeof(Item));
    int count[BASE] = {0}; 
    // Store count of occurrences in count array.
    for (int i = 0; i < size; i++) {
        count[(Key(A[i]) / exp) % BASE ]++; 
    }
    // Change count[i] so that count[i] now contains actual 
    // position of this digit in output[] 
    for (int i = 1; i < BASE; i++) {
        count[i] += count[i - 1]; 
    }
    // Build the output Array 
    for (int i = size - 1; i >= 0; i--) { 
        output[count[(Key(A[i]) / exp) % BASE] - 1] = A[i]; 
        count[(Key(A[i]) / exp) % BASE]--; 
    } 
    for (int i = 0; i < size; i++) {
        A[i] = output[i]; 
    }
    // Not calling ArrayFree here since we still need the 
    // items for printing later.
    free(output);
} 
